﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    //public float camSens = 0.25f;
    //private Vector3 lastMouse = new Vector3(255, 255, 255);

    public GameObject northWall;
    public GameObject eastWall;
    public GameObject westWall;

    float minFov = 30f;
    float maxFov = 110f;
    float sensitivity = 60f;

    void FixedUpdate()
    {
        /*lastMouse = Input.mousePosition - lastMouse;
        lastMouse = new Vector3(-lastMouse.y * camSens, lastMouse.x * camSens, 0);
        lastMouse = new Vector3(transform.eulerAngles.x + lastMouse.x, transform.eulerAngles.y + lastMouse.y, 0);
        transform.eulerAngles = lastMouse;
        lastMouse = Input.mousePosition;*/

        Vector3 movement = new Vector3(0, 0, 0);

        if ((Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow)) && (transform.position.z < (northWall.transform.position.z - 5)))
        {
            movement = new Vector3(0, 0, 1f);
        }
        if ((Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow)) && (transform.position.z > -10))
        {
            movement = new Vector3(0, 0, -1f);
        }
        if ((Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) && (transform.position.x > westWall.transform.position.x))
        {
            movement = new Vector3(-1f, 0, 0);
        }
        if ((Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) && (transform.position.x < eastWall.transform.position.x))
        {
            movement = new Vector3(1f, 0, 0);
        }

        
        Vector3 newPosition = transform.position;
        transform.Translate(movement);
        newPosition.x = transform.position.x;
        newPosition.z = transform.position.z;
        transform.position = newPosition;

        //This block of code allows the player to zoom in and out using field of view of the camera instead of actually moving the camera
        float fov = Camera.main.fieldOfView;
        fov -= Input.GetAxis("Mouse ScrollWheel") * sensitivity;
        fov = Mathf.Clamp(fov, minFov, maxFov);
        Camera.main.fieldOfView = fov;

        //This code zooms the camera in and out but actually moves the camera instead of just changing field of view. It currently doesn't work with the game since it stops 
        //the camera from moving left right up and down.
        /*float ScrollWheelChange = Input.GetAxis("Mouse ScrollWheel");           //This little peece of code is written by JelleWho https://github.com/jellewie
        if (ScrollWheelChange != 0)
        {                                            //If the scrollwheel has changed
            float R = ScrollWheelChange * 15;                                   //The radius from current camera
            float PosX = Camera.main.transform.eulerAngles.x + 90;              //Get up and down
            float PosY = -1 * (Camera.main.transform.eulerAngles.y - 90);       //Get left to right
            PosX = PosX / 180 * Mathf.PI;                                       //Convert from degrees to radians
            PosY = PosY / 180 * Mathf.PI;                                       //^
            float X = R * Mathf.Sin(PosX) * Mathf.Cos(PosY);                    //Calculate new coords
            float Z = R * Mathf.Sin(PosX) * Mathf.Sin(PosY);                    //^
            float Y = R * Mathf.Cos(PosX);                                      //^
            float CamX = Camera.main.transform.position.x;                      //Get current camera postition for the offset
            float CamY = Camera.main.transform.position.y;                      //^
            float CamZ = Camera.main.transform.position.z;                      //^
            Camera.main.transform.position = new Vector3(CamX + X, CamY + Y, CamZ + Z);//Move the main camera
        }*/
    }
}
