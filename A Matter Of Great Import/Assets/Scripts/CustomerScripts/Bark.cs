﻿[System.Serializable]
public class Bark
{
    public string id;
    public int character;
    public string dialogue;
    public string[] triggerRules;

}
