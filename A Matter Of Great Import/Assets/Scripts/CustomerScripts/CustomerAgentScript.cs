﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

public class CustomerAgentScript : MonoBehaviour
{

    public UnityEngine.AI.NavMeshAgent agent;
    public Vector3 goDestination = new Vector3(100f, 0f, 0f);

    public GameObject destinationRoom;
    public RoomController destinationController;

    public Dictionary<string, bool> worldQuery = new Dictionary<string, bool>();

    public TextAsset barksFile;
    
    public int trueCount;
    public int winningTrueCount = 0;
    public List<Bark> winningBarks = new List<Bark>();
    public List<Bark> genericBarks = new List<Bark>();
    public Bark winningBark;
    public string winningBarkText;

    public bool isBarking;

    public float barkTime = 3f;

    public bool loopBreak = false;

    public Text speechBubbleText;
    public Image speechBubbleBG;

    public List<Wiggle> wigglers = new List<Wiggle>();
    // Start is called before the first frame update
    void Start()
    {
        //agent.SetDestination(new Vector3 (0f, 0f, 0f));
    }

    // Update is called once per frame
    void Update()
    {
        if (Mathf.Abs(transform.position.x - goDestination.x) < 0.25f && Mathf.Abs(transform.position.z - goDestination.z) < 0.25f)
        {
            //Add cool money animations!
            if (!isBarking)
            {
                Bark();
                isBarking = true;
            }
        }
    }

    public void SetDestination(Vector3 destination)
    {
        agent.SetDestination(destination);
        goDestination = destination;
    }

    public void Bark()
    {
       
       GetQuery(); //build a list of facts about the world

       GetBark(); //Get the bark from the JSON that best fits those facts
       
       if (Random.Range(0, 100) > 25f) // later 50f?
       {
            PlayBark(winningBark); //Use the winningBark that you got from GetBark() and play it
       }
       else
       {
           Destroy(gameObject);
       }
       


       //After bark is said, disappear, maybe someday in a confetti of ziggies
        //DeSpawn();
    }

    public void PlayBark(Bark barkToPlay)
    {
        speechBubbleText.GetComponent<FancySpeechBubble>().Set(barkToPlay.dialogue);
        speechBubbleBG.color = Color.blue;

        gameObject.GetComponent<NavMeshAgent>().isStopped = true;

        foreach (Wiggle wiggle in wigglers)
        {
            Destroy(wiggle);
        }
        StartCoroutine("WaitForBark");
    }

    private void GetBark()
    {
        barksFile = Resources.Load<TextAsset>("Barks/barks");

        Barks barkList = JsonUtility.FromJson<Barks>(barksFile.text);

        trueCount = 0; 
        winningTrueCount = 0;

        winningBarks.Clear();
        genericBarks.Clear();

        foreach (Bark bark in barkList.barks)
        {
            if (bark.triggerRules.Length > 0)
            {
                foreach (string rule in bark.triggerRules)
                {
                    if (!worldQuery.ContainsKey(rule))
                    {
                        break;
                    }
                    if (worldQuery[rule] == false)
                    {
                        loopBreak = true;
                        break;
                    }
                    if (worldQuery[rule] == true)
                    {
                        trueCount++;
                    }
                }

                if (loopBreak)
                {
                }
                else if (trueCount > winningTrueCount)
                {
                    winningBarks.Clear();
                    winningBarks.Add(bark);
                    winningTrueCount = trueCount;
                }
                else if (trueCount == winningTrueCount)
                {
                    winningBarks.Add(bark);
                }
                
                trueCount = 0;
            }

            else
            {
                genericBarks.Add(bark);
            }

            loopBreak = false;

        }

        if (winningBarks.Count == 1)
        {
            winningBark = winningBarks[0];
        }
        else if (winningBarks.Count > 1)
        {
            winningBark = winningBarks[(int) Random.Range(0, winningBarks.Count - 1)];
        }
        else if (winningBarks.Count == 0)
        {
            winningBark = genericBarks[(int) Random.Range(0, genericBarks.Count - 1)];
        }

    }

    private void GetQuery()
    {
        worldQuery.Clear();

        destinationController = destinationRoom.GetComponent<RoomController>();

        //New barks: 

            //room names
        
        if (destinationController.roomBusinessTemplate.roomName == "General Store")
        {
            worldQuery.Add("c_room_general", true);
        }
        else
        {
            worldQuery.Add("c_room_general", false);
        }

        if (destinationController.roomBusinessTemplate.roomName == "Holo-Theater")
        {
            worldQuery.Add("c_room_holo", true);
        }
        else
        {
            worldQuery.Add("c_room_holo", false);
        }

        if (destinationController.roomBusinessTemplate.roomName == "Laundromat")
        {
            worldQuery.Add("c_room_laundromat", true);
        }
        else
        {
            worldQuery.Add("c_room_laundromat", false);
        }

        if (destinationController.roomBusinessTemplate.roomName == "Office")
        {
            worldQuery.Add("c_room_office", true);
        }
        else
        {
            worldQuery.Add("c_room_office", false);
        }

        if (destinationController.roomBusinessTemplate.roomName == "Police Station")
        {
            worldQuery.Add("c_room_station", true);
        }
        else
        {
            worldQuery.Add("c_room_station", false);
        }

        if (destinationController.roomBusinessTemplate.roomName == "Restaurant")
        {
            worldQuery.Add("c_room_restaurant", true);
        }
        else
        {
            worldQuery.Add("c_room_restaurant", false);
        }

        if (destinationController.roomBusinessTemplate.roomName == "Casino")
        {
            worldQuery.Add("c_room_casino", true);
        }
        else
        {
            worldQuery.Add("c_room_casino", false);
        }

        if (destinationController.roomBusinessTemplate.roomName == "Courthouse")
        {
            worldQuery.Add("c_room_courthouse", true);
        }
        else
        {
            worldQuery.Add("c_room_courthouse", false);
        }

                //Room conditions

        if (0f < destinationController.condition && destinationController.condition < 20f)
        {
            worldQuery.Add("c_condition_high", true);
        }
        else
        {
            worldQuery.Add("c_condition_high", false);
        }

        if (20f <= destinationController.condition)
        {
            worldQuery.Add("c_condition_veryhigh", true);
        }
        else
        {
            worldQuery.Add("c_condition_veryhigh", false);
        }

        if (-20f < destinationController.condition && destinationController.condition < 0f)
        {
            worldQuery.Add("c_condition_low", true);
        }
        else
        {
            worldQuery.Add("c_condition_low", false);
        }

        if (destinationController.condition <= -20f)
        {
            worldQuery.Add("c_condition_verylow", true);
        }
        else
        {
            worldQuery.Add("c_condition_verylow", false);
        }


            //Commenting out old bark system for non-destructive purposes
        /*
        if (destinationController.attributeStats["Foot Traffic"] > 0 && System.Array.Exists(destinationController.roomBusinessTemplate.likedAttributes, attName => attName == "Foot Traffic"))
        {
            worldQuery.Add("c_foottraffic_liked_a", true);
        }
        else
        {
            worldQuery.Add("c_foottraffic_liked_a", false);
        }
        if (destinationController.attributeStats["Security"] > 0 && System.Array.Exists(destinationController.roomBusinessTemplate.likedAttributes, attName => attName == "Security"))
        {
            worldQuery.Add("c_security_liked_a", true);
        }
        else
        {
            worldQuery.Add("c_security_liked_a", false);
        }

        if (destinationController.attributeStats["Professional"] > 0 && System.Array.Exists(destinationController.roomBusinessTemplate.likedAttributes, attName => attName == "Professional"))
        {
            worldQuery.Add("c_professional_liked_a", true);
        }
        else
        {
            worldQuery.Add("c_professional_liked_a", false);
        }

        if (destinationController.attributeStats["Noise"] > 0 && System.Array.Exists(destinationController.roomBusinessTemplate.likedAttributes, attName => attName == "Noise"))
        {
            worldQuery.Add("c_noise_liked_a", true);
        }
        else
        {
            worldQuery.Add("c_noise_liked_a", false);
        }
        
        if (destinationController.attributeStats["Food"] > 0 && System.Array.Exists(destinationController.roomBusinessTemplate.likedAttributes, attName => attName == "Food"))
        {
            worldQuery.Add("c_food_liked_a", true);
        }
        else
        {
            worldQuery.Add("c_food_liked_a", false);
        }
        
        if (destinationController.attributeStats["Criminal"] > 0 && System.Array.Exists(destinationController.roomBusinessTemplate.likedAttributes, attName => attName == "Criminal"))
        {
            worldQuery.Add("c_criminal_liked_a", true);
        }
        else
        {
            worldQuery.Add("c_criminal_liked_a", false);
        }
        if (destinationController.attributeStats["Foot Traffic"] > 0 && System.Array.Exists(destinationController.roomBusinessTemplate.dislikedAttributes, attName => attName == "Foot Traffic"))
        {
            worldQuery.Add("c_foottraffic_disliked_a", true);
        }
        else
        {
            worldQuery.Add("c_foottraffic_disliked_a", false);
        }

        if (destinationController.attributeStats["Security"] > 0 && System.Array.Exists(destinationController.roomBusinessTemplate.dislikedAttributes, attName => attName == "Security"))
        {
            worldQuery.Add("c_security_disliked_a", true);
        }
        else
        {
            worldQuery.Add("c_security_disliked_a", false);
        }

        if (destinationController.attributeStats["Professional"] > 0 && System.Array.Exists(destinationController.roomBusinessTemplate.dislikedAttributes, attName => attName == "Professional"))
        {
            worldQuery.Add("c_professional_disliked_a", true);
        }
        else
        {
            worldQuery.Add("c_professional_disliked_a", false);
        }

        if (destinationController.attributeStats["Noise"] > 0 && System.Array.Exists(destinationController.roomBusinessTemplate.dislikedAttributes, attName => attName == "Noise"))
        {
            worldQuery.Add("c_noise_disliked_a", true);
        }
        else
        {
            worldQuery.Add("c_noise_disliked_a", false);
        }
        
        if (destinationController.attributeStats["Food"] > 0 && System.Array.Exists(destinationController.roomBusinessTemplate.dislikedAttributes, attName => attName == "Food"))
        {
            worldQuery.Add("c_food_disliked_a", true);
        }
        else
        {
            worldQuery.Add("c_food_disliked_a", false);
        }
        
        if (destinationController.attributeStats["Criminal"] > 0 && System.Array.Exists(destinationController.roomBusinessTemplate.dislikedAttributes, attName => attName == "Criminal"))
        {
            worldQuery.Add("c_criminal_disliked_a", true);
        }
        else
        {
            worldQuery.Add("c_criminal_disliked_a", false);
        }


        //Inactive atts

        if (destinationController.attributeStats["Foot Traffic"] == 0 && System.Array.Exists(destinationController.roomBusinessTemplate.likedAttributes, attName => attName == "Foot Traffic"))
        {
            worldQuery.Add("c_foottraffic_liked_i", true);
        }
        else
        {
            worldQuery.Add("c_foottraffic_liked_i", false);
        }
        if (destinationController.attributeStats["Security"] == 0 && System.Array.Exists(destinationController.roomBusinessTemplate.likedAttributes, attName => attName == "Security"))
        {
            worldQuery.Add("c_security_liked_i", true);
        }
        else
        {
            worldQuery.Add("c_security_liked_i", false);
        }

        if (destinationController.attributeStats["Professional"] == 0 && System.Array.Exists(destinationController.roomBusinessTemplate.likedAttributes, attName => attName == "Professional"))
        {
            worldQuery.Add("c_professional_liked_i", true);
        }
        else
        {
            worldQuery.Add("c_professional_liked_i", false);
        }

        if (destinationController.attributeStats["Noise"] == 0 && System.Array.Exists(destinationController.roomBusinessTemplate.likedAttributes, attName => attName == "Noise"))
        {
            worldQuery.Add("c_noise_liked_i", true);
        }
        else
        {
            worldQuery.Add("c_noise_liked_i", false);
        }
        
        if (destinationController.attributeStats["Food"] == 0 && System.Array.Exists(destinationController.roomBusinessTemplate.likedAttributes, attName => attName == "Food"))
        {
            worldQuery.Add("c_food_liked_i", true);
        }
        else
        {
            worldQuery.Add("c_food_liked_i", false);
        }
        
        if (destinationController.attributeStats["Criminal"] == 0 && System.Array.Exists(destinationController.roomBusinessTemplate.likedAttributes, attName => attName == "Criminal"))
        {
            worldQuery.Add("c_criminal_liked_i", true);
        }
        else
        {
            worldQuery.Add("c_criminal_liked_i", false);
        }

        if (destinationController.attributeStats["Foot Traffic"] == 0 && System.Array.Exists(destinationController.roomBusinessTemplate.dislikedAttributes, attName => attName == "Foot Traffic"))
        {
            worldQuery.Add("c_foottraffic_disliked_i", true);
        }
        else
        {
            worldQuery.Add("c_foottraffic_disliked_i", false);
        }

        if (destinationController.attributeStats["Security"] == 0 && System.Array.Exists(destinationController.roomBusinessTemplate.dislikedAttributes, attName => attName == "Security"))
        {
            worldQuery.Add("c_security_disliked_i", true);
        }
        else
        {
            worldQuery.Add("c_security_disliked_i", false);
        }

        if (destinationController.attributeStats["Professional"] == 0 && System.Array.Exists(destinationController.roomBusinessTemplate.dislikedAttributes, attName => attName == "Professional"))
        {
            worldQuery.Add("c_professional_disliked_i", true);
        }
        else
        {
            worldQuery.Add("c_professional_disliked_i", false);
        }

        if (destinationController.attributeStats["Noise"] == 0 && System.Array.Exists(destinationController.roomBusinessTemplate.dislikedAttributes, attName => attName == "Noise"))
        {
            worldQuery.Add("c_noise_disliked_i", true);
        }
        else
        {
            worldQuery.Add("c_noise_disliked_i", false);
        }
        
        if (destinationController.attributeStats["Food"] == 0 && System.Array.Exists(destinationController.roomBusinessTemplate.dislikedAttributes, attName => attName == "Food"))
        {
            worldQuery.Add("c_food_disliked_i", true);
        }
        else
        {
            worldQuery.Add("c_food_disliked_i", false);
        }
        
        if (destinationController.attributeStats["Criminal"] == 0 && System.Array.Exists(destinationController.roomBusinessTemplate.dislikedAttributes, attName => attName == "Criminal"))
        {
            worldQuery.Add("c_criminal_disliked_i", true);
        }
        else
        {
            worldQuery.Add("c_criminal_disliked_i", false);
        }

        //Do you like atts

        if (System.Array.Exists(destinationController.roomBusinessTemplate.likedAttributes, attName => attName == "Foot Traffic"))
        {
            worldQuery.Add("c_foottraffic_liked", true);
        }
        else
        {
            worldQuery.Add("c_foottraffic_liked", false);
        }
        if (System.Array.Exists(destinationController.roomBusinessTemplate.likedAttributes, attName => attName == "Security"))
        {
            worldQuery.Add("c_security_liked", true);
        }
        else
        {
            worldQuery.Add("c_security_liked", false);
        }

        if (System.Array.Exists(destinationController.roomBusinessTemplate.likedAttributes, attName => attName == "Professional"))
        {
            worldQuery.Add("c_professional_liked", true);
        }
        else
        {
            worldQuery.Add("c_professional_liked", false);
        }

        if (System.Array.Exists(destinationController.roomBusinessTemplate.likedAttributes, attName => attName == "Noise"))
        {
            worldQuery.Add("c_noise_liked", true);
        }
        else
        {
            worldQuery.Add("c_noise_liked", false);
        }
        
        if (System.Array.Exists(destinationController.roomBusinessTemplate.likedAttributes, attName => attName == "Food"))
        {
            worldQuery.Add("c_food_liked", true);
        }
        else
        {
            worldQuery.Add("c_food_liked", false);
        }
        
        if (System.Array.Exists(destinationController.roomBusinessTemplate.likedAttributes, attName => attName == "Criminal"))
        {
            worldQuery.Add("c_criminal_liked", true);
        }
        else
        {
            worldQuery.Add("c_criminal_liked", false);
        }

        if (System.Array.Exists(destinationController.roomBusinessTemplate.dislikedAttributes, attName => attName == "Foot Traffic"))
        {
            worldQuery.Add("c_foottraffic_disliked", true);
        }
        else
        {
            worldQuery.Add("c_foottraffic_disliked", false);
        }

        if (System.Array.Exists(destinationController.roomBusinessTemplate.dislikedAttributes, attName => attName == "Security"))
        {
            worldQuery.Add("c_security_disliked", true);
        }
        else
        {
            worldQuery.Add("c_security_disliked", false);
        }

        if (System.Array.Exists(destinationController.roomBusinessTemplate.dislikedAttributes, attName => attName == "Professional"))
        {
            worldQuery.Add("c_professional_disliked", true);
        }
        else
        {
            worldQuery.Add("c_professional_disliked", false);
        }

        if (System.Array.Exists(destinationController.roomBusinessTemplate.dislikedAttributes, attName => attName == "Noise"))
        {
            worldQuery.Add("c_noise_disliked", true);
        }
        else
        {
            worldQuery.Add("c_noise_disliked", false);
        }
        
        if (System.Array.Exists(destinationController.roomBusinessTemplate.dislikedAttributes, attName => attName == "Food"))
        {
            worldQuery.Add("c_food_disliked", true);
        }
        else
        {
            worldQuery.Add("c_food_disliked", false);
        }
        
        if (System.Array.Exists(destinationController.roomBusinessTemplate.dislikedAttributes, attName => attName == "Criminal"))
        {
            worldQuery.Add("c_criminal_disliked", true);
        }
        else
        {
            worldQuery.Add("c_criminal_disliked", false);
        }
        //Room conditions

        if (0f < destinationController.condition && destinationController.condition < 5f)
        {
            worldQuery.Add("c_condition_high", true);
        }
        else
        {
            worldQuery.Add("c_condition_high", false);
        }

        if (5f <= destinationController.condition && destinationController.condition <= 10f)
        {
            worldQuery.Add("c_condition_veryhigh", true);
        }
        else
        {
            worldQuery.Add("c_condition_veryhigh", false);
        }

        if (-5f < destinationController.condition && destinationController.condition < 0f)
        {
            worldQuery.Add("c_condition_low", true);
        }
        else
        {
            worldQuery.Add("c_condition_low", false);
        }

        if (-10f <= destinationController.condition && destinationController.condition <= -5f)
        {
            worldQuery.Add("c_condition_verylow", true);
        }
        else
        {
            worldQuery.Add("c_condition_verylow", false);
        }

        //faction

        if (destinationController.roomBusinessTemplate.faction == "Law")
        {
            worldQuery.Add("c_faction_law", true);
        }
        else
        {
            worldQuery.Add("c_faction_law", false);
        }

        if (destinationController.roomBusinessTemplate.faction == "Crime")
        {
            worldQuery.Add("c_faction_crime", true);
        }
        else
        {
            worldQuery.Add("c_faction_crime", false);
        }

        if (destinationController.roomBusinessTemplate.faction == "BRU")
        {
            worldQuery.Add("c_faction_bru", true);
        }
        else
        {
            worldQuery.Add("c_faction_bru", false);
        }

        //room names
        
        if (destinationController.roomBusinessTemplate.roomName == "General Store")
        {
            worldQuery.Add("c_room_general", true);
        }
        else
        {
            worldQuery.Add("c_room_general", false);
        }

        if (destinationController.roomBusinessTemplate.roomName == "Holo-Theater")
        {
            worldQuery.Add("c_room_holo", true);
        }
        else
        {
            worldQuery.Add("c_room_holo", false);
        }

        if (destinationController.roomBusinessTemplate.roomName == "Laundromat")
        {
            worldQuery.Add("c_room_laundromat", true);
        }
        else
        {
            worldQuery.Add("c_room_laundromat", false);
        }

        if (destinationController.roomBusinessTemplate.roomName == "Office")
        {
            worldQuery.Add("c_room_office", true);
        }
        else
        {
            worldQuery.Add("c_room_office", false);
        }

        if (destinationController.roomBusinessTemplate.roomName == "Police Station")
        {
            worldQuery.Add("c_room_station", true);
        }
        else
        {
            worldQuery.Add("c_room_station", false);
        }

        if (destinationController.roomBusinessTemplate.roomName == "Restaurant")
        {
            worldQuery.Add("c_room_restaurant", true);
        }
        else
        {
            worldQuery.Add("c_room_restaurant", false);
        }

        if (destinationController.roomBusinessTemplate.roomName == "Casino")
        {
            worldQuery.Add("c_room_casino", true);
        }
        else
        {
            worldQuery.Add("c_room_casino", false);
        }

        if (destinationController.roomBusinessTemplate.roomName == "Courthouse")
        {
            worldQuery.Add("c_room_courthouse", true);
        }
        else
        {
            worldQuery.Add("c_room_courthouse", false);
        }*/

    }

    private void DeSpawn()
    {
        Destroy(gameObject);
    }

    IEnumerator WaitForBark()
    {
        yield return new WaitForSeconds(barkTime);

        DeSpawn();
    }
    
}
