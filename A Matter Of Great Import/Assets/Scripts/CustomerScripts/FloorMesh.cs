﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class FloorMesh : MonoBehaviour
{

    public List<GameObject> customerDestinationList = new List<GameObject>();

    public Vector3 customerSpawnLocation;

    public List<GameObject> roomList = new List<GameObject>();
    private Vector3 tempTarget;

    public UIManager uiManager;

    public float spawnTime;

    public GameObject customerPrefab;

    public GameObject spawnRef;

    public int currentDestinationRoomIndex;


    void Start()
    {
        roomList = uiManager.currentRooms; //Grab the list of rooms from the UI Manager

        foreach (GameObject room in roomList) //Create the target positions for customers
        {
            /*if (room.GetComponent<RoomController>().roomBusinessTemplate == null)
            {
                tempTarget = room.transform.position + new Vector3(0f, 0f, -5f);
                customerDestinationList.Add(tempTarget);
            }*/
            if (room.GetComponent<RoomController>().roomBusinessTemplate != null && room.GetComponent<RoomController>().roomBusinessTemplate.roomName == "Entry")
            {               
                customerSpawnLocation = room.transform.position + new Vector3(0f, 0f, -5f);
                
            }
            else //if (room.GetComponent<RoomController>().roomBusinessTemplate != null)
            {
                
            }

        }

        StartCoroutine("WaitForSpawn");
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SpawnCustomer()
    {
        if (customerDestinationList.Count > 0)
        {
            GameObject[] customerDestinationArray = customerDestinationList.ToArray();

            /*float overallProfit = 0;
            foreach (GameObject room in customerDestinationArray)
            {
                overallProfit += room.GetComponent<RoomController>().calculatedProfit;
            }*/
           
            //float currentChance = 0;
            
            
            //if (randNum <= (customerDestinationArray[i].GetComponent<RoomController>().calculatedProfit  / overallProfit * 100) + currentChance)
            //{            
            spawnRef = Instantiate(customerPrefab, customerSpawnLocation + new Vector3(0f, -0.25f, 0f), transform.rotation);
            spawnRef.GetComponent<CustomerAgentScript>().SetDestination(customerDestinationList[currentDestinationRoomIndex].transform.position + new Vector3(0f, 0f, -5f));
            spawnRef.GetComponent<CustomerAgentScript>().destinationRoom = customerDestinationList[currentDestinationRoomIndex];
            //}

            if(customerDestinationList.Count - 1 == currentDestinationRoomIndex)
            {
                currentDestinationRoomIndex = 0;
            }
            else
            {
                currentDestinationRoomIndex++;           
            }

            //currentChance += customerDestinationArray[i].GetComponent<RoomController>().calculatedProfit / overallProfit * 100;
            
        }

        StartCoroutine("WaitForSpawn");
        //UpdateRooms();
    }

    IEnumerator WaitForSpawn()
    {
        yield return new WaitForSeconds(spawnTime);

        SpawnCustomer();
    }

    public void UpdateRooms()
    {
    
    }
}
