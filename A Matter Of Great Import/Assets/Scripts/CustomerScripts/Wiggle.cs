﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wiggle : MonoBehaviour
{

    public float sinVar;
    public float sinVarMult;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate (Mathf.Sin(Time.time * sinVar) * sinVarMult, 0.0f, 0.0f);
    }
}
