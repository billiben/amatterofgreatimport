﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameController
{
    public static int currentLevelNum;

    public static string[,] allObjectives = new string[,] { { "Obtain 2000 ziggies", "" },
        { "Obtain 2000 ziggies", "Have a perfect law relationship" }, {"Obtain 4000 ziggies", "Eliminate all rats"}, {"Obtain 10000 ziggies and eliminate all rats", "" },
        { "Obtain 4000 ziggies", "Have either 5 law rooms or crime rooms at one time and no other factions"}, {"Destroy and free all 5 pirat petting pens", ""} };

    public static bool[,] isObjectiveComplete = new bool[,] { {false, false},
        {false, false}, {false, false}, {false, false}, {false, false}, {false, false} };

    //public static bool rerollChosen;

    //public static bool demolitionChosen;
}
