﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ItemDragHandler : MonoBehaviour, IDragHandler, IEndDragHandler, IBeginDragHandler, IPointerDownHandler
{
    public GameObject roomPicture;
    public Tray tray;
    public RerollTrayController rerollTray;
    public DemolitionTrayController demolitionTray;
    public RoomZoom roomZoom;

    public static bool mouseReleasedRoomController;
    public static bool mouseReleasedTray;
    public static bool isDragging;
    public static char roomNum;

    //This function makes an image of the selected room active and puts it directly on the mouse cursor
    public void OnBeginDrag(PointerEventData eventData)
    {
        roomPicture.GetComponent<Image>().sprite = gameObject.GetComponent<Image>().sprite;
        roomPicture.transform.position = Input.mousePosition;
        roomPicture.SetActive(true);
    }

    //This keeps the room picture on the mouse cursor while dragging 
    public void OnDrag(PointerEventData eventData)
    {
        roomPicture.transform.position = Input.mousePosition;
        isDragging = true;
    }

    //The picture is set back to origin, made inactive, and set to a blank image
    public void OnEndDrag(PointerEventData eventData)
    {
        roomPicture.transform.position = new Vector3(0,0,0);
        roomPicture.GetComponent<Image>().sprite = null;
        roomPicture.SetActive(false);

        mouseReleasedRoomController = true;
        mouseReleasedTray = true;
        isDragging = false;
        roomNum = gameObject.name[gameObject.name.Length - 1];

        if(roomZoom.currentHoveredOverFoundation == null)
        {
            mouseReleasedRoomController = false;
        }
    }

    //When the player clicks on a picture in the tray, this assigns the room type associated with that picture to the current room type that can be placed
    public void OnPointerDown(PointerEventData eventData)
    {
        
        if (gameObject.name.CompareTo("Room1") == 0)
        {
            Tray.currentBusinessSelected = tray.room1;
        }
        else if (gameObject.name.CompareTo("Room2") == 0)
        {
            Tray.currentBusinessSelected = tray.room2;
        }
        else if (gameObject.name.CompareTo("Room3") == 0)
        {
            Tray.currentBusinessSelected = tray.room3;
        }
        /*else
        {
            RerollTrayController.currentBusinessSelected = rerollTray.room4;
        }
        
        else
        {
            if (gameObject.name.CompareTo("Room1") == 0)
            {
                DemolitionTrayController.currentBusinessSelected = demolitionTray.room1;
            }
            else
            {
                DemolitionTrayController.currentBusinessSelected = demolitionTray.room2;
            }
        }*/
    }
}
