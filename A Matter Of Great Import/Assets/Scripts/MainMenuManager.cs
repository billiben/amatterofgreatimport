﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuManager : MonoBehaviour
{
    public GameObject startButton;
    public GameObject quitButton;

    public void Start()
    {
       startButton.GetComponent<Button>().onClick.AddListener(StartGame);
       quitButton.GetComponent<Button>().onClick.AddListener(Application.Quit);
    }

    public void StartGame()
    {
        SceneManager.LoadScene(1);
    }
}
