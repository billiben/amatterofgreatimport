﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class ObjectiveController : MonoBehaviour
{
    public GameObject objectiveButton;
    public GameObject objectiveList;
    public GameObject objectiveText;
    public GameObject UIManager;

    public int numOfCurrObjectives;

    private bool nextScene;
    private float timeTillNextScene;

    private bool moneyObtained2000;
    private bool moneyObtained4000;
    private bool moneyObtained10000;
    private bool isLawRelationshipPerfect;
    private bool ratsEliminated;
    private bool fiveCrimeOrLaw;
    private bool allPettingPensRemoved;

    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i < 2; i++)
        {
            if (GameController.allObjectives[GameController.currentLevelNum, i] != "")
            {
                SetObjectiveText(GameController.allObjectives[GameController.currentLevelNum, i]);
            }
        }
        
        objectiveButton.GetComponent<Button>().onClick.AddListener(objectivesList);

        objectiveText.GetComponent<TextMeshProUGUI>().text = "Have a rating of at least " + UIManager.GetComponent<UIManager>().minRating + " stars before you fill the station or time runs out!";
    }

    // Update is called once per frame
    void Update()
    {
        CheckObjectives();

        if (nextScene)
        {
            timeTillNextScene += Time.deltaTime;

            if (timeTillNextScene > 5)
            {
                GameController.currentLevelNum++;
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            }
        }
    }

    public void objectivesList()
    {
        if (objectiveList.activeSelf == false)
        {
            objectiveList.SetActive(true);
        }
        else if (objectiveList.activeSelf == true)
        {
            objectiveList.SetActive(false);
        }
    }

    public void SetObjectiveText(string content)
    {
        //objectiveText.GetComponent<TextMeshProUGUI>().text += content + "\n\n";
        numOfCurrObjectives++;
    }

    public bool ContainsObjective(string objective)
    {
        for(int i = 0; i < 2; i++)
        {
            if (GameController.allObjectives[GameController.currentLevelNum, i].Equals(objective))
            {
                return true;
            }
        }

        return false;
    }

    public void StrikeThroughCompletedObjective()
    {
        objectiveText.GetComponent<TextMeshProUGUI>().text = "";

        for (int i = 0; i < 2; i++)
        {
            if (GameController.allObjectives[GameController.currentLevelNum, i] != null && GameController.isObjectiveComplete[GameController.currentLevelNum, i] == false)
            {
                objectiveText.GetComponent<TextMeshProUGUI>().text += GameController.allObjectives[GameController.currentLevelNum, i] + "\n\n";
            } 
            else if(GameController.allObjectives[GameController.currentLevelNum, i] != null && GameController.isObjectiveComplete[GameController.currentLevelNum, i] == true)
            {            
                objectiveText.GetComponent<TextMeshProUGUI>().text += "Completed: " + GameController.allObjectives[GameController.currentLevelNum, i] + "\n\n";
            }
        }
    }

    public void SetObjectiveCompleted(string s)
    {
        for (int i = 0; i < 2; i++)
        {
            if (GameController.allObjectives[GameController.currentLevelNum, i] == s) 
            {
                GameController.isObjectiveComplete[GameController.currentLevelNum, i] = true;
            }
        }
    }

    public void CheckObjectives()
    {
        if (numOfCurrObjectives == 0 && !(SceneManager.GetActiveScene().buildIndex == SceneManager.sceneCountInBuildSettings - 1))
        {
            nextScene = true;
        }

        //if (gameObject.GetComponent<UIManager>().money >= 2000 && !moneyObtained2000 && ContainsObjective("Obtain 2000 ziggies"))
        {
            objectiveList.SetActive(true);
            SetObjectiveCompleted("Obtain 2000 ziggies");
            StrikeThroughCompletedObjective();
            moneyObtained2000 = true;
            numOfCurrObjectives--;
        }

        //if (gameObject.GetComponent<UIManager>().money >= 4000 && !moneyObtained4000 && ContainsObjective("Obtain 4000 ziggies"))
        {
            objectiveList.SetActive(true);
            SetObjectiveCompleted("Obtain 4000 ziggies");
            StrikeThroughCompletedObjective();
            moneyObtained4000 = true;
            numOfCurrObjectives--;
        }

        if (gameObject.GetComponent<UIManager>().lawRelationship >= 50 && !isLawRelationshipPerfect && ContainsObjective("Have a perfect law relationship"))
        {
            objectiveList.SetActive(true);
            SetObjectiveCompleted("Have a perfect law relationship");
            StrikeThroughCompletedObjective();
            isLawRelationshipPerfect = true;
            numOfCurrObjectives--;
        }

        //if (gameObject.GetComponent<RatCreation>().noRats && !ratsEliminated && ContainsObjective("Eliminate all rats"))
        {
            objectiveList.SetActive(true);
            SetObjectiveCompleted("Eliminate all rats");
            StrikeThroughCompletedObjective();
            ratsEliminated = true;
            numOfCurrObjectives--;
        }

        //if (gameObject.GetComponent<RatCreation>().noRats && !ratsEliminated && gameObject.GetComponent<UIManager>().money >= 10000 && !moneyObtained10000 && ContainsObjective("Obtain 10000 ziggies and eliminate all rats"))
        {
            objectiveList.SetActive(true);
            SetObjectiveCompleted("Obtain 10000 ziggies and eliminate all rats");
            StrikeThroughCompletedObjective();
            ratsEliminated = true;
            moneyObtained10000 = true;
            numOfCurrObjectives--;
        }

        if (((GetComponent<UIManager>().numOfCrimeRooms >= 5 && GetComponent<UIManager>().numOfLawRooms == 0) 
            || (GetComponent<UIManager>().numOfLawRooms >= 5 && GetComponent<UIManager>().numOfCrimeRooms == 0)) 
            && !fiveCrimeOrLaw && ContainsObjective("Have either 5 law rooms or crime rooms at one time and no other factions"))
        {
            objectiveList.SetActive(true);
            SetObjectiveCompleted("Have either 5 law rooms or crime rooms at one time and no other factions");
            StrikeThroughCompletedObjective();
            fiveCrimeOrLaw = true;
            numOfCurrObjectives--;
        }

        if (GetComponent<UIManager>().numOfPiratPettingPens == 0 && !allPettingPensRemoved && ContainsObjective("Destroy and free all 5 pirat petting pens"))
        {
            objectiveList.SetActive(true);
            SetObjectiveCompleted("Destroy and free all 5 pirat petting pens");
            StrikeThroughCompletedObjective();
            allPettingPensRemoved = true;
            numOfCurrObjectives--;
        }
    }
}
