using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class DemolitionTrayController : MonoBehaviour
{
    public UIManager uiManager;

    public RoomCreation room1;
    public RoomCreation room2;

    public Button room1image;
    public Button room2image;

    public GameObject room1NameText;
    public GameObject room2NameText;
    public GameObject room1CostText;
    public GameObject room2CostText;
    public GameObject room1FactionText;
    public GameObject room2FactionText;
    public GameObject room1FactionModifierText;
    public GameObject room2FactionModifierText;

    private int randomInt;
    private float tempCost;

    public static RoomCreation currentBusinessSelected;

    public RoomCreation[] possibleRooms;

    private void Start()
    {
        ChangeRooms1();
        ChangeRooms2();

        currentBusinessSelected = room1;

        StartCoroutine("UpdateRoom1Cost");
        StartCoroutine("UpdateRoom2Cost");
    }

    private void Update()
    {
        //print("MouseReleased: " + ItemDragHandler.mouseReleased);
        //print("Placed Room: " + RoomController.placedRoom);
        if (ItemDragHandler.roomNum.CompareTo('1') == 0 && ItemDragHandler.mouseReleasedTray && RoomController.placedRoom)
        {
            //print('1');
            RoomController.placedRoom = false;
            ItemDragHandler.mouseReleasedTray = false;
            ChangeRooms1();
        }
        if (ItemDragHandler.roomNum.CompareTo('2') == 0 && ItemDragHandler.mouseReleasedTray && RoomController.placedRoom)
        {
            //print('2');
            RoomController.placedRoom = false;
            ItemDragHandler.mouseReleasedTray = false;
            ChangeRooms2();
        }
    }

    public void ChangeRooms1()
    {
        randomInt = Random.Range(0, possibleRooms.Length);
        room1 = possibleRooms[randomInt];
        room1image.GetComponent<Image>().sprite = possibleRooms[randomInt].roomJPG;
        room1NameText.GetComponent<TextMeshProUGUI>().text = possibleRooms[randomInt].roomName;

        if (room1.faction == "BRU")
        {
            room1FactionText.GetComponent<TextMeshProUGUI>().text = "Faction: BRU";
            tempCost = Mathf.Floor(room1.placementCost * (100 + (uiManager.bruRelationship * -1)) * 0.01f);
            room1CostText.GetComponent<TextMeshProUGUI>().text = "Cost: " + tempCost;
        }
        else if (room1.faction == "Law")
        {
            room1FactionText.GetComponent<TextMeshProUGUI>().text = "Faction: Law";
            tempCost = Mathf.Floor(room1.placementCost * (100 + (uiManager.lawRelationship * -1)) * 0.01f);
            room1CostText.GetComponent<TextMeshProUGUI>().text = "Cost: " + tempCost;
        }
        else if (room1.faction == "Crime")
        {
            room1FactionText.GetComponent<TextMeshProUGUI>().text = "Faction: Crime";
            tempCost = Mathf.Floor(room1.placementCost * (100 + (uiManager.crimeRelationship * -1)) * 0.01f);
            room1CostText.GetComponent<TextMeshProUGUI>().text = "Cost: " + tempCost;
        }
        

        if (tempCost - room1.placementCost > 0)
        {
            room1FactionModifierText.GetComponent<TextMeshProUGUI>().color = Color.red;
            room1FactionModifierText.GetComponent<TextMeshProUGUI>().text = "+" + (tempCost - room1.placementCost);
        }
        else if (tempCost - room1.placementCost < 0)
        {
            room1FactionModifierText.GetComponent<TextMeshProUGUI>().color = Color.green;
            room1FactionModifierText.GetComponent<TextMeshProUGUI>().text = "" + (tempCost - room1.placementCost);
        }
        else
        {
            room1FactionModifierText.GetComponent<TextMeshProUGUI>().color = Color.white;
            room1FactionModifierText.GetComponent<TextMeshProUGUI>().text = "" + (tempCost - room1.placementCost);
        }
    }

    public void ChangeRooms2()
    {
        randomInt = Random.Range(0, possibleRooms.Length);
        room2 = possibleRooms[randomInt];
        room2image.GetComponent<Image>().sprite = possibleRooms[randomInt].roomJPG;
        room2NameText.GetComponent<TextMeshProUGUI>().text = possibleRooms[randomInt].roomName;

        if (room2.faction == "BRU")
        {
            room2FactionText.GetComponent<TextMeshProUGUI>().text = "Faction: BRU";
            tempCost = Mathf.Floor(room2.placementCost * (100 + (uiManager.bruRelationship * -1)) * 0.01f);
            room2CostText.GetComponent<TextMeshProUGUI>().text = "Cost: " + tempCost;
        }
        else if (room2.faction == "Law")
        {
            room2FactionText.GetComponent<TextMeshProUGUI>().text = "Faction: Law";
            tempCost = Mathf.Floor(room2.placementCost * (100 + (uiManager.lawRelationship * -1)) * 0.01f);
            room2CostText.GetComponent<TextMeshProUGUI>().text = "Cost: " + tempCost;
        }
        else if (room2.faction == "Crime")
        {
            room2FactionText.GetComponent<TextMeshProUGUI>().text = "Faction: Crime";
            tempCost = Mathf.Floor(room2.placementCost * (100 + (uiManager.crimeRelationship * -1)) * 0.01f);
            room2CostText.GetComponent<TextMeshProUGUI>().text = "Cost: " + tempCost;
        }

        if (tempCost - room2.placementCost > 0)
        {
            room2FactionModifierText.GetComponent<TextMeshProUGUI>().color = Color.red;
            room2FactionModifierText.GetComponent<TextMeshProUGUI>().text = "+" + (tempCost - room2.placementCost);
        }
        else if (tempCost - room2.placementCost < 0)
        {
            room2FactionModifierText.GetComponent<TextMeshProUGUI>().color = Color.green;
            room2FactionModifierText.GetComponent<TextMeshProUGUI>().text = "" + (tempCost - room2.placementCost);
        }
        else
        {
            room2FactionModifierText.GetComponent<TextMeshProUGUI>().color = Color.white;
            room2FactionModifierText.GetComponent<TextMeshProUGUI>().text = "" + (tempCost - room2.placementCost);
        }
    }

    IEnumerator UpdateRoom1Cost()
    {
        yield return new WaitForSeconds(1f);

        if (room1.faction == "BRU")
        {
            room1FactionText.GetComponent<TextMeshProUGUI>().text = "Faction: BRU";
            tempCost = Mathf.Floor(room1.placementCost * (100 + (uiManager.bruRelationship * -1)) * 0.01f);
            room1CostText.GetComponent<TextMeshProUGUI>().text = "Cost: " + tempCost;
        }
        else if (room1.faction == "Law")
        {
            room1FactionText.GetComponent<TextMeshProUGUI>().text = "Faction: Law";
            tempCost = Mathf.Floor(room1.placementCost * (100 + (uiManager.lawRelationship * -1)) * 0.01f);
            room1CostText.GetComponent<TextMeshProUGUI>().text = "Cost: " + tempCost;
        }
        else if (room1.faction == "Crime")
        {
            room1FactionText.GetComponent<TextMeshProUGUI>().text = "Faction: Crime";
            tempCost = Mathf.Floor(room1.placementCost * (100 + (uiManager.crimeRelationship * -1)) * 0.01f);
            room1CostText.GetComponent<TextMeshProUGUI>().text = "Cost: " + tempCost;
        }


        if (tempCost - room1.placementCost > 0)
        {
            room1FactionModifierText.GetComponent<TextMeshProUGUI>().color = Color.red;
            room1FactionModifierText.GetComponent<TextMeshProUGUI>().text = "+" + (tempCost - room1.placementCost);
        }
        else if (tempCost - room1.placementCost < 0)
        {
            room1FactionModifierText.GetComponent<TextMeshProUGUI>().color = Color.green;
            room1FactionModifierText.GetComponent<TextMeshProUGUI>().text = "" + (tempCost - room1.placementCost);
        }
        else
        {
            room1FactionModifierText.GetComponent<TextMeshProUGUI>().color = Color.white;
            room1FactionModifierText.GetComponent<TextMeshProUGUI>().text = "" + (tempCost - room1.placementCost);
        }

        StartCoroutine("UpdateRoom1Cost");
    }

    IEnumerator UpdateRoom2Cost()
    {
        yield return new WaitForSeconds(1f);

        if (room2.faction == "BRU")
        {
            room2FactionText.GetComponent<TextMeshProUGUI>().text = "Faction: BRU";
            tempCost = Mathf.Floor(room2.placementCost * (100 + (uiManager.bruRelationship * -1)) * 0.01f);
            room2CostText.GetComponent<TextMeshProUGUI>().text = "Cost: " + tempCost;
        }
        else if (room2.faction == "Law")
        {
            room2FactionText.GetComponent<TextMeshProUGUI>().text = "Faction: Law";
            tempCost = Mathf.Floor(room2.placementCost * (100 + (uiManager.lawRelationship * -1)) * 0.01f);
            room2CostText.GetComponent<TextMeshProUGUI>().text = "Cost: " + tempCost;
        }
        else if (room2.faction == "Crime")
        {
            room2FactionText.GetComponent<TextMeshProUGUI>().text = "Faction: Crime";
            tempCost = Mathf.Floor(room2.placementCost * (100 + (uiManager.crimeRelationship * -1)) * 0.01f);
            room2CostText.GetComponent<TextMeshProUGUI>().text = "Cost: " + tempCost;
        }

        if (tempCost - room2.placementCost > 0)
        {
            room2FactionModifierText.GetComponent<TextMeshProUGUI>().color = Color.red;
            room2FactionModifierText.GetComponent<TextMeshProUGUI>().text = "+" + (tempCost - room2.placementCost);
        }
        else if (tempCost - room2.placementCost < 0)
        {
            room2FactionModifierText.GetComponent<TextMeshProUGUI>().color = Color.green;
            room2FactionModifierText.GetComponent<TextMeshProUGUI>().text = "" + (tempCost - room2.placementCost);
        }
        else
        {
            room2FactionModifierText.GetComponent<TextMeshProUGUI>().color = Color.white;
            room2FactionModifierText.GetComponent<TextMeshProUGUI>().text = "" + (tempCost - room2.placementCost);
        }

        StartCoroutine("UpdateRoom2Cost");
    }

    //Cycles through the possibleRooms array to the left, and chooses the current placeable room (using the RoomCreation class)
    /*public void clickedLeftArrow()
    {
        currentRoomNum -= 1;
        if (currentRoomNum < 0)
        {
            currentRoomNum = possibleRooms.Length - 1;
        }

        room1 = possibleRooms[currentRoomNum];
        room1image.GetComponent<Image>().sprite = possibleRooms[currentRoomNum].roomJPG;
        room1text.text = possibleRooms[currentRoomNum].roomName;

        currentBusinessSelected = room1;
    }

    //Cycles through the possibleRooms array to the right, and chooses the current placeable room (using the RoomCreation class)
    public void clickedRightArrow()
    {
        currentRoomNum += 1;
        if (currentRoomNum > possibleRooms.Length - 1)
        {
            currentRoomNum = 0;
        }

        room1 = possibleRooms[currentRoomNum];
        room1image.GetComponent<Image>().sprite = possibleRooms[currentRoomNum].roomJPG;
        room1text.text = possibleRooms[currentRoomNum].roomName;

        currentBusinessSelected = room1;
    }*/

    /*public void clickedRoom1()
    {
        
        currentBusinessSelected = room1;
        
    }*/
}
