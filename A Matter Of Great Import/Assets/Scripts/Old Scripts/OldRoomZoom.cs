﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OldRoomZoom : MonoBehaviour
{
    /*public Sprite[] allAttributeSprites;
    public GameObject topRoom;
    public GameObject topRoomAttributeSprite;
    public GameObject topRoomOutput;
    public GameObject topRoomArrow;
    public GameObject bottomRoom;
    public GameObject bottomRoomAttributeSprite;
    public GameObject bottomRoomOutput;
    public GameObject bottomRoomArrow;
    public GameObject leftRoom;
    public GameObject leftRoomAttributeSprite;
    public GameObject leftRoomOutput;
    public GameObject leftRoomArrow;
    public GameObject rightRoom;
    public GameObject rightRoomAttributeSprite;
    public GameObject rightRoomOutput;
    public GameObject rightRoomArrow;*/

    /*public GameObject upgradeUI;   
    public GameObject confirmUpgradeButton;
    public GameObject cancelUpgradeButton;*/

    /*void Start()
    {   upgradeButton.GetComponent<Button>().onClick.AddListener(ConfirmRoomUpgrade);
        upgradeButtonDemolition.GetComponent<Button>().onClick.AddListener(ConfirmRoomUpgrade);

        removeButton.GetComponent<Button>().onClick.AddListener(ConfirmRemoveRoom);
        removeButtonDemolition.GetComponent<Button>().onClick.AddListener(ConfirmRemoveRoom);

        removeRoomYesButton.GetComponent<Button>().onClick.AddListener(ConfirmRemoveRoom);
        removeRoomNoButton.GetComponent<Button>().onClick.AddListener(CancelRemoveRoom);

        changeRoomButton.GetComponent<Button>().onClick.AddListener(ShowChangeRoomUI);
        changeUILeftArrow.GetComponent<Button>().onClick.AddListener(ChangeRoomLeft);
        changeUIRightArrow.GetComponent<Button>().onClick.AddListener(ChangeRoomRight);
        confirmChangeButton.GetComponent<Button>().onClick.AddListener(ConfirmChangeRoom);
        cancelChangeButton.GetComponent<Button>().onClick.AddListener(CancelChangeRoom);
    }

    /*public void ShowRoomZoom()
    {
        uiManager.analyticsButton.GetComponent<Button>().interactable = false;

        topRoomOutput.GetComponent<TextMeshProUGUI>().text = "";
        bottomRoomOutput.GetComponent<TextMeshProUGUI>().text = "";
        leftRoomOutput.GetComponent<TextMeshProUGUI>().text = "";
        rightRoomOutput.GetComponent<TextMeshProUGUI>().text = "";

        topRoom.SetActive(false);
        bottomRoom.SetActive(false);
        leftRoom.SetActive(false);
        rightRoom.SetActive(false);

        if (previousSelectedRoom == null)
        {
            originalPosition = mainCamera.transform.position;
            originalSelectedRoom = activePlaceRoom;
        }

        previousSelectedRoom = activePlaceRoom;

        //These if else statements makes the remove button and upgrade button not visible for Entry rooms, the upgrade button not visible for pirat petting pens, and both visible
        //for everything else
        if(activePlaceRoom.roomBusinessTemplate.name == "Entry")
        {
            removeButton.SetActive(false);
            upgradeButton.SetActive(false);
        }
        else if (activePlaceRoom.roomBusinessTemplate.name == "Pirat Petting Pen")
        {
            upgradeButton.SetActive(false);
            removeButton.SetActive(true);
        }
        else
        {
            removeButton.SetActive(true);
            upgradeButton.SetActive(true);
        }

        if(activePlaceRoom.level == 3)
        {
            upgradeButtonText.GetComponent<TextMeshProUGUI>().text = "Room level maxed";
            upgradeButtonDemolitionText.GetComponent<TextMeshProUGUI>().text = "Room level maxed";
        }
        else if (activePlaceRoom.roomBusinessTemplate.faction == "BRU")
        {
            upgradeCost = 75 + (uiManager.bruRelationship * -1);
            upgradeButtonText.GetComponent<TextMeshProUGUI>().text = "Upgrade room for " + upgradeCost + "$";
            upgradeButtonDemolitionText.GetComponent<TextMeshProUGUI>().text = "Upgrade room for " + upgradeCost + "$";
        }
        else if (activePlaceRoom.roomBusinessTemplate.faction == "Law")
        {
            upgradeCost = 75 + (uiManager.lawRelationship * -1);
            upgradeButtonText.GetComponent<TextMeshProUGUI>().text = "Upgrade room for " + upgradeCost + "$";
            upgradeButtonDemolitionText.GetComponent<TextMeshProUGUI>().text = "Upgrade room for " + upgradeCost + "$";
        }
        else if (activePlaceRoom.roomBusinessTemplate.faction == "Crime")
        {
            upgradeCost = 75 + (uiManager.crimeRelationship * -1);
            upgradeButtonText.GetComponent<TextMeshProUGUI>().text = "Upgrade room for " + upgradeCost + "$";
            upgradeButtonDemolitionText.GetComponent<TextMeshProUGUI>().text = "Upgrade room for " + upgradeCost + "$";
        }

        if (activePlaceRoom.roomBusinessTemplate.roomName == "Pirat Petting Pen")
        {
            removeButtonText.GetComponent<TextMeshProUGUI>().text = "Remove room for 500$";
            removeButtonDemolitionText.GetComponent<TextMeshProUGUI>().text = "Remove room for 500$";
            removeRoomCost = 500f;
        }
        else
        {
            removeButtonText.GetComponent<TextMeshProUGUI>().text = "Remove room for 500$";
            removeButtonDemolitionText.GetComponent<TextMeshProUGUI>().text = "Remove room for 500$";
            removeRoomCost = 500f;
        }

        if (!activePlaceRoom.hasBeenPlaced)
        {
            placementCostText.SetActive(true);
            placementCostBackground.SetActive(true);
            placementCostText.GetComponent<TextMeshProUGUI>().text = "Cost: " + activePlaceRoom.calculatedPlacementCost;

            upgradeButton.SetActive(false);
            upgradeButtonDemolition.SetActive(false);
            removeButton.SetActive(false);
            removeButtonDemolition.SetActive(false);
            changeRoomButton.SetActive(false);
        }
        else
        {
            placementCostText.SetActive(false);
            placementCostBackground.SetActive(false);
        }

        roomLevelText.GetComponent<TextMeshProUGUI>().text = "lvl " + activePlaceRoom.level;

        if (activePlaceRoom.calculatedProfit > Mathf.Ceil(activePlaceRoom.baseProfit))
        {
            profitText.GetComponent<TextMeshProUGUI>().color = Color.green;
            conditionProfitNumText.GetComponent<TextMeshProUGUI>().color = Color.green;
            conditionProfitNumText.GetComponent<TextMeshProUGUI>().text = "+" + (activePlaceRoom.calculatedProfit - Mathf.Ceil(activePlaceRoom.baseProfit));
            conditionText.GetComponent<TextMeshProUGUI>().color = Color.green;
        }
        else if (activePlaceRoom.calculatedProfit < Mathf.Ceil(activePlaceRoom.baseProfit))
        {
            profitText.GetComponent<TextMeshProUGUI>().color = Color.red;
            conditionProfitNumText.GetComponent<TextMeshProUGUI>().color = Color.red;
            conditionProfitNumText.GetComponent<TextMeshProUGUI>().text = "" + (activePlaceRoom.calculatedProfit - Mathf.Ceil(activePlaceRoom.baseProfit));
            conditionText.GetComponent<TextMeshProUGUI>().color = Color.red;
        }
        else
        {
            profitText.GetComponent<TextMeshProUGUI>().color = Color.white;
            conditionProfitNumText.GetComponent<TextMeshProUGUI>().color = new Color(0.8f, 0.8f, 0.8f);
            conditionProfitNumText.GetComponent<TextMeshProUGUI>().text = "" + (activePlaceRoom.calculatedProfit - Mathf.Ceil(activePlaceRoom.baseProfit));
            conditionText.GetComponent<TextMeshProUGUI>().color = Color.white;
        }
        profitText.GetComponent<TextMeshProUGUI>().text = activePlaceRoom.calculatedProfit.ToString();
        conditionText.GetComponent<TextMeshProUGUI>().text = activePlaceRoom.condition.ToString();

        if (activePlaceRoom.attributeModifier > 0)
        {
            conditionExplanationNumText.GetComponent<TextMeshProUGUI>().color = Color.green;
            conditionExplanationNumText.GetComponent<TextMeshProUGUI>().text = "+" + activePlaceRoom.attributeModifier;
        }
        else if (activePlaceRoom.attributeModifier < 0)
        {
            conditionExplanationNumText.GetComponent<TextMeshProUGUI>().color = Color.red;
            conditionExplanationNumText.GetComponent<TextMeshProUGUI>().text = "" + activePlaceRoom.attributeModifier;
        }
        else
        {
            conditionExplanationNumText.GetComponent<TextMeshProUGUI>().color = new Color(0.8f,0.8f,0.8f);
            conditionExplanationNumText.GetComponent<TextMeshProUGUI>().text = "" + activePlaceRoom.attributeModifier;
        }
    }*/

    /*public Sprite DetermineAttributeSprite(RoomController room)
    {
        if (room.output.Equals("Foot Traffic"))
        {
            return allAttributeSprites[0];
        }
        else if (room.output.Equals("Noise"))
        {
            return allAttributeSprites[1];
        }
        else if (room.output.Equals("Food"))
        {
            return allAttributeSprites[2];
        }
        else if (room.output.Equals("Professional"))
        {
            return allAttributeSprites[3];
        }
        else if (room.output.Equals("Security"))
        {
            return allAttributeSprites[4];
        }
        else if (room.output.Equals("Criminal"))
        {
            return allAttributeSprites[5];
        }
        else
        {
            return null;
        }
    }*/

    /*public void ExitRoomZoom()
    {
        previousSelectedRoom = null;
    }*/

    /*IEnumerator UpdateRoomZoom()
    {
        if (activePlaceRoom.level < 3)
            {
                if (activePlaceRoom.roomBusinessTemplate.faction == "BRU")
                {
                    upgradeCost = 75 + (uiManager.bruRelationship * -1);
                    upgradeButtonText.GetComponent<TextMeshProUGUI>().text = "Upgrade room for " + upgradeCost + "$";
                    upgradeButtonDemolitionText.GetComponent<TextMeshProUGUI>().text = "Upgrade room for " + upgradeCost + "$";
                }
                else if (activePlaceRoom.roomBusinessTemplate.faction == "Law")
                {
                    upgradeCost = 75 + (uiManager.lawRelationship * -1);
                    upgradeButtonText.GetComponent<TextMeshProUGUI>().text = "Upgrade room for " + upgradeCost + "$";
                    upgradeButtonDemolitionText.GetComponent<TextMeshProUGUI>().text = "Upgrade room for " + upgradeCost + "$";
                }
                else if (activePlaceRoom.roomBusinessTemplate.faction == "Crime")
                {
                    upgradeCost = 75 + (uiManager.crimeRelationship * -1);
                    upgradeButtonText.GetComponent<TextMeshProUGUI>().text = "Upgrade room for " + upgradeCost + "$";
                    upgradeButtonDemolitionText.GetComponent<TextMeshProUGUI>().text = "Upgrade room for " + upgradeCost + "$";
                }
            }

        if (activePlaceRoom.calculatedProfit > Mathf.Ceil(activePlaceRoom.baseProfit))
        {
            profitText.GetComponent<TextMeshProUGUI>().color = Color.green;
            conditionProfitNumText.GetComponent<TextMeshProUGUI>().color = Color.green;
            conditionProfitNumText.GetComponent<TextMeshProUGUI>().text = "+" + (activePlaceRoom.calculatedProfit - Mathf.Ceil(activePlaceRoom.baseProfit));
            conditionText.GetComponent<TextMeshProUGUI>().color = Color.green;
        }
        else if (activePlaceRoom.calculatedProfit < Mathf.Ceil(activePlaceRoom.baseProfit))
        {
            profitText.GetComponent<TextMeshProUGUI>().color = Color.red;
            conditionProfitNumText.GetComponent<TextMeshProUGUI>().color = Color.red;
            conditionProfitNumText.GetComponent<TextMeshProUGUI>().text = "" + (activePlaceRoom.calculatedProfit - Mathf.Ceil(activePlaceRoom.baseProfit));
            conditionText.GetComponent<TextMeshProUGUI>().color = Color.red;
        }
        else
        {
            profitText.GetComponent<TextMeshProUGUI>().color = Color.white;
            conditionProfitNumText.GetComponent<TextMeshProUGUI>().color = new Color(0.8f, 0.8f, 0.8f);
            conditionProfitNumText.GetComponent<TextMeshProUGUI>().text = "" + (activePlaceRoom.calculatedProfit - Mathf.Ceil(activePlaceRoom.baseProfit));
            conditionText.GetComponent<TextMeshProUGUI>().color = Color.white;
        }
        profitText.GetComponent<TextMeshProUGUI>().text = activePlaceRoom.calculatedProfit.ToString();
        conditionText.GetComponent<TextMeshProUGUI>().text = activePlaceRoom.condition.ToString();
    }*/

    /*public void ConfirmRoomUpgrade()
    {
        if (uiManager.money < upgradeCost)
        {
            uiManager.CancelAction("Not enough ziggies!");
            return;
        }

        uiManager.money -= upgradeCost;

        activePlaceRoom.UpgradeRoom();

        roomLevelText.GetComponent<TextMeshProUGUI>().text = "lvl " + activePlaceRoom.level;

        if (activePlaceRoom.level == 3)
        {
            upgradeButtonText.GetComponent<TextMeshProUGUI>().text = "Room level maxed";
            upgradeButton.GetComponent<Button>().interactable = false;
            upgradeButtonDemolitionText.GetComponent<TextMeshProUGUI>().text = "Room level maxed";
            upgradeButtonDemolition.GetComponent<Button>().interactable = false;

        }
    }

    public void ConfirmRemoveRoom()
    {
        if (uiManager.money < removeRoomCost)
        {
            uiManager.CancelAction("Not enough ziggies!");
            return;
        }

        uiManager.money -= removeRoomCost;

        if (activePlaceRoom.roomBusinessTemplate.faction == "BRU")
        {
            uiManager.bruRelationship -= 25;
            if (uiManager.bruRelationship < -50)
            {
                uiManager.bruRelationship = -50;
            }
        }
        else if (activePlaceRoom.roomBusinessTemplate.faction == "Law")
        {
            uiManager.lawRelationship -= 25;
            if (uiManager.lawRelationship < -50)
            {
                uiManager.lawRelationship = -50;
            }
        }
        else if (activePlaceRoom.roomBusinessTemplate.faction == "Crime")
        {
            uiManager.crimeRelationship -= 25;
            if (uiManager.crimeRelationship < -50)
            {
                uiManager.crimeRelationship = -50;
            }
        }

        //uiManager.updateFacRelationships();

        if (activePlaceRoom.roomBusinessTemplate.name == "Pirat Petting Pen")
        {
            //activePlaceRoom.ratControl.SetPettingPenProfitReduction(false);
        }

        activePlaceRoom.RemoveRoom();
    }*/

    /*public void ShowChangeRoomUI()
    {
        foreach (Transform UI in roomZoom.transform)
        {
            UI.gameObject.SetActive(false);
        }

        changeRoomUI.SetActive(true);
        roomZoomVisible = false;

        currentChangeRoom = 0;
        changeUIRoomImage.GetComponent<Image>().sprite = pauseUI.demolitionTrayUI.GetComponent<DemolitionTrayController>().possibleRooms[currentChangeRoom].roomJPG;
        changeUIRoomNameText.GetComponent<TextMeshProUGUI>().text = pauseUI.demolitionTrayUI.GetComponent<DemolitionTrayController>().possibleRooms[currentChangeRoom].roomName;
    }

    public void ChangeRoomLeft()
    {
        if (currentChangeRoom == 0)
        {
            currentChangeRoom = pauseUI.demolitionTrayUI.GetComponent<DemolitionTrayController>().possibleRooms.Length - 1;
        }
        else
        {
            --currentChangeRoom;
        }

        changeUIRoomImage.GetComponent<Image>().sprite = pauseUI.demolitionTrayUI.GetComponent<DemolitionTrayController>().possibleRooms[currentChangeRoom].roomJPG;
        changeUIRoomNameText.GetComponent<TextMeshProUGUI>().text = pauseUI.demolitionTrayUI.GetComponent<DemolitionTrayController>().possibleRooms[currentChangeRoom].roomName;
    }

    public void ChangeRoomRight()
    {
        if (currentChangeRoom == pauseUI.demolitionTrayUI.GetComponent<DemolitionTrayController>().possibleRooms.Length - 1)
        {
            currentChangeRoom = 0;
        }
        else
        {
            ++currentChangeRoom;
        }

        changeUIRoomImage.GetComponent<Image>().sprite = pauseUI.demolitionTrayUI.GetComponent<DemolitionTrayController>().possibleRooms[currentChangeRoom].roomJPG;
        changeUIRoomNameText.GetComponent<TextMeshProUGUI>().text = pauseUI.demolitionTrayUI.GetComponent<DemolitionTrayController>().possibleRooms[currentChangeRoom].roomName;
    }

    public void ConfirmChangeRoom()
    {
        foreach (Transform UI in roomZoom.transform)
        {
            UI.gameObject.SetActive(true);
        }

        roomZoomVisible = true;
        removeUI.SetActive(false);
        upgradeButton.SetActive(false);
        
        removeButton.SetActive(false);

        placeRoomButton.SetActive(false);
        exitButton.SetActive(false);

        changeRoomUI.SetActive(false);

        if (uiManager.money < activePlaceRoom.roomBusinessTemplate.placementCost)
        {
            uiManager.CancelPurchase();
            return;
        }

        ExitRoomZoom();

        if (pauseUI.demolitionTrayUI.GetComponent<DemolitionTrayController>().possibleRooms[currentChangeRoom].faction == "BRU")
        {
            uiManager.money -= Mathf.Floor(pauseUI.demolitionTrayUI.GetComponent<DemolitionTrayController>().possibleRooms[currentChangeRoom].placementCost *
                (100 + (uiManager.bruRelationship * -1)) * 0.01f);
        }
        else if (pauseUI.demolitionTrayUI.GetComponent<DemolitionTrayController>().possibleRooms[currentChangeRoom].faction == "Law")
        {
            uiManager.money -= Mathf.Floor(pauseUI.demolitionTrayUI.GetComponent<DemolitionTrayController>().possibleRooms[currentChangeRoom].placementCost *
                (100 + (uiManager.lawRelationship * -1)) * 0.01f);
        }
        else if (pauseUI.demolitionTrayUI.GetComponent<DemolitionTrayController>().possibleRooms[currentChangeRoom].faction == "Crime")
        {
            uiManager.money -= Mathf.Floor(pauseUI.demolitionTrayUI.GetComponent<DemolitionTrayController>().possibleRooms[currentChangeRoom].placementCost *
                (100 + (uiManager.crimeRelationship * -1)) * 0.01f);
        }

        uiManager.UpdateMoney();

        activePlaceRoom.RemoveRoom();

        activePlaceRoom.roomBusinessTemplate = pauseUI.demolitionTrayUI.GetComponent<DemolitionTrayController>().possibleRooms[currentChangeRoom];
        activePlaceRoom.AddRoomBusiness();
        activePlaceRoom.roomPlacementsTillOpen = 2;
    }

    public void CancelChangeRoom()
    {
        foreach (Transform UI in roomZoom.transform)
        {
            UI.gameObject.SetActive(true);
        }

        roomZoomVisible = true;

        removeUI.SetActive(false);
        removeButton.SetActive(false);

        upgradeButton.SetActive(false);

        placeRoomButton.SetActive(false);
        exitButton.SetActive(false);

        changeRoomUI.SetActive(false);
    }*/

    /*public void ShowRoomUpgradeUI()
   {
       foreach (Transform UI in roomZoom.transform)
       {
           UI.gameObject.SetActive(false);
       }

       upgradeUI.SetActive(true);
       roomZoomVisible = false;

       if (activePlaceRoom.roomBusinessTemplate.faction == "BRU")
       {
           upgradeCost = 75 + (uiManager.bruRelationship * -1);
           upgradeButtonText.GetComponent<TextMeshProUGUI>().text = "Upgrade room for " + upgradeCost + "$?";
       }
       else if (activePlaceRoom.roomBusinessTemplate.faction == "Law")
       {
           upgradeCost = 75 + (uiManager.lawRelationship * -1);
           upgradeButtonText.GetComponent<TextMeshProUGUI>().text = "Upgrade room for " + upgradeCost + "$?";
       }
       else if (activePlaceRoom.roomBusinessTemplate.faction == "Crime")
       {
           upgradeCost = 75 + (uiManager.crimeRelationship * -1);
           upgradeButtonText.GetComponent<TextMeshProUGUI>().text = "Upgrade room for " + upgradeCost + "$?";
       }
   }*/

    /*public void CancelRoomUpgrade()
     {
         foreach (Transform UI in roomZoom.transform)
         {
             UI.gameObject.SetActive(true);
         }

         roomZoomVisible = true;

         removeUI.SetActive(false);
         upgradeUI.SetActive(false);
         changeRoomUI.SetActive(false);
         placeRoomButton.SetActive(false);
         exitButton.SetActive(false);       

         if (GameController.demolitionChosen)
         {
             removeButton.SetActive(false);
         }
         else
         {
             removeButtonDemolition.SetActive(false);
             changeRoomButton.SetActive(false);
         }
     }*/

    /*public void ShowRemoveUI()
    {
        foreach (Transform UI in roomZoom.transform)
        {
            UI.gameObject.SetActive(false);
        }

        removeUI.SetActive(true);
        roomZoomVisible = false;

        if (activePlaceRoom.roomBusinessTemplate.roomName == "Pirat Petting Pen")
        {
            removeConfirmationText.GetComponent<TextMeshProUGUI>().text = "Remove room for 500$?";
            removeRoomCost = 500f;
        }
        else
        {
            removeConfirmationText.GetComponent<TextMeshProUGUI>().text = "Remove room for 500$?";
            removeRoomCost = 500f;
        }
    }*/

    /*public void CancelRemoveRoom()
    {
        foreach (Transform UI in roomZoom.transform)
        {
            UI.gameObject.SetActive(true);
        }

        removeUI.SetActive(false);
        upgradeUI.SetActive(false);
        changeRoomUI.SetActive(false);

        roomZoomVisible = true;

        placeRoomButton.SetActive(false);
        exitButton.SetActive(false);

        if (GameController.demolitionChosen)
        {
            removeButton.SetActive(false);
        }
        else
        {
            removeButtonDemolition.SetActive(false);
            changeRoomButton.SetActive(false);
        }
    }*/
}
