using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class RerollTrayController : MonoBehaviour
{
    public RoomCreation room1;
    public RoomCreation room2;
    public RoomCreation room3;
    public RoomCreation room4;

    public Button room1image;
    public Button room2image;
    public Button room3image;
    public Button room4image;

    public GameObject room1NameText;
    public GameObject room2NameText;
    public GameObject room3NameText;
    public GameObject room4NameText;
    public GameObject room1CostText;
    public GameObject room2CostText;
    public GameObject room3CostText;
    public GameObject room4CostText;
    public GameObject room1FactionText;
    public GameObject room2FactionText;
    public GameObject room3FactionText;
    public GameObject room4FactionText;
    public GameObject room1FactionModifierText;
    public GameObject room2FactionModifierText;
    public GameObject room3FactionModifierText;
    public GameObject room4FactionModifierText;

    private int randomInt;
    private float tempCost;

    public bool firstRoll = true;
    public Button rerollButton;

    public static RoomCreation currentBusinessSelected;
    public UIManager uiManager;

    public RoomCreation[] possibleRooms;

    public AudioClip[] money;
    private AudioSource audioSource;

    private void Start()
    {
        //This reroll call sets up all the rooms available to the player
        Reroll();
        rerollButton.onClick.AddListener(Reroll);

        currentBusinessSelected = room1;
        StartCoroutine("UpdateRoom1Cost");
        StartCoroutine("UpdateRoom2Cost");
        StartCoroutine("UpdateRoom3Cost");
        StartCoroutine("UpdateRoom4Cost");

        audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        //print("MouseReleased: " + ItemDragHandler.mouseReleased);
        //print("Placed Room: " + RoomController.placedRoom);
        
        //These if statements are only used to choose a new random room scriptable object for each tray slot when the previous room is placed
        if (ItemDragHandler.roomNum.CompareTo('1') == 0 && ItemDragHandler.mouseReleasedTray && RoomController.placedRoom)
        {
            //print('1');
            RoomController.placedRoom = false;
            ItemDragHandler.mouseReleasedTray = false;
            ChangeRooms1();
        }
        if (ItemDragHandler.roomNum.CompareTo('2') == 0 && ItemDragHandler.mouseReleasedTray && RoomController.placedRoom)
        {
            //print('2');
            RoomController.placedRoom = false;
            ItemDragHandler.mouseReleasedTray = false;
            ChangeRooms2();
        }
        if (ItemDragHandler.roomNum.CompareTo('3') == 0 && ItemDragHandler.mouseReleasedTray && RoomController.placedRoom)
        {
            //print('2');
            RoomController.placedRoom = false;
            ItemDragHandler.mouseReleasedTray = false;
            ChangeRooms3();
        }
        if (ItemDragHandler.roomNum.CompareTo('4') == 0 && ItemDragHandler.mouseReleasedTray && RoomController.placedRoom)
        {
            //print('2');
            RoomController.placedRoom = false;
            ItemDragHandler.mouseReleasedTray = false;
            ChangeRooms4();
        }
    }

    public void Reroll()
    {
        //This if statement is solely used to set up the reroll UI when the level starts and is needed to avoid charging the player since they didn't press reroll
        if (firstRoll)
        {
            firstRoll = false;
        }
        //Charges the player the cost of reroll or cancels their payment if they don't have enough money
        else
        {
            /*if(uiManager.money >= 100)
            {
                uiManager.money -= 100;
                uiManager.currentProfitOverSixSeconds -= 100;
                uiManager.UpdateMoney();
            }
            else
            {
                uiManager.CancelAction("Not enough ziggies!");
                return;
            }*/

            int randInt = Random.Range(0, 2);
            audioSource.PlayOneShot(money[randInt]);
        }   

        ChangeRooms1();
        ChangeRooms2();
        ChangeRooms3();
        ChangeRooms4();
    }

    //These functions actually change the rooms available in each tray slot and are either called during reroll or after a room has been placed
    public void ChangeRooms1()
    {
        randomInt = Random.Range(0, possibleRooms.Length);
        room1 = possibleRooms[randomInt];
        room1image.GetComponent<Image>().sprite = room1.roomJPG;
        room1NameText.GetComponent<TextMeshProUGUI>().text = room1.roomName;

        if (room1.faction == "BRU")
        {
            room1FactionText.GetComponent<TextMeshProUGUI>().text = "Faction: BRU";
            tempCost = Mathf.Floor(room1.placementCost * (100 + (uiManager.bruRelationship * -1)) * 0.01f);
            room1CostText.GetComponent<TextMeshProUGUI>().text = "Cost: " + tempCost;
        }
        else if (room1.faction == "Law")
        {
            room1FactionText.GetComponent<TextMeshProUGUI>().text = "Faction: Law";
            tempCost = Mathf.Floor(room1.placementCost * (100 + (uiManager.lawRelationship * -1)) * 0.01f);
            room1CostText.GetComponent<TextMeshProUGUI>().text = "Cost: " + tempCost;
        }
        else if (room1.faction == "Crime")
        {
            room1FactionText.GetComponent<TextMeshProUGUI>().text = "Faction: Crime";
            tempCost = Mathf.Floor(room1.placementCost * (100 + (uiManager.crimeRelationship * -1)) * 0.01f);
            room1CostText.GetComponent<TextMeshProUGUI>().text = "Cost: " + tempCost;
        }


        if (tempCost - room1.placementCost > 0)
        {
            room1FactionModifierText.GetComponent<TextMeshProUGUI>().color = Color.red;
            room1FactionModifierText.GetComponent<TextMeshProUGUI>().text = "+" + (tempCost - room1.placementCost);
        }
        else if (tempCost - room1.placementCost < 0)
        {
            room1FactionModifierText.GetComponent<TextMeshProUGUI>().color = Color.green;
            room1FactionModifierText.GetComponent<TextMeshProUGUI>().text = "" + (tempCost - room1.placementCost);
        }
        else
        {
            room1FactionModifierText.GetComponent<TextMeshProUGUI>().color = Color.white;
            room1FactionModifierText.GetComponent<TextMeshProUGUI>().text = "" + (tempCost - room1.placementCost);
        }
    }

    public void ChangeRooms2()
    {
        randomInt = Random.Range(0, possibleRooms.Length);
        room2 = possibleRooms[randomInt];
        room2image.GetComponent<Image>().sprite = room2.roomJPG;
        room2NameText.GetComponent<TextMeshProUGUI>().text = room2.roomName;

        if (room2.faction == "BRU")
        {
            room2FactionText.GetComponent<TextMeshProUGUI>().text = "Faction: BRU";
            tempCost = Mathf.Floor(room2.placementCost * (100 + (uiManager.bruRelationship * -1)) * 0.01f);
            room2CostText.GetComponent<TextMeshProUGUI>().text = "Cost: " + tempCost;
        }
        else if (room2.faction == "Law")
        {
            room2FactionText.GetComponent<TextMeshProUGUI>().text = "Faction: Law";
            tempCost = Mathf.Floor(room2.placementCost * (100 + (uiManager.lawRelationship * -1)) * 0.01f);
            room2CostText.GetComponent<TextMeshProUGUI>().text = "Cost: " + tempCost;
        }
        else if (room1.faction == "Crime")
        {
            room2FactionText.GetComponent<TextMeshProUGUI>().text = "Faction: Crime";
            tempCost = Mathf.Floor(room2.placementCost * (100 + (uiManager.crimeRelationship * -1)) * 0.01f);
            room2CostText.GetComponent<TextMeshProUGUI>().text = "Cost: " + tempCost;
        }

        if (tempCost - room2.placementCost > 0)
        {
            room2FactionModifierText.GetComponent<TextMeshProUGUI>().color = Color.red;
            room2FactionModifierText.GetComponent<TextMeshProUGUI>().text = "+" + (tempCost - room2.placementCost);
        }
        else if (tempCost - room2.placementCost < 0)
        {
            room2FactionModifierText.GetComponent<TextMeshProUGUI>().color = Color.green;
            room2FactionModifierText.GetComponent<TextMeshProUGUI>().text = "" + (tempCost - room2.placementCost);
        }
        else
        {
            room2FactionModifierText.GetComponent<TextMeshProUGUI>().color = Color.white;
            room2FactionModifierText.GetComponent<TextMeshProUGUI>().text = "" + (tempCost - room2.placementCost);
        }
    }

    public void ChangeRooms3()
    {
        randomInt = Random.Range(0, possibleRooms.Length);
        room3 = possibleRooms[randomInt];
        room3image.GetComponent<Image>().sprite = room3.roomJPG;
        room3NameText.GetComponent<TextMeshProUGUI>().text = room3.roomName;

        if (room3.faction == "BRU")
        {
            room3FactionText.GetComponent<TextMeshProUGUI>().text = "Faction: BRU";
            tempCost = Mathf.Floor(room3.placementCost * (100 + (uiManager.bruRelationship * -1)) * 0.01f);
            room3CostText.GetComponent<TextMeshProUGUI>().text = "Cost: " + tempCost;
        }
        else if (room3.faction == "Law")
        {
            room3FactionText.GetComponent<TextMeshProUGUI>().text = "Faction: Law";
            tempCost = Mathf.Floor(room3.placementCost * (100 + (uiManager.lawRelationship * -1)) * 0.01f);
            room3CostText.GetComponent<TextMeshProUGUI>().text = "Cost: " + tempCost;
        }
        else if (room3.faction == "Crime")
        {
            room3FactionText.GetComponent<TextMeshProUGUI>().text = "Faction: Crime";
            tempCost = Mathf.Floor(room3.placementCost * (100 + (uiManager.crimeRelationship * -1)) * 0.01f);
            room3CostText.GetComponent<TextMeshProUGUI>().text = "Cost: " + tempCost;
        }

        if (tempCost - room3.placementCost > 0)
        {
            room3FactionModifierText.GetComponent<TextMeshProUGUI>().color = Color.red;
            room3FactionModifierText.GetComponent<TextMeshProUGUI>().text = "+" + (tempCost - room3.placementCost);
        }
        else if (tempCost - room3.placementCost < 0)
        {
            room3FactionModifierText.GetComponent<TextMeshProUGUI>().color = Color.green;
            room3FactionModifierText.GetComponent<TextMeshProUGUI>().text = "" + (tempCost - room3.placementCost);
        }
        else
        {
            room3FactionModifierText.GetComponent<TextMeshProUGUI>().color = Color.white;
            room3FactionModifierText.GetComponent<TextMeshProUGUI>().text = "" + (tempCost - room3.placementCost);
        }
    }

    public void ChangeRooms4()
    {
        randomInt = Random.Range(0, possibleRooms.Length);
        room4 = possibleRooms[randomInt];
        room4image.GetComponent<Image>().sprite = room4.roomJPG;
        room4NameText.GetComponent<TextMeshProUGUI>().text = room4.roomName;

        if (room4.faction == "BRU")
        {
            room4FactionText.GetComponent<TextMeshProUGUI>().text = "Faction: BRU";
            tempCost = Mathf.Floor(room4.placementCost * (100 + (uiManager.bruRelationship * -1)) * 0.01f);
            room4CostText.GetComponent<TextMeshProUGUI>().text = "Cost: " + tempCost;
        }
        else if (room4.faction == "Law")
        {
            room4FactionText.GetComponent<TextMeshProUGUI>().text = "Faction: Law";
            tempCost = Mathf.Floor(room4.placementCost * (100 + (uiManager.lawRelationship * -1)) * 0.01f);
            room4CostText.GetComponent<TextMeshProUGUI>().text = "Cost: " + tempCost;
        }
        else if (room4.faction == "Crime")
        {
            room4FactionText.GetComponent<TextMeshProUGUI>().text = "Faction: Crime";
            tempCost = Mathf.Floor(room4.placementCost * (100 + (uiManager.crimeRelationship * -1)) * 0.01f);
            room4CostText.GetComponent<TextMeshProUGUI>().text = "Cost: " + tempCost;
        }

        if (tempCost - room4.placementCost > 0)
        {
            room4FactionModifierText.GetComponent<TextMeshProUGUI>().color = Color.red;
            room4FactionModifierText.GetComponent<TextMeshProUGUI>().text = "+" + (tempCost - room4.placementCost);
        }
        else if (tempCost - room4.placementCost < 0)
        {
            room4FactionModifierText.GetComponent<TextMeshProUGUI>().color = Color.green;
            room4FactionModifierText.GetComponent<TextMeshProUGUI>().text = "" + (tempCost - room4.placementCost);
        }
        else
        {
            room4FactionModifierText.GetComponent<TextMeshProUGUI>().color = Color.white;
            room4FactionModifierText.GetComponent<TextMeshProUGUI>().text = "" + (tempCost - room4.placementCost);
        }
    }

    //The following functions update the costs on the tray UI for each room in the tray
    IEnumerator UpdateRoom1Cost()
    {
        yield return new WaitForSeconds(1f);

        if (room1.faction == "BRU")
        {
            room1FactionText.GetComponent<TextMeshProUGUI>().text = "Faction: BRU";
            tempCost = Mathf.Floor(room1.placementCost * (100 + (uiManager.bruRelationship * -1)) * 0.01f);
            room1CostText.GetComponent<TextMeshProUGUI>().text = "Cost: " + tempCost;
        }
        else if (room1.faction == "Law")
        {
            room1FactionText.GetComponent<TextMeshProUGUI>().text = "Faction: Law";
            tempCost = Mathf.Floor(room1.placementCost * (100 + (uiManager.lawRelationship * -1)) * 0.01f);
            room1CostText.GetComponent<TextMeshProUGUI>().text = "Cost: " + tempCost;
        }
        else if (room1.faction == "Crime")
        {
            room1FactionText.GetComponent<TextMeshProUGUI>().text = "Faction: Crime";
            tempCost = Mathf.Floor(room1.placementCost * (100 + (uiManager.crimeRelationship * -1)) * 0.01f);
            room1CostText.GetComponent<TextMeshProUGUI>().text = "Cost: " + tempCost;
        }


        if (tempCost - room1.placementCost > 0)
        {
            room1FactionModifierText.GetComponent<TextMeshProUGUI>().color = Color.red;
            room1FactionModifierText.GetComponent<TextMeshProUGUI>().text = "+" + (tempCost - room1.placementCost);
        }
        else if (tempCost - room1.placementCost < 0)
        {
            room1FactionModifierText.GetComponent<TextMeshProUGUI>().color = Color.green;
            room1FactionModifierText.GetComponent<TextMeshProUGUI>().text = "" + (tempCost - room1.placementCost);
        }
        else
        {
            room1FactionModifierText.GetComponent<TextMeshProUGUI>().color = Color.white;
            room1FactionModifierText.GetComponent<TextMeshProUGUI>().text = "" + (tempCost - room1.placementCost);
        }

        StartCoroutine("UpdateRoom1Cost");
    }

    IEnumerator UpdateRoom2Cost()
    {
        yield return new WaitForSeconds(1f);

        if (room2.faction == "BRU")
        {
            room2FactionText.GetComponent<TextMeshProUGUI>().text = "Faction: BRU";
            tempCost = Mathf.Floor(room2.placementCost * (100 + (uiManager.bruRelationship * -1)) * 0.01f);
            room2CostText.GetComponent<TextMeshProUGUI>().text = "Cost: " + tempCost;
        }
        else if (room2.faction == "Law")
        {
            room2FactionText.GetComponent<TextMeshProUGUI>().text = "Faction: Law";
            tempCost = Mathf.Floor(room2.placementCost * (100 + (uiManager.lawRelationship * -1)) * 0.01f);
            room2CostText.GetComponent<TextMeshProUGUI>().text = "Cost: " + tempCost;
        }
        else if (room2.faction == "Crime")
        {
            room2FactionText.GetComponent<TextMeshProUGUI>().text = "Faction: Crime";
            tempCost = Mathf.Floor(room2.placementCost * (100 + (uiManager.crimeRelationship * -1)) * 0.01f);
            room2CostText.GetComponent<TextMeshProUGUI>().text = "Cost: " + tempCost;
        }

        if (tempCost - room2.placementCost > 0)
        {
            room2FactionModifierText.GetComponent<TextMeshProUGUI>().color = Color.red;
            room2FactionModifierText.GetComponent<TextMeshProUGUI>().text = "+" + (tempCost - room2.placementCost);
        }
        else if (tempCost - room2.placementCost < 0)
        {
            room2FactionModifierText.GetComponent<TextMeshProUGUI>().color = Color.green;
            room2FactionModifierText.GetComponent<TextMeshProUGUI>().text = "" + (tempCost - room2.placementCost);
        }
        else
        {
            room2FactionModifierText.GetComponent<TextMeshProUGUI>().color = Color.white;
            room2FactionModifierText.GetComponent<TextMeshProUGUI>().text = "" + (tempCost - room2.placementCost);
        }

        StartCoroutine("UpdateRoom2Cost");
    }

    IEnumerator UpdateRoom3Cost()
    {
        yield return new WaitForSeconds(1f);

        if (room3.faction == "BRU")
        {
            room3FactionText.GetComponent<TextMeshProUGUI>().text = "Faction: BRU";
            tempCost = Mathf.Floor(room3.placementCost * (100 + (uiManager.bruRelationship * -1)) * 0.01f);
            room3CostText.GetComponent<TextMeshProUGUI>().text = "Cost: " + tempCost;
        }
        else if (room3.faction == "Law")
        {
            room3FactionText.GetComponent<TextMeshProUGUI>().text = "Faction: Law";
            tempCost = Mathf.Floor(room3.placementCost * (100 + (uiManager.lawRelationship * -1)) * 0.01f);
            room3CostText.GetComponent<TextMeshProUGUI>().text = "Cost: " + tempCost;
        }
        else if (room3.faction == "Crime")
        {
            room3FactionText.GetComponent<TextMeshProUGUI>().text = "Faction: Crime";
            tempCost = Mathf.Floor(room3.placementCost * (100 + (uiManager.crimeRelationship * -1)) * 0.01f);
            room3CostText.GetComponent<TextMeshProUGUI>().text = "Cost: " + tempCost;
        }

        if (tempCost - room3.placementCost > 0)
        {
            room3FactionModifierText.GetComponent<TextMeshProUGUI>().color = Color.red;
            room3FactionModifierText.GetComponent<TextMeshProUGUI>().text = "+" + (tempCost - room3.placementCost);
        }
        else if (tempCost - room3.placementCost < 0)
        {
            room3FactionModifierText.GetComponent<TextMeshProUGUI>().color = Color.green;
            room3FactionModifierText.GetComponent<TextMeshProUGUI>().text = "" + (tempCost - room3.placementCost);
        }
        else
        {
            room3FactionModifierText.GetComponent<TextMeshProUGUI>().color = Color.white;
            room3FactionModifierText.GetComponent<TextMeshProUGUI>().text = "" + (tempCost - room3.placementCost);
        }

        StartCoroutine("UpdateRoom3Cost");
    }

    IEnumerator UpdateRoom4Cost()
    {
        yield return new WaitForSeconds(1f);

        if (room4.faction == "BRU")
        {
            room4FactionText.GetComponent<TextMeshProUGUI>().text = "Faction: BRU";
            tempCost = Mathf.Floor(room4.placementCost * (100 + (uiManager.bruRelationship * -1)) * 0.01f);
            room4CostText.GetComponent<TextMeshProUGUI>().text = "Cost: " + tempCost;
        }
        else if (room4.faction == "Law")
        {
            room4FactionText.GetComponent<TextMeshProUGUI>().text = "Faction: Law";
            tempCost = Mathf.Floor(room4.placementCost * (100 + (uiManager.lawRelationship * -1)) * 0.01f);
            room4CostText.GetComponent<TextMeshProUGUI>().text = "Cost: " + tempCost;
        }
        else if (room4.faction == "Crime")
        {
            room4FactionText.GetComponent<TextMeshProUGUI>().text = "Faction: Crime";
            tempCost = Mathf.Floor(room4.placementCost * (100 + (uiManager.crimeRelationship * -1)) * 0.01f);
            room4CostText.GetComponent<TextMeshProUGUI>().text = "Cost: " + tempCost;
        }

        if (tempCost - room4.placementCost > 0)
        {
            room4FactionModifierText.GetComponent<TextMeshProUGUI>().color = Color.red;
            room4FactionModifierText.GetComponent<TextMeshProUGUI>().text = "+" + (tempCost - room4.placementCost);
        }
        else if (tempCost - room4.placementCost < 0)
        {
            room4FactionModifierText.GetComponent<TextMeshProUGUI>().color = Color.green;
            room4FactionModifierText.GetComponent<TextMeshProUGUI>().text = "" + (tempCost - room4.placementCost);
        }
        else
        {
            room4FactionModifierText.GetComponent<TextMeshProUGUI>().color = Color.white;
            room4FactionModifierText.GetComponent<TextMeshProUGUI>().text = "" + (tempCost - room4.placementCost);
        }

        StartCoroutine("UpdateRoom4Cost");
    }

    //Cycles through the possibleRooms array to the left, and chooses the current placeable room (using the RoomCreation class)
    /*public void clickedLeftArrow()
    {
        currentRoomNum -= 1;
        if (currentRoomNum < 0)
        {
            currentRoomNum = possibleRooms.Length - 1;
        }

        room1 = possibleRooms[currentRoomNum];
        room1image.GetComponent<Image>().sprite = possibleRooms[currentRoomNum].roomJPG;
        room1text.text = possibleRooms[currentRoomNum].roomName;

        currentBusinessSelected = room1;
    }

    //Cycles through the possibleRooms array to the right, and chooses the current placeable room (using the RoomCreation class)
    public void clickedRightArrow()
    {
        currentRoomNum += 1;
        if (currentRoomNum > possibleRooms.Length - 1)
        {
            currentRoomNum = 0;
        }

        room1 = possibleRooms[currentRoomNum];
        room1image.GetComponent<Image>().sprite = possibleRooms[currentRoomNum].roomJPG;
        room1text.text = possibleRooms[currentRoomNum].roomName;

        currentBusinessSelected = room1;
    }*/

    /*public void clickedRoom1()
    {
        
        currentBusinessSelected = room1;
        
    }*/
}
