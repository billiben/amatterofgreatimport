﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

public class UIAppSubstateManager : MonoBehaviour
{
    public Sprite interactionsLog; // sprite for the background/template of the interactions log
    public Sprite relationshipsLog; //sprite for the background/template of the relationships log
    /// </summary>


    public Button firstTab;//first tab, top button
    public Button secondTab;//second tab, middle button
    public Button thirdTab;//third tab on phone, bottom button
    public Button trayButton;//button object for the tray
    public Button generalTrayButton;

    public GameStateManager gameState;
    public UIManager uiManager;
    public Image activeTabBackground;//image of the background representing the active open app tab
    public TextMeshProUGUI logText;
    // Update is called once per frame


    private void Start()
    {
        this.generalTrayButton.onClick.AddListener(setTrayActiveFromGeneral);
    }

    void setTrayActiveFromGeneral()
    {
        this.activeTabBackground.gameObject.SetActive(false);
        this.logText.gameObject.SetActive(false);
        this.trayButton.gameObject.SetActive(true);
        this.trayButton.onClick.AddListener(placeRoom);
        this.setActiveSubstate("manager");
        //this.uiManager.currentUIState = "managerMenu";
        this.generalTrayButton.gameObject.SetActive(false);
    }
    void Update()
    {
        
    }

    public void setActiveSubstate(string substate)//function called from overall ui manager, takes a substate string and sets the tab names active to that substate application
    {
        this.activeAllTabs(true);

        if(substate == "zuckabook")
        {
            this.firstTab.GetComponentInChildren<TextMeshProUGUI>().SetText("Faction Relationships");
            this.secondTab.GetComponentInChildren<TextMeshProUGUI>().SetText("Terrestrial/Nonterrestrial Acquaintances");
            this.thirdTab.GetComponentInChildren<TextMeshProUGUI>().SetText("Whatever the Third Tab is");
            this.removeAllListeners();
            this.firstTab.onClick.AddListener(setRelationshipsActive);
            this.secondTab.onClick.AddListener(setSocialMediaActive);
            //this.thirdTab.onClick.AddListener();
        }
        else if(substate == "manager")
        {
            this.firstTab.GetComponentInChildren<TextMeshProUGUI>().SetText("Room Glossary");
            this.secondTab.GetComponentInChildren<TextMeshProUGUI>().SetText("Interactions Log");
            this.thirdTab.GetComponentInChildren<TextMeshProUGUI>().SetText("Tray");
            this.removeAllListeners();
            this.firstTab.onClick.AddListener(setGlossaryActive);
            this.secondTab.onClick.AddListener(setInteractionsLogActive);
            this.thirdTab.onClick.AddListener(setTrayActive);
        }
        else if(substate == "minigames")
        {
            this.firstTab.GetComponentInChildren<TextMeshProUGUI>().SetText("MINIGAME01");
            this.secondTab.GetComponentInChildren<TextMeshProUGUI>().SetText("MINIGAME02");
            this.thirdTab.GetComponentInChildren<TextMeshProUGUI>().SetText("MINIGAME03");
            this.removeAllListeners();
            this.firstTab.onClick.AddListener(setMinigame01Active);
            this.secondTab.onClick.AddListener(setMinigame02Active);
            //this.thirdTab.onClick.AddListener();
        }
    }

    void removeAllListeners()//function to clear all the listeners on the tabs
    {
        this.firstTab.onClick.RemoveAllListeners();
        this.secondTab.onClick.RemoveAllListeners();
        this.thirdTab.onClick.RemoveAllListeners();
    }
    public void setBackgroundActive(bool state)//takes a state and sets the background, active template active or not
    {
        this.activeTabBackground.gameObject.SetActive(state);
    }
    public void activeAllTabs(bool state)//takes a state and sets all the tabs active or not
    {
        this.firstTab.gameObject.SetActive(state);
        this.secondTab.gameObject.SetActive(state);
        this.thirdTab.gameObject.SetActive(state);
    }
    void setRelationshipsActive()//sets the active tab to faction relationships, sets background active
    {
        this.activeTabBackground.sprite = relationshipsLog;
        this.setBackgroundActive(true);
    }
    void setSocialMediaActive()//sets the active tab to full zuckabook, sets background active
    {

    }
    void setGlossaryActive()//sets the active tab to room glossary sets background active
    {

    }
    void setInteractionsLogActive()//sets the active tab to interactions log, sets background active
    {
        this.activeTabBackground.sprite = interactionsLog;
        this.logText.gameObject.SetActive(true);
        this.logText.text = "Law room placed, no environment flags detected";
        this.trayButton.gameObject.SetActive(false);
        this.setBackgroundActive(true);
    }

    void setTrayActive()
    {
        this.activeTabBackground.gameObject.SetActive(false);
        this.logText.gameObject.SetActive(false);
        this.trayButton.gameObject.SetActive(true);
        this.trayButton.onClick.AddListener(placeRoom);

    }
    void setMinigame01Active()//sets the active tab to the first mingame, sets background active
    {

    }

    public void placeRoom()
    {
        this.setSubstate(false);
        this.trayButton.gameObject.SetActive(false);
    }
    void setMinigame02Active()
    {

    }

    public void setSubstate(bool state)
    {
        this.firstTab.gameObject.SetActive(state);
        this.secondTab.gameObject.SetActive(state);
        this.thirdTab.gameObject.SetActive(state);
        this.setBackgroundActive(state);
    }
    //minor change that doesnt matteer
}
