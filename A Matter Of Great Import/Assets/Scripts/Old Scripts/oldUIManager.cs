﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class oldUIManager : MonoBehaviour
{
    /*public string currentUIState; // string represnting the overall current state of the UI
    public string currentUISubstate;//string representing the substate of the UI in the phone

    public gameStateManager gameState;
    public valueManager valueManager;//overall values of the game
    public UIAppSubstateManager substateManager;//substate manager for the active apps

    public Canvas generalUI;//canvas of the UI for general gameplay
    public Canvas phoneUI;//canvas of UI for phone interface

    public Image phoneBackground;//image of the phone itself as the background

    public TextMeshProUGUI profit;
    public TextMeshProUGUI bottomLine;
    public TextMeshProUGUI feedbackText;

    public bool isUIUpdated;//boolean to track if the UI needs to be updated or it can sit

    public Slider BRU;
    public Slider law;
    public Slider crime;
    public Slider money;

    public Button phoneIcon;
    public Button zuckabookIcon;
    public Button managerIcon;
    public Button minigamesIcon;
    public Button exitButton;//button to exit the phone menu back to the general UI
    public Button backButton;
    public Button confirmButton;
    public Button denyButton;

    public GameObject moneyText;

    public static bool isFirstRoomPlaced;
    private bool firstBRURoom;
    private bool firstLawRoom;
    private bool firstCrimeRoom;

    public GameObject sixSecondProfitText;
    public float currentProfitOverSixSeconds;

    public float money;
    public float currentBRURoomProfits;
    public float currentLawRoomProfits;
    public float currentCrimeRoomProfits;
    public float bruDividend;
    public float lawDividend;
    public float crimeDividend;
    private int bruDividendCounter;
    private int lawDividendCounter;
    private int crimeDividendCounter;

    //Room analytics UI variables
    //public GameObject analyticsButton;
    public GameObject centerOfFloor;
    public int cameraFOVMultiplier;
    public bool profitAnalyticUIVisible;
    public bool roomAttributeAnalyticUIVisible;
    private Vector3 mainCameraPreviousPosition;

    public float timeTillGameOver;

    //Rat system variables
    public GameObject ratCleanupUI;
    public GameObject yesButton;
    public GameObject noButton;
    public GameObject situationText;
    public GameObject currentRatRoom;

    private void Start()//on start the listeners for buttons are added, and profit/bottom line are initially updated
    {
        phoneIcon.onClick.AddListener(openPhoneMenu);
        zuckabookIcon.onClick.AddListener(openZuckabook);
        managerIcon.onClick.AddListener(openManager);
        minigamesIcon.onClick.AddListener(openMinigames);
        exitButton.onClick.AddListener(exitMenu);
        backButton.onClick.AddListener(goBack);
        updateBottomLine();
        updateProfit();
        this.currentUIState = "general";
    }
    // Update is called once per frame
    void Update()
    {
        //If the player has debt, then a timer starts. If the timer lasts 12 seconds before they are in the positive again, the game is over and the pause ui is brought up.
        if (money < 0)
        {
            timeTillGameOver += Time.deltaTime;

            if (timeTillGameOver >= 12)
            {
                money = 1;
                pauseUI.GameOver();
            }
        }

        //If the timer that counts down game over has started but the overall money is now positive, reset the timer to 0
        if (money >= 0 && timeTillGameOver > 0)
        {
            timeTillGameOver = 0;
        }

        money.value = valueManager.GetComponent<valueManager>().money;
        this.updateUI(currentUIState, isUIUpdated);
    }
    public void goBack()
    {
        if(this.currentUIState == "phoneMenu")
        {
            this.exitMenu();
            this.currentUIState = "general";
        }
        else if(this.currentUIState == "zuckabookMenu" || this.currentUIState == "managerMenu" || this.currentUIState == "minigamesMenu")
        {
            this.setPhoneState();
            this.currentUIState = "phoneMenu";
        }
    }

    public void changeUISubstate(string newSubstate)
    {
        this.currentUISubstate = newSubstate;
    }

    void updateUI(string state, bool updated)//function that takes the ui state and whether it has been updated recently, and acts accordingly
    {
        if (!updated)
        {
            if (state == "general")
            {
                this.setGeneralState();
                this.updateProfit();
                this.updateBottomLine();
                this.isUIUpdated = false;
                if(gameState.gameState =="place room")
                {
                    this.currentUIState = "placeRoom";
                }
            }
            else if (state == "phoneMenu")
            {
                this.setPhoneState();
            }
            else if (state == "zuckabookMenu")
            {
                setAppState("zuckabook");
            }
            else if (state == "managerMenu")
            {
                setAppState("manager");
            }
            else if (state == "minigamesMenu")
            {
                setAppState("minigames");
            }
            else if (state == "placeRoom")
            {
               //print("buttonshouldappear");
                this.denyButton.gameObject.SetActive(true);
                this.denyButton.onClick.AddListener(roomDenied);
            }

            else if(state == "checkRoom")
            {
                this.feedbackText.gameObject.SetActive(true);
                this.confirmButton.gameObject.SetActive(true);
                this.confirmButton.onClick.AddListener(roomConfirmed);
            }
        }
    }

    void roomConfirmed()
    {
        this.currentUIState = "general";
        this.feedbackText.gameObject.SetActive(false);
        this.denyButton.gameObject.SetActive(false);
        this.confirmButton.gameObject.SetActive(false);
    }
    void roomDenied()
    {
        this.currentUIState = "general";
        gameState.GetComponent<gameStateManager>().activePlaceRoom.GetComponent<roomController>().removeRoom();
        this.feedbackText.gameObject.SetActive(false);
        this.denyButton.gameObject.SetActive(false);
        this.confirmButton.gameObject.SetActive(false);
    }
    void updateProfit()//function to update the profit variable
    {
        this.profit.text = this.valueManager.getMoney().ToString();
    }
    void updateBottomLine()//function to update the bottom line variable in the UI
    {
        this.bottomLine.text = this.valueManager.getBottomLine().ToString();
    }
    void openPhoneMenu()
    {
        this.currentUIState = "phoneMenu";
        this.isUIUpdated = false;
    }
    void openZuckabook()
    {
        this.currentUIState = "zuckabookMenu";
        this.isUIUpdated = false;
    }
    void openManager()
    {
        this.currentUIState = "managerMenu";
        this.isUIUpdated = false;
    }
    void openMinigames()
    {
        this.currentUIState = "minigamesMenu";
        this.isUIUpdated = false;
    }
    public void exitMenu()//function to exit all phone ui back to the general interface
    {
        this.currentUIState = "general";
        this.substateManager.activeAllTabs(false);
        this.substateManager.setBackgroundActive(false);
        this.substateManager.trayButton.gameObject.SetActive(false);
        this.isUIUpdated = false;
    }
    void setGeneralState()//sets the active elements of the ui to the general state, keeping bottomline, money and the phone icon visible
    {
        this.phoneIcon.gameObject.SetActive(true);
        this.profit.gameObject.SetActive(true);
        this.bottomLine.gameObject.SetActive(true);
        this.phoneBackground.gameObject.SetActive(false);
        this.zuckabookIcon.gameObject.SetActive(false);
        this.managerIcon.gameObject.SetActive(false);
        this.minigamesIcon.gameObject.SetActive(false);
        this.exitButton.gameObject.SetActive(false);
        this.backButton.gameObject.SetActive(false);
        this.isUIUpdated = true;
    }
    void setPhoneState()//sets the active elements of teh ui to the phone state, keeping the apps and phone background visible
    {
        this.phoneIcon.gameObject.SetActive(false);
        this.profit.gameObject.SetActive(false);
        this.bottomLine.gameObject.SetActive(false);
        this.phoneBackground.gameObject.SetActive(true);
        this.zuckabookIcon.gameObject.SetActive(true);
        this.managerIcon.gameObject.SetActive(true);
        this.minigamesIcon.gameObject.SetActive(true);
        this.exitButton.gameObject.SetActive(true);
        this.backButton.gameObject.SetActive(true);
        this.substateManager.setSubstate(false);
        this.isUIUpdated = true;
    }

    void startNewApp()//function to hide all unnecessary gameobjects when starting up a new app in the phone interface
    {
        this.phoneIcon.gameObject.SetActive(false);
        this.profit.gameObject.SetActive(false);
        this.bottomLine.gameObject.SetActive(false);
        this.managerIcon.gameObject.SetActive(false);
        this.minigamesIcon.gameObject.SetActive(false);
        this.zuckabookIcon.gameObject.SetActive(false);
        this.backButton.gameObject.SetActive(true);
    }
    void setAppState(string appName) //function that sets the state of teh active app on the phone
    {
        this.phoneBackground.gameObject.SetActive(true);
        this.startNewApp();
        this.substateManager.setActiveSubstate(appName);
    }
    
    public void checkRoom(bool isPlacing)
    {
        //Sets the sidebar active and fills in the sidebar's information for room name, room image, and profit the room generates 
        sidebar.SetActive(true);
        roomName.text = activePlaceRoom.roomBusinessTemplate.roomName;
        roomImage.sprite = activePlaceRoom.roomBusinessTemplate.roomJPG;
        //If isPlacing is true, display placement cost of room. Else just display profit
        if (isPlacing)
        {
            info.text = "Profit: " + activePlaceRoom.calculatedProfit + "\n" + "Placement Cost: " + activePlaceRoom.calculatedPlacementCost;
        }
        else
        {
            info.text = "Profit: " + activePlaceRoom.calculatedProfit;
        }
        info.text += "\n";

        info.text += "Current room condition: " + activePlaceRoom.condition;
        info.text += "\n \n";

        //Sets the text for what the room likes 
        info.text += "Likes: ";
        for (int i = 0; i < activePlaceRoom.roomBusinessTemplate.likedAttributes.Length; i++)
        {
            //If the goodInteractions list has been fully iterated through, don't add a comma after the last interaction
            if (i + 1 == activePlaceRoom.roomBusinessTemplate.likedAttributes.Length)
            {
                info.text += activePlaceRoom.roomBusinessTemplate.likedAttributes[i] + " " + activePlaceRoom.attributeStats[activePlaceRoom.roomBusinessTemplate.likedAttributes[i]];
            }
            //Else add a comma
            else
            {
                info.text += activePlaceRoom.roomBusinessTemplate.likedAttributes[i] + " " + activePlaceRoom.attributeStats[activePlaceRoom.roomBusinessTemplate.likedAttributes[i]] + ", ";
            }
        }

        info.text += "\n" + "Dislikes: ";
        for (int i = 0; i < activePlaceRoom.roomBusinessTemplate.dislikedAttributes.Length; i++)
        {
            if (i + 1 == activePlaceRoom.roomBusinessTemplate.dislikedAttributes.Length)
            {
                info.text += activePlaceRoom.roomBusinessTemplate.dislikedAttributes[i] + " " + activePlaceRoom.attributeStats[activePlaceRoom.roomBusinessTemplate.dislikedAttributes[i]];
            }
            else
            {
                info.text += activePlaceRoom.roomBusinessTemplate.dislikedAttributes[i] + " " + activePlaceRoom.attributeStats[activePlaceRoom.roomBusinessTemplate.dislikedAttributes[i]] + ", ";
            }
        }

        info.text += "\n" + "Outputs: " + activePlaceRoom.roomBusinessTemplate.outputs;
        //mainArrows.SetActive(true);
    }

    public void confirmPlace()
    {
        money -= roomZoomScript.activePlaceRoom.calculatedPlacementCost;
        currentProfitOverSixSeconds -= roomZoomScript.activePlaceRoom.calculatedPlacementCost;

        if (roomZoomScript.activePlaceRoom.roomBusinessTemplate.faction == "BRU" && firstBRURoom == true)
        {
            InvokeRepeating("CalculateBRURoomProfits", 6, 6);
            InvokeRepeating("BRUDividendTimer", 1, 1);
            firstBRURoom = false;

            //isFirstRoomPlaced is only used to start the rat spawning timer on a level since rats won't spawn until a room has been placed
            if (isFirstRoomPlaced == false)
            {
                isFirstRoomPlaced = true;
            }
        }
        else if (roomZoomScript.activePlaceRoom.roomBusinessTemplate.faction == "Law" && firstLawRoom == true)
        {
            InvokeRepeating("CalculateLawRoomProfits", 6, 6);
            InvokeRepeating("LawDividendTimer", 1, 1);
            firstLawRoom = false;

            if (isFirstRoomPlaced == false)
            {
                isFirstRoomPlaced = true;
            }
        }
        else if (roomZoomScript.activePlaceRoom.roomBusinessTemplate.faction == "Crime" && firstCrimeRoom == true)
        {
            InvokeRepeating("CalculateCrimeRoomProfits", 6, 6);
            InvokeRepeating("CrimeDividendTimer", 1, 1);
            firstCrimeRoom = false;

            if (isFirstRoomPlaced == false)
            {
                isFirstRoomPlaced = true;
            }
        }
    }

    private void CalculateBRURoomProfits()
    {
        //This foreach loop updates all bru rooms attributes modifiers, calculated profits, conditions, and modifies bru faction relations based on how the room is doing
        foreach (RoomController room in RoomController.allRoomControllers)
        {
            if (room != null && room.roomBusinessTemplate.faction == "BRU") //&& room.roomPlacementsTillOpen == 0)
            {
                room.UpdateRoomValues();
            }
        }

        //These if else statements count down to when the bottom line is due, update necessary UI elements,
        //and add money to the players current money based on how much profit each bru room made in the last 6 seconds
        if (bruDividendCounter < 9)
        {
            bruDividendCounter += 1;
            money += currentBRURoomProfits;
            currentProfitOverSixSeconds += currentBRURoomProfits;
            currentBRURoomProfits = 0;
            //moneyText.GetComponent<TextMeshProUGUI>().text = "Total Ziggies: " + money;
            //bruDividendCostText.GetComponent<TextMeshProUGUI>().text = "Place room in ";
        }
        else if (bruDividendCounter >= 9)
        {
            bruDividend = numOfBRURooms * 0;
            bruDividendCounter = 0;
            money += currentBRURoomProfits - bruDividend;
            currentProfitOverSixSeconds += currentBRURoomProfits - bruDividend;
            currentBRURoomProfits = 0;
            //moneyText.GetComponent<TextMeshProUGUI>().text = "Total Ziggies: " + money;
            //bruDividendCostText.GetComponent<TextMeshProUGUI>().text = "Place room in ";
        }
    }

    private void CalculateLawRoomProfits()
    {
        foreach (RoomController room in RoomController.allRoomControllers)
        {
            if (room != null && room.roomBusinessTemplate.faction == "Law") //&& room.roomPlacementsTillOpen == 0)
            {
                room.UpdateRoomValues();
            }
        }

        if (lawDividendCounter < 9)
        {
            lawDividendCounter += 1;
            money += currentLawRoomProfits;
            currentProfitOverSixSeconds += currentLawRoomProfits;
            currentLawRoomProfits = 0;
            //moneyText.GetComponent<TextMeshProUGUI>().text = "Total Ziggies: " + money;
            //lawDividendCostText.GetComponent<TextMeshProUGUI>().text = "Place room in ";
        }
        else if (lawDividendCounter >= 9)
        {
            lawDividend = numOfLawRooms * 0;
            lawDividendCounter = 0;
            money += currentLawRoomProfits - lawDividend;
            currentProfitOverSixSeconds += currentLawRoomProfits - lawDividend;
            currentLawRoomProfits = 0;
            //moneyText.GetComponent<TextMeshProUGUI>().text = "Total Ziggies: " + money;
            //lawDividendCostText.GetComponent<TextMeshProUGUI>().text = "Place room in ";
        }
    }

    private void CalculateCrimeRoomProfits()
    {
        foreach (RoomController room in RoomController.allRoomControllers)
        {
            if (room != null && room.roomBusinessTemplate.faction == "Crime") //&& room.roomPlacementsTillOpen == 0)
            {
                room.UpdateRoomValues();
            }
        }

        if (crimeDividendCounter < 9)
        {
            crimeDividendCounter += 1;
            money += currentCrimeRoomProfits;
            currentProfitOverSixSeconds += currentCrimeRoomProfits;
            currentCrimeRoomProfits = 0;
            //moneyText.GetComponent<TextMeshProUGUI>().text = "Total Ziggies: " + money;
            //crimeDividendCostText.GetComponent<TextMeshProUGUI>().text = "Place room in ";
        }
        else if (crimeDividendCounter >= 9)
        {
            crimeDividend = numOfCrimeRooms * 0;
            crimeDividendCounter = 0;
            money += currentCrimeRoomProfits - crimeDividend;
            currentProfitOverSixSeconds += currentCrimeRoomProfits - crimeDividend;
            currentCrimeRoomProfits = 0;
            //moneyText.GetComponent<TextMeshProUGUI>().text = "Total Ziggies: " + money;
            //crimeDividendCostText.GetComponent<TextMeshProUGUI>().text = "Place room in ";
        }
    }
    
    //This updates the UI text that shows the player their profit over the last six seconds
    public void OverallProfitOverSixSeconds()
    {
        if (currentProfitOverSixSeconds > 0)
        {
            sixSecondProfitText.GetComponent<TextMeshProUGUI>().color = Color.green;
            sixSecondProfitText.GetComponent<TextMeshProUGUI>().text = "+" + currentProfitOverSixSeconds;
        }
        else if (currentProfitOverSixSeconds < 0)
        {
            sixSecondProfitText.GetComponent<TextMeshProUGUI>().color = Color.red;
            sixSecondProfitText.GetComponent<TextMeshProUGUI>().text = "" + currentProfitOverSixSeconds;
        }
        else
        {
            sixSecondProfitText.GetComponent<TextMeshProUGUI>().color = Color.white;
            sixSecondProfitText.GetComponent<TextMeshProUGUI>().text = "" + currentProfitOverSixSeconds;
        }
        currentProfitOverSixSeconds = 0;
    }
    
    //Whenever a room is placed, this updates each factions' dividend costs and also the dividend UI 
    public void UpdateFactionDividendUI()
    {
        bruDividend = numOfBRURooms * 0;
        //bruDividendCostText.GetComponent<TextMeshProUGUI>().text = "Place room in ";

        lawDividend = numOfLawRooms * 0;
        //lawDividendCostText.GetComponent<TextMeshProUGUI>().text = "Place room in ";

        crimeDividend = numOfCrimeRooms * 0;
        //crimeDividendCostText.GetComponent<TextMeshProUGUI>().text = "Place room in ";
    }

    //Updates faction relationship ui
    public void updateFacRelationships()
    {
        if(bruRelationship >= 0)
        {
            bruSliderFill.GetComponent<Image>().color = new Color(0.34f, .91f, .2f);
        }
        else
        {
            bruSliderFill.GetComponent<Image>().color = Color.red;
        }

        if (crimeRelationship >= 0)
        {
            crimeSliderFill.GetComponent<Image>().color = new Color(0.34f, .91f, .2f);
        }
        else
        {
            crimeSliderFill.GetComponent<Image>().color = Color.red;
        }

        if (lawRelationship >= 0)
        {
            lawSliderFill.GetComponent<Image>().color = new Color(0.34f, .91f, .2f);
        }
        else
        {
            lawSliderFill.GetComponent<Image>().color = Color.red;
        }

        bru.value = bruRelationship;
        crime.value = crimeRelationship;
        law.value = lawRelationship;
    }

    //Updates money ui
    public void UpdateMoney()
    {
        moneyText.GetComponent<TextMeshProUGUI>().text = "Total Ziggies: " + money;
    }

    public void ShowAnalyticsUI()
    {
        if (!profitAnalyticUIVisible)
        {
            tray.SetActive(false);
            profitAnalyticUIVisible = true;
            mainCameraPreviousPosition = mainCamera.transform.position;
            mainCamera.GetComponent<Camera>().transform.eulerAngles = new Vector3(90, 0, 0);
            mainCamera.transform.position = new Vector3(centerOfFloor.transform.position.x, 12 * cameraFOVMultiplier, centerOfFloor.transform.position.z);

            foreach (RoomController room in RoomController.allRoomControllers)
            {
                if (room != null)
                {
                    room.analyticUICanvas.SetActive(true);

                    if (room.calculatedProfit > room.baseProfit)
                    {
                        room.profitStatusSprite.GetComponent<Image>().sprite = room.profitStatusUp;
                    }
                    else if (room.calculatedProfit < room.baseProfit)
                    {
                        room.profitStatusSprite.GetComponent<Image>().sprite = room.profitStatusDown;
                    }
                    else if (room.roomBusinessTemplate.faction == "Pirats" || room.roomBusinessTemplate.name == "Entry")
                    {
                        room.profitStatusSpriteBackground.SetActive(false);              
                    }
                    else
                    {
                        room.profitStatusSprite.GetComponent<Image>().sprite = room.profitStatusNeutral;
                    }
                }
            }
        }
        else
        {
            tray.SetActive(true);
            profitAnalyticUIVisible = false;
            roomAttributeAnalyticUIVisible = false;
            mainCamera.transform.position = mainCameraPreviousPosition;
            mainCamera.GetComponent<Camera>().transform.eulerAngles = new Vector3(60, 0, 0);

            foreach (RoomController room in RoomController.allRoomControllers)
            {
                if (room != null)
                {
                    room.analyticUICanvas.SetActive(false);
                    room.profitStatusSpriteBackground.SetActive(true);
                    room.attributeAnalyticUI.SetActive(false);
                }
            }
        }
    }

    public void ShowIncomingAttributesAnalyticUI()
    {
        foreach (RoomController room in RoomController.allRoomControllers)
        {
            if (room != null && (roomZoomScript.activePlaceRoom.roomBusinessTemplate.likedAttributes.Contains(room.output) ||
                roomZoomScript.activePlaceRoom.roomBusinessTemplate.dislikedAttributes.Contains(room.output)))
            {
                if ((Mathf.Abs(roomZoomScript.activePlaceRoom.roomX - room.roomX) + Mathf.Abs(roomZoomScript.activePlaceRoom.roomY - room.roomY)) == 3)
                {
                    if (room.outputModifier == 3)
                    {
                        ShowIncomingAttributeAnalyticUIHelper(room);

                        if (room.attributeText.GetComponent<TextMeshProUGUI>().color == Color.green)
                        {
                            room.attributeText.GetComponent<TextMeshProUGUI>().text = "+1";
                        }
                        else
                        {
                            room.attributeText.GetComponent<TextMeshProUGUI>().text = "-1";
                        }                                           
                    }
                }

                if ((Mathf.Abs(roomZoomScript.activePlaceRoom.roomX - room.roomX) + Mathf.Abs(roomZoomScript.activePlaceRoom.roomY - room.roomY)) == 2)
                {
                    if (room.outputModifier == 3)
                    {
                        ShowIncomingAttributeAnalyticUIHelper(room);

                        if (room.attributeText.GetComponent<TextMeshProUGUI>().color == Color.green)
                        {
                            room.attributeText.GetComponent<TextMeshProUGUI>().text = "+2";
                        }
                        else
                        {
                            room.attributeText.GetComponent<TextMeshProUGUI>().text = "-2";
                        }
                    }
                    else if (room.outputModifier == 2)
                    {
                        ShowIncomingAttributeAnalyticUIHelper(room);

                        if (room.attributeText.GetComponent<TextMeshProUGUI>().color == Color.green)
                        {
                            room.attributeText.GetComponent<TextMeshProUGUI>().text = "+1";
                        }
                        else
                        {
                            room.attributeText.GetComponent<TextMeshProUGUI>().text = "-1";
                        }
                    }
                }

                if ((Mathf.Abs(roomZoomScript.activePlaceRoom.roomX - room.roomX) + Mathf.Abs(roomZoomScript.activePlaceRoom.roomY - room.roomY)) == 1)
                {
                    if (room.outputModifier == 3)
                    {
                        ShowIncomingAttributeAnalyticUIHelper(room);

                        if (room.attributeText.GetComponent<TextMeshProUGUI>().color == Color.green)
                        {
                            room.attributeText.GetComponent<TextMeshProUGUI>().text = "+3";
                        }
                        else
                        {
                            room.attributeText.GetComponent<TextMeshProUGUI>().text = "-3";
                        }
                    }
                    else if (room.outputModifier == 2)
                    {
                        ShowIncomingAttributeAnalyticUIHelper(room);

                        if (room.attributeText.GetComponent<TextMeshProUGUI>().color == Color.green)
                        {
                            room.attributeText.GetComponent<TextMeshProUGUI>().text = "+2";
                        }
                        else
                        {
                            room.attributeText.GetComponent<TextMeshProUGUI>().text = "-2";
                        }
                    }
                    else if (room.outputModifier == 1)
                    {
                        ShowIncomingAttributeAnalyticUIHelper(room);

                        if (room.attributeText.GetComponent<TextMeshProUGUI>().color == Color.green)
                        {
                            room.attributeText.GetComponent<TextMeshProUGUI>().text = "+1";
                        }
                        else
                        {
                            room.attributeText.GetComponent<TextMeshProUGUI>().text = "-1";
                        }
                    }
                }
            }
        }
    }
    
    public void ShowIncomingAttributeAnalyticUIHelper(RoomController room)
    {
        roomAttributeAnalyticUIVisible = true;

        //room.attributeSprite.GetComponent<Image>().sprite = roomZoomScript.DetermineAttributeSprite(room);
        //room.attributeText.GetComponent<TextMeshProUGUI>().color = roomZoomScript.DetermineAttributeTextColor(room.output);

        room.attributeAnalyticUI.SetActive(true);
        //pauseUI.screenDimmer.SetActive(true);
    }

    public void HideIncomingAttributeAnalyticUI()
    {
        roomAttributeAnalyticUIVisible = false;
        //pauseUI.screenDimmer.SetActive(false);

        foreach (RoomController room in RoomController.allRoomControllers)
        {
            if (room != null)
            {
                room.attributeAnalyticUI.SetActive(false);
            }
        }
    }
    
    //Sets up rat clean up text depending on whether rats need to be exterminated or just cleaned up
    public void ShowRatUI()
    {
        ratCleanupUI.SetActive(true);

        if (currentRatRoom.GetComponent<RatController>().isInfested)
        {
            situationText.GetComponent<TextMeshProUGUI>().text = "Exterminate rats in this room for 200$?";
        }
        else
        {
            situationText.GetComponent<TextMeshProUGUI>().text = "Clean up rats in this room for 100$?";
        }
        
    }

    public void getRidOfRats()
    {
        //Removes rats according to extermination 
        if (currentRatRoom.GetComponent<RatController>().isInfested && money >= 200)
        {
            ratCleanupUI.SetActive(false);
            money -= 200;
            currentProfitOverSixSeconds -= 200;
            UpdateMoney();

            currentRatRoom.GetComponent<RatController>().exterminationStarted = true;
            currentRatRoom.GetComponent<RatController>().ShowRatUI();
        }
        //Removes rats according to clean up
        else if(!currentRatRoom.GetComponent<RatController>().isInfested && money >= 100)
        {
            ratCleanupUI.SetActive(false);
            money -= 100;
            currentProfitOverSixSeconds -= 100;
            UpdateMoney();

            currentRatRoom.GetComponent<RatController>().cleanupStarted = true;
            currentRatRoom.GetComponent<RatController>().ShowRatUI();
        }
        //Tells the player they don't have enough money to either clean up the rats or exterminate them. Also exits the function
        else
        {
            CancelAction("Not enough ziggies!");
            return;
        }

        //Resets rat variables in the room being cleaned and makes the rat cleanup curtains visible
        //currentRatRoom.GetComponent<RatController>().cleanupSignifier.SetActive(true);
        currentRatRoom.GetComponent<RatController>().ratsStarted = false;
        currentRatRoom.GetComponent<RatController>().ratSpawnCoroutineStarted = false;
        currentRatRoom.GetComponent<RatController>().isInfested = false;
        currentRatRoom.GetComponent<RatController>().times5SecondsHavePassed = 0;
    }

    //Sets rat cleanup ui inactive
    public void leaveRatsAlone()
    {
        ratCleanupUI.SetActive(false);
    }

    //Calculates the empty room tax based on how many empty rooms there are. The tax is 1 money per empty room
    private float EmptyRoomTax()
    {
        //This for loop checks if a room has a business placed in it or if rats have spawned. 
        //If no business has been placed or rats have spawned, the room is counted toward the tax.
        int emptyTax = 0;
        foreach (GameObject room in currentRooms)
        {
            if (!room.GetComponent<RoomController>().hasBeenPlaced || room.GetComponent<RatController>().ratsStarted)
            {
                emptyTax--;
            }
        }

        return emptyTax;
    }*/
}
