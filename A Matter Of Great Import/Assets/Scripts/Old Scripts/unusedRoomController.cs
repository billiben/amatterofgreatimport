﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class unusedRoomController : MonoBehaviour
{
    /*
    public Button swapRoomsButton;//overarching UI element of swap rooms, inherited from the game controller
    public bool isSelected;//boolean to determine if this. room is selected
    public Vector2 cords;

    private void OnMouseOver()
    {
        else if (gameController.GetComponent<gameStateManager>().gameState == "general")
            {
                this.swapRoomsButton.gameObject.SetActive(true);
                this.swapRoomsButton.onClick.AddListener(roomSwap);
                this.isSelected = true;
                this.gameController.GetComponent<gameStateManager>().currentRoom = this.room;
                this.gameController.GetComponent<gameStateManager>().currentRoomController = this;
            }
            else if (this.gameController.GetComponent<gameStateManager>().uiManager.currentUIState == "swap rooms" && !this.isSelected && this.gameController.GetComponent<gameStateManager>().currentRoom != null)//intended to see if the room swap state is active, and this is not the already selected swap room and the current room isn't empty
            {
                if (this.roomX > (int)(this.gameController.GetComponent<gameStateManager>().currentRoom.location.x - 1) && this.roomX < (int)(this.gameController.GetComponent<gameStateManager>().currentRoom.location.x + 1))//intended to ensure the room is within 1 adjacent space on the X axis
                {
                    if (this.roomY > (int)(this.gameController.GetComponent<gameStateManager>().currentRoom.location.y - 1) && this.roomY < (int)(this.gameController.GetComponent<gameStateManager>().currentRoom.location.y + 1))//intedned to ensure the room is within 1 adjacent space on the y axis
                    {
                        roomCreation tempRoom = new roomCreation();//assigns a blank variable for temp
                        tempRoom = this.room;//assigns the temp to this room
                        this.room = this.gameController.GetComponent<gameStateManager>().currentRoom;//assigns this room to the state of the selected room
                        this.gameController.GetComponent<gameStateManager>().currentRoom = tempRoom;//assigns the selected room this rooms previous state

                        Vector2 temp = new Vector2(this.roomX, this.roomY);//creates a temp vector2 to hold this room location
                        this.room.location = this.gameController.GetComponent<gameStateManager>().currentRoom.location;//moves this room to the location of the other room
                        this.updateRoomLocation(this.gameController.GetComponent<gameStateManager>().currentRoom.location.x, this.gameController.GetComponent<gameStateManager>().currentRoom.location.y);//updates the internal states of the scripts
                        this.gameController.GetComponent<gameStateManager>().currentRoom.location = temp;//moves the other room to this room's previous location
                        this.gameController.GetComponent<gameStateManager>().currentRoomController.updateRoomLocation(temp.x, temp.y);//updates the internal states of teh scripts
                        this.returnToNormal();//returns everything to normal
                    }
                    else
                    {
                        print("You can't put a room there!");//feedback if you can't put a room there
                    }
                }
                else
                {
                    print("You can't put a room there!");//same as the line above
                }
            }
    }

    public void roomSwap()//function to activate the room swap state
    {
        this.swapRoomsButton.gameObject.GetComponentInChildren<TextMeshProUGUI>().SetText("Select the room you want to switch, or click this again to cancel!"); //updates the button text
        this.swapRoomsButton.onClick.RemoveAllListeners();//clears all previous listeners on the swap button
        this.swapRoomsButton.onClick.AddListener(returnToNormal);//adds a listener to return the state to normal on swap button click 
        this.gameController.GetComponent<gameStateManager>().uiManager.currentUIState = "swap rooms";//sets teh current UI state to swap rooms
    }

    public void removeRoom()
    {
        roomBusinessTemplate = null;
        Destroy(businessInstance);
        attributesReceived.Clear();
        goodInteractions.Clear();
        badInteractions.Clear();
        arrows.SetActive(false);
    }
    
      public void updateRoomLocation(float x, float y)//function to update teh current locations of this room to whatevber floats are put in
    {
        this.roomX = (int)x;
        this.roomY = (int)y;
    }

    public void returnToNormal()//function to return the game state to normal
    {
        this.swapRoomsButton.onClick.RemoveAllListeners();//clears any listenre off swap rooms button
        this.swapRoomsButton.gameObject.GetComponentInChildren<TextMeshProUGUI>().SetText("Room Switcheroo?");//sets text back to comic relief mode
        this.swapRoomsButton.gameObject.SetActive(false);//hides teh swap room button
        this.isSelected = false;//makes all roooms deactivated from beign selected
        //this.gameController.GetComponent<GameStateManager>().currentRoom = null;//sets the managers current room to empty
        //this.gameController.GetComponent<GameStateManager>().currentRoomController = null;//sets teh managers current room controller to empty


    }*/
}
