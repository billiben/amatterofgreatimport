﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseUI : MonoBehaviour
{
    public RoomZoom roomZoomScript;

    public GameObject pauseButton;
    public GameObject screenDimmer;
    public GameObject pauseText;
    public GameObject mainMenuButton;
    public GameObject quitButton;
    public GameObject confirmQuitPanel;
    public GameObject resumeGameButton;
    public GameObject switchRoomSelectionButton;
    public GameObject helpButton;
    public GameObject gameOverText;
    public GameObject retryLevelButton;

    public bool firstRoomSelectionChoice;
    public bool confirmQuitUIActive;
    public static bool isPaused;

    public bool isGameOver;

    public GameObject instructions;
    public GameObject exitInstructionsButton;

    public GameObject chooseRoomSelectionUI;
    public GameObject rerollButton;
    public GameObject demolitionButton;
    public GameObject rerollTrayUI;
    public GameObject demolitionTrayUI;

    // Start is called before the first frame update
    void Start()
    {
        pauseButton.GetComponent<Button>().onClick.AddListener(PauseGame);
        helpButton.GetComponent<Button>().onClick.AddListener(SetGameInstructionsActive);
        exitInstructionsButton.GetComponent<Button>().onClick.AddListener(SetGameInstructionsInactive);
        mainMenuButton.GetComponent<Button>().onClick.AddListener(ReturnToMainMenu);
        retryLevelButton.GetComponent<Button>().onClick.AddListener(RetryLevel);
        quitButton.GetComponent<Button>().onClick.AddListener(Quit);
        resumeGameButton.GetComponent<Button>().onClick.AddListener(ResumeGame);
    }

    // Update is called once per frame
    void Update()
    {
        //Pauses the game whenever the player presses the escape key
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PauseGame();
        }

        if (confirmQuitUIActive)
        {
            gameOverText.SetActive(false);

            //Exits the game when the player is prompted to quit or resume
            if (Input.GetKeyDown(KeyCode.Y))
            {
                Application.Quit();
            }
            //Resumes the game when the player is prompted to quit or resume
            else if (Input.GetKeyDown(KeyCode.N))
            {
                if (isGameOver)
                {
                    GameOver();

                    confirmQuitPanel.SetActive(false);
                    confirmQuitUIActive = false;

                    mainMenuButton.transform.localPosition = new Vector3(mainMenuButton.transform.localPosition.x,
                        mainMenuButton.transform.localPosition.y - 160, mainMenuButton.transform.localPosition.z);

                    quitButton.transform.localPosition = new Vector3(quitButton.transform.localPosition.x,
                        quitButton.transform.localPosition.y - 80, quitButton.transform.localPosition.z);
                }
                else
                {
                    screenDimmer.SetActive(false);
                    Time.timeScale = 1;
                    confirmQuitUIActive = false;
                    isPaused = false;
                    confirmQuitPanel.SetActive(false);
                }               
            }
        }
    }

    //This function shows or hides the main pause menu buttons depending on what choices the player has made in the pause menu
    public void ShowPauseMenu(bool isActive)
    {
        pauseText.SetActive(isActive);
        resumeGameButton.SetActive(isActive);
        //switchRoomSelectionButton.SetActive(isActive);
        helpButton.SetActive(isActive);
        mainMenuButton.SetActive(isActive);
        quitButton.SetActive(isActive);
    }

    //Pauses all game processes happening and brings up the pause menu
    public void PauseGame()
    {
        if (Time.timeScale == 1)
        {
            Time.timeScale = 0;
            isPaused = true;
            screenDimmer.SetActive(true);
            ShowPauseMenu(true);
        }
    }

    //Resumes game processes and hides the pause menu
    public void ResumeGame()
    {
        Time.timeScale = 1;
        isPaused = false;
        screenDimmer.SetActive(false);
        ShowPauseMenu(false);
    }

    public void SetGameInstructionsActive()
    {
        instructions.SetActive(true);
        ShowPauseMenu(false);
    }

    public void SetGameInstructionsInactive()
    {
        instructions.SetActive(false);
        ShowPauseMenu(true);

    }

    public void ReturnToMainMenu()
    {
        isPaused = false;
        GameController.currentLevelNum = 0;
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }

    public void RetryLevel()
    {
        isPaused = false;
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void GameOver()
    {
        if (isGameOver && !confirmQuitUIActive)
        {
            return;
        }

        isGameOver = true;
        Time.timeScale = 0;
        screenDimmer.SetActive(true);

        mainMenuButton.transform.localPosition = new Vector3(mainMenuButton.transform.localPosition.x,
            mainMenuButton.transform.localPosition.y + 160, mainMenuButton.transform.localPosition.z);

        mainMenuButton.SetActive(true);

        retryLevelButton.SetActive(true);

        quitButton.transform.localPosition = new Vector3(quitButton.transform.localPosition.x,
            quitButton.transform.localPosition.y + 80, quitButton.transform.localPosition.z);

        quitButton.SetActive(true);
        gameOverText.SetActive(true);
        isPaused = true;
    }

    public void Quit()
    {
        confirmQuitPanel.SetActive(true);
        confirmQuitUIActive = true;

        retryLevelButton.SetActive(false);
        ShowPauseMenu(false);
    }

   /*//Brings up the choose room selection UI that allows the player to change room selection 
   public void SwitchRoomSelection()
   {
       chooseRoomSelectionUI.SetActive(true);
       ShowPauseMenu(false);
   }*/

    /*//Makes the reroll tray UI active for the player when they click the reroll UI button on the screen where they choose which room selection to use
    public void RerollUISelected()
    {
        if (firstRoomSelectionChoice && SceneManager.GetActiveScene().buildIndex == 2)
        {
            GameController.rerollChosen = true;
            chooseRoomSelectionUI.SetActive(false);
            rerollTrayUI.SetActive(true);
            firstRoomSelectionChoice = false;
        }
        else
        {
            GameController.demolitionChosen = false;
            GameController.rerollChosen = true;
            chooseRoomSelectionUI.SetActive(false);
            demolitionTrayUI.SetActive(false);
            rerollTrayUI.SetActive(true);

            roomZoomScript.upgradeButtonDemolition.SetActive(false);
            roomZoomScript.removeButtonDemolition.SetActive(false);
            roomZoomScript.changeRoomButton.SetActive(false);

            ShowPauseMenu(true);
        }
    }

    //Makes the demolition tray UI active for the player when they click the demolition UI button on the screen where they choose which room selection to use
    public void DemolitionUISelected()
    {
        if (firstRoomSelectionChoice && SceneManager.GetActiveScene().buildIndex == 2)
        {
            GameController.demolitionChosen = true;
            chooseRoomSelectionUI.SetActive(false);
            demolitionTrayUI.SetActive(true);
            firstRoomSelectionChoice = false;
        }
        else
        {
            GameController.rerollChosen = false;
            GameController.demolitionChosen = true;
            chooseRoomSelectionUI.SetActive(false);
            rerollTrayUI.SetActive(false);
            demolitionTrayUI.SetActive(true);

            roomZoomScript.upgradeButton.SetActive(false);
            roomZoomScript.removeButton.SetActive(false);

            ShowPauseMenu(true);
        }
    }*/

    /*void Start()
    {
        rerollButton.GetComponent<Button>().onClick.AddListener(RerollUISelected);
        demolitionButton.GetComponent<Button>().onClick.AddListener(DemolitionUISelected);
        switchRoomSelectionButton.GetComponent<Button>().onClick.AddListener(SwitchRoomSelection);

        //This code brings up the choose room selection UI at the beginning of the first level only
        firstRoomSelectionChoice = true;
        if (SceneManager.GetActiveScene().buildIndex == 2)
        {
            chooseRoomSelectionUI.SetActive(true);
            GameController.rerollChosen = false;
            GameController.demolitionChosen = false;
        }

        //Shows the reroll tray UI at the beginning of the current level since that is the previous choice of room selection
        if (GameController.rerollChosen)
        {
            rerollTrayUI.SetActive(true);
        }

        //Shows the demolition tray UI at the beginning of the current level since that is the previous choice of room selection
        if (GameController.demolitionChosen)
        {
            demolitionTrayUI.SetActive(true);
        }
    }*/
}
