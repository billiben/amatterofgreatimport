﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RatController : MonoBehaviour
{
    /*public RoomController thisRoom;
    public UIManager uiManager;

    public bool ratsStealingProfit;
    public float percentProfitLeftFromRats;
    public bool piratPenReducingProfit;
    public bool ratsStarted;
    public bool ratSpawnCoroutineStarted;
    public bool isInfested;
    public float piratPenSpawnCountdown;
    public float piratSpawnAgainCountdown;
    public int times5SecondsHavePassed;
    public List<Vector2> nearbyEmptyRoomLocations;

    public bool cleanupStarted;
    public bool exterminationStarted;
    public float cleanupTime;
    public float exterminationTime;
    //public GameObject cleanupSignifier;

    public GameObject ratCanvas;
    public GameObject ratText;
    public GameObject ratSlider;

    public void Update()
    {
        if (ratsStarted && !ratSpawnCoroutineStarted)
        {
            StartCoroutine("SpawnMoreRatsInThisRoom");
            ratSpawnCoroutineStarted = true;
        }

        if (gameObject.GetComponent<RoomController>().roomBusinessTemplate != null && 
            gameObject.GetComponent<RoomController>().roomBusinessTemplate.roomName == "Pirat Petting Pen")
        {           
            if (piratPenSpawnCountdown < 60)
            {
                piratPenSpawnCountdown += Time.deltaTime;
            }
            else
            {
                piratSpawnAgainCountdown += Time.deltaTime;

                if(piratSpawnAgainCountdown >= 5)
                {
                    piratSpawnAgainCountdown = 0;

                    FindNearbyEmptyRooms();

                    //The rest of the code after this if statement determines whether a rat will spawn 
                    //through certain precentage chances depending on how many rats are in a room
                    if (nearbyEmptyRoomLocations.Count != 0)
                    {
                        float randomNum = Random.Range(1, 100);

                        if ((times5SecondsHavePassed >= 0 && times5SecondsHavePassed <= 3) && randomNum <= 20)
                        {
                            SpawnRatInOtherRoom();
                        }

                        else if ((times5SecondsHavePassed >= 4 && times5SecondsHavePassed <= 8) && randomNum <= 40)
                        {
                            SpawnRatInOtherRoom();
                        }

                        else if ((times5SecondsHavePassed >= 9 && times5SecondsHavePassed <= 13) && randomNum <= 60)
                        {
                            SpawnRatInOtherRoom();
                        }

                        else if (times5SecondsHavePassed >= 14 && randomNum <= 80)
                        {
                            SpawnRatInOtherRoom();
                        }
                    }

                    SetPettingPenProfitReduction(true);
                }              
            }
        }


        //This if statement is activated if the room is cleaning up rats. After 30 seconds,
        //all the variables that contain room business data are cleared. All the rats
        //are also deleted
        if (cleanupStarted)
        {
            cleanupTime += Time.deltaTime;

            if (cleanupTime >= 30)
            {
                Destroy(thisRoom.businessInstance);

                thisRoom.hasBeenPlaced = false;
                thisRoom.roomBusinessTemplate = null;
                thisRoom.businessInstance = null;
                RoomController.allRoomControllers[thisRoom.roomX, thisRoom.roomY] = null;
                //cleanupSignifier.SetActive(false);
                SetRatProfitStealingBool(false);
                thisRoom.outputModifier = 1;

                cleanupTime = 0.0f;
                cleanupStarted = false;

                StartSpawningRatsAgain();
            }
        }

        //This if statement is activated if the room is exterminating rats. After 45 seconds,
        //all the variables that contain room business data are cleared. All the rats
        //are also deleted
        if (exterminationStarted)
        {
            exterminationTime += Time.deltaTime;

            if (exterminationTime >= 45)
            {
                Destroy(thisRoom.businessInstance);

                thisRoom.hasBeenPlaced = false;
                thisRoom.roomBusinessTemplate = null;
                thisRoom.businessInstance = null;
                RoomController.allRoomControllers[thisRoom.roomX, thisRoom.roomY] = null;
                //cleanupSignifier.SetActive(false);
                SetRatProfitStealingBool(false);
                thisRoom.outputModifier = 1;

                exterminationTime = 0.0f;
                exterminationStarted = false;

                StartSpawningRatsAgain();
            }
        }
    }

    //The following code is used to spawn more rats in this room and other rooms
    IEnumerator SpawnMoreRatsInThisRoom()
    {
        if (ratsStarted)
        {
            if (times5SecondsHavePassed == 0)
            {
                //This next code sets the boolean that halves nearby rooms
                //profits when an infested rat room is near them to true
                SetRatProfitStealingBool(true);
                SetRatProfitStealingPercent(0.9f);
            }

            yield return new WaitForSeconds(5);

            times5SecondsHavePassed++;

            if (times5SecondsHavePassed == 2)
            {
                SetRatProfitStealingPercent(0.8f);
            }

            if (times5SecondsHavePassed == 4)
            {
                Destroy(thisRoom.businessInstance);
                thisRoom.businessInstance = Instantiate(thisRoom.roomBusinessTemplate.roomArt2, new Vector3(gameObject.transform.position.x + 5,
                    gameObject.transform.position.y + 0.5f, gameObject.transform.position.z - 1.15f), Quaternion.identity);

                SetRatProfitStealingPercent(0.7f);
            }

            if (times5SecondsHavePassed == 6)
            {
                SetRatProfitStealingPercent(0.6f);
            }

            //Sets the room to infested, and tries to spawn a rat in another adjacent empty room
            //The next 30 lines of code are similar to the ChooseInitialSpawnRoom function in the RatSystem script
            if (times5SecondsHavePassed >= 8)
            {
                if (times5SecondsHavePassed == 8)
                {
                    Destroy(thisRoom.businessInstance);
                    thisRoom.businessInstance = Instantiate(thisRoom.roomBusinessTemplate.roomArt2, new Vector3(gameObject.transform.position.x + 5,
                        gameObject.transform.position.y + 0.5f, gameObject.transform.position.z - 1.15f), Quaternion.identity);

                    SetRatProfitStealingPercent(0.5f);
                }

                isInfested = true;

                FindNearbyEmptyRooms();

                //The rest of the code after this if statement determines whether a rat will spawn 
                //through certain precentage chances depending on how many rats are in a room
                if (nearbyEmptyRoomLocations.Count != 0)
                {
                    float randomNum = Random.Range(1, 100);

                    if ((times5SecondsHavePassed >= 8 && times5SecondsHavePassed <= 10) && randomNum <= 20)
                    {
                        SpawnRatInOtherRoom();
                    }

                    else if ((times5SecondsHavePassed >= 11 && times5SecondsHavePassed <= 15) && randomNum <= 40)
                    {
                        SpawnRatInOtherRoom();
                    }

                    else if ((times5SecondsHavePassed >= 16 && times5SecondsHavePassed <= 20) && randomNum <= 60)
                    {
                        SpawnRatInOtherRoom();
                    }

                    else if (times5SecondsHavePassed >= 21 && randomNum <= 80)
                    {
                        SpawnRatInOtherRoom();
                    }
                }
            }

            StartCoroutine("SpawnMoreRatsInThisRoom");
        }
    }

    public void SetRatProfitStealingBool(bool isHalving)
    {
        if (RoomController.allRoomControllers[thisRoom.roomX, (thisRoom.roomY - 1)] != null)
        {
            RoomController.allRoomControllers[thisRoom.roomX, (thisRoom.roomY - 1)].ratControl.ratsStealingProfit = isHalving;
        }

        if (RoomController.allRoomControllers[thisRoom.roomX, (thisRoom.roomY + 1)] != null)
        {
            RoomController.allRoomControllers[thisRoom.roomX, (thisRoom.roomY + 1)].ratControl.ratsStealingProfit = isHalving;
        }

        if (RoomController.allRoomControllers[(thisRoom.roomX - 1), thisRoom.roomY] != null)
        {
            RoomController.allRoomControllers[(thisRoom.roomX - 1), thisRoom.roomY].ratControl.ratsStealingProfit = isHalving;
        }

        if (RoomController.allRoomControllers[(thisRoom.roomX + 1), thisRoom.roomY] != null)
        {
            RoomController.allRoomControllers[(thisRoom.roomX + 1), thisRoom.roomY].ratControl.ratsStealingProfit = isHalving;
        }
    }

    public void SetRatProfitStealingPercent(float percentLeft)
    {
        if (RoomController.allRoomControllers[thisRoom.roomX, (thisRoom.roomY - 1)] != null)
        {
            RoomController.allRoomControllers[thisRoom.roomX, (thisRoom.roomY - 1)].ratControl.percentProfitLeftFromRats = percentLeft;
        }

        if (RoomController.allRoomControllers[thisRoom.roomX, (thisRoom.roomY + 1)] != null)
        {
            RoomController.allRoomControllers[thisRoom.roomX, (thisRoom.roomY + 1)].ratControl.percentProfitLeftFromRats = percentLeft;
        }

        if (RoomController.allRoomControllers[(thisRoom.roomX - 1), thisRoom.roomY] != null)
        {
            RoomController.allRoomControllers[(thisRoom.roomX - 1), thisRoom.roomY].ratControl.percentProfitLeftFromRats = percentLeft;
        }

        if (RoomController.allRoomControllers[(thisRoom.roomX + 1), thisRoom.roomY] != null)
        {
            RoomController.allRoomControllers[(thisRoom.roomX + 1), thisRoom.roomY].ratControl.percentProfitLeftFromRats = percentLeft;
        }
    }

    //This function spawns rats other rooms that are empty and nearby rat rooms. It is identical to the last 
    //part of the ChooseInitialSpawnRoom function in the RatSystem script
    public void SpawnRatInOtherRoom()
    {
        if (nearbyEmptyRoomLocations.Count > 0)
        {
            Vector2 finalLocation = nearbyEmptyRoomLocations[Random.Range(0, nearbyEmptyRoomLocations.Count - 1)];

            GameObject ratRoom = GameObject.Find("Room " + finalLocation.x + "," + finalLocation.y);
            ratRoom.GetComponent<RoomController>().roomBusinessTemplate = uiManager.GetComponent<RatCreation>().ratTemplate;
            ratRoom.GetComponent<RoomController>().businessInstance = Instantiate(uiManager.GetComponent<RatCreation>().ratTemplate.roomArt1, new Vector3(ratRoom.transform.position.x + 5, ratRoom.transform.position.y + 0.5f, ratRoom.transform.position.z - 1.15f), Quaternion.identity);
            ratRoom.GetComponent<RoomController>().hasBeenPlaced = true;
            ratRoom.GetComponent<RoomController>().outputModifier = 0;
            ratRoom.GetComponent<RatController>().ratsStarted = true;
            ratRoom.GetComponent<RatController>().times5SecondsHavePassed++;
            RoomController.allRoomControllers[(int)finalLocation.x, (int)finalLocation.y] = ratRoom.GetComponent<RoomController>();
        }
    }

    //This function sets the variables ratCountdown and ratsSpawning in the RatSystem script to the values needed in order to 
    //start spawning rats again. It only starts spawning rats again if no room has rats, is cleaning up rats, or is exterminating rats
    public void StartSpawningRatsAgain()
    {
        foreach (GameObject room in uiManager.currentRooms)
        {
            if (room.GetComponent<RatController>().ratsStarted || room.GetComponent<RatController>().cleanupStarted || 
                room.GetComponent<RatController>().exterminationStarted)
            {
                return;
            }
        }

        uiManager.GetComponent<RatCreation>().noRats = true;
        uiManager.GetComponent<RatCreation>().areRatsOnFloor = true;
        uiManager.GetComponent<RatCreation>().ratCountdown = 0.0f;
        uiManager.GetComponent<RatCreation>().ratsSpawning = false;
    }

    public void ShowRatUI()
    {
        ratCanvas.SetActive(true);
        if (isInfested)
        {
            ratText.GetComponent<TextMeshProUGUI>().text = "Extermination Time Left: " + 45 + "s";
            ratSlider.GetComponent<Slider>().maxValue = 45;
            ratSlider.GetComponent<Slider>().value = 45;
            StartCoroutine("ExterminationCountdown");
        }
        else
        {
            ratText.GetComponent<TextMeshProUGUI>().text = "Cleanup Time Left: " + 30 + "s";
            ratSlider.GetComponent<Slider>().maxValue = 30;
            ratSlider.GetComponent<Slider>().value = 30;
            StartCoroutine("CleanupCountdown");
        }
    }

    IEnumerator ExterminationCountdown()
    {
        yield return new WaitForSeconds(1);
        ratSlider.GetComponent<Slider>().value--;
        ratText.GetComponent<TextMeshProUGUI>().text = "Extermination Time Left: " + ratSlider.GetComponent<Slider>().value + "s";

        if(ratSlider.GetComponent<Slider>().value <= 0)
        {
            ratCanvas.SetActive(false);
        }
        else
        {
            StartCoroutine("ExterminationCountdown");
        }
    }

    IEnumerator CleanupCountdown()
    {
        yield return new WaitForSeconds(1);
        ratSlider.GetComponent<Slider>().value--;
        ratText.GetComponent<TextMeshProUGUI>().text = "Cleanup Time Left: " + ratSlider.GetComponent<Slider>().value + "s";

        if (ratSlider.GetComponent<Slider>().value <= 0)
        {
            ratCanvas.SetActive(false);
        }
        else
        {
            StartCoroutine("CleanupCountdown");
        }
    }

    public void SetPettingPenProfitReduction(bool isReducing)
    {
        if (RoomController.allRoomControllers[thisRoom.roomX, (thisRoom.roomY - 1)] != null)
        {
            RoomController.allRoomControllers[thisRoom.roomX, (thisRoom.roomY - 1)].ratControl.piratPenReducingProfit = isReducing;
        }

        if (RoomController.allRoomControllers[thisRoom.roomX, (thisRoom.roomY + 1)] != null)
        {
            RoomController.allRoomControllers[thisRoom.roomX, (thisRoom.roomY + 1)].ratControl.piratPenReducingProfit = isReducing;
        }

        if (RoomController.allRoomControllers[(thisRoom.roomX - 1), thisRoom.roomY] != null)
        {
            RoomController.allRoomControllers[(thisRoom.roomX - 1), thisRoom.roomY].ratControl.piratPenReducingProfit = isReducing;
        }

        if (RoomController.allRoomControllers[(thisRoom.roomX + 1), thisRoom.roomY] != null)
        {
            RoomController.allRoomControllers[(thisRoom.roomX + 1), thisRoom.roomY].ratControl.piratPenReducingProfit = isReducing;
        }
    }

    public void FindNearbyEmptyRooms()
    {
        nearbyEmptyRoomLocations.Clear();
        if (RoomController.allRoomControllers[thisRoom.roomX, (thisRoom.roomY - 1)] == null)
        {
            if (GameObject.Find("Room " + thisRoom.roomX + "," + (thisRoom.roomY - 1)) != null)
            {
                nearbyEmptyRoomLocations.Add(new Vector2(thisRoom.roomX, thisRoom.roomY - 1));
            }
        }

        if (RoomController.allRoomControllers[thisRoom.roomX, (thisRoom.roomY + 1)] == null)
        {
            if (GameObject.Find("Room " + thisRoom.roomX + "," + (thisRoom.roomY + 1)) != null)
            {
                nearbyEmptyRoomLocations.Add(new Vector2(thisRoom.roomX, thisRoom.roomY + 1));
            }
        }

        if (RoomController.allRoomControllers[(thisRoom.roomX - 1), thisRoom.roomY] == null)
        {
            if (GameObject.Find("Room " + (thisRoom.roomX - 1) + "," + thisRoom.roomY) != null)
            {
                nearbyEmptyRoomLocations.Add(new Vector2(thisRoom.roomX - 1, thisRoom.roomY));
            }
        }

        if (RoomController.allRoomControllers[(thisRoom.roomX + 1), thisRoom.roomY] == null)
        {
            if (GameObject.Find("Room " + (thisRoom.roomX + 1) + "," + thisRoom.roomY) != null)
            {
                nearbyEmptyRoomLocations.Add(new Vector2(thisRoom.roomX + 1, thisRoom.roomY));
            }
        }
    }*/

}
