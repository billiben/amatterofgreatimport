﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Runtime.InteropServices;
using System.Threading;
using UnityEngine;

public class RatCreation : MonoBehaviour
{
    /*public bool areRatsOnFloor;
    public float ratCountdown;
    
    public List<Vector2> emptyAdjacentRoomLocations;
    public RoomCreation ratTemplate;
    public bool ratsSpawning;

    public bool noRats;

    void Update()
    {
        //The rat timer starts once the first room is placed
        if (UIManager.isFirstRoomPlaced)
        {
            //This if statement counts to 60 seconds, and then the else if spawns the initial rat in the station
            if (ratCountdown < 60 && areRatsOnFloor)
            {
                ratCountdown += Time.deltaTime;
            }
            else if (!ratsSpawning && areRatsOnFloor)
            {
                ratsSpawning = true;
                ChooseInitialSpawnRoom();
            }
        }
        
    }

    //This function chooses the initial spawn room for the rats and spawns the rat
    public void ChooseInitialSpawnRoom()
    {
        //The xLocation and yLocation variables are used as counter variables to keep track of 
        //where the loop currently is in the 2D array allRoomCreations. They are also used to access 
        //rooms in the allRoomCreations array. emptyAdjacentRoomLocations is a List used to keep track
        //of all empty room locations that are also adjacent to a user placed room
        emptyAdjacentRoomLocations.Clear();
        int xLocation = 0;
        int yLocation = 0;

        foreach (RoomController room in RoomController.allRoomControllers)
        {
            //Checking to see if the room exists in the array
            if (room != null)
            {
                //Checking to see if any empty rooms are nearby in the array
                if (RoomController.allRoomControllers[xLocation, (yLocation - 1)] == null)
                {
                    //Making sure the empty room actually exists in the scene, since the 2D array has more spots than actual rooms exist in the scene
                    if (GameObject.Find("Room " + xLocation + "," + (yLocation - 1)) != null)
                    {
                        //The rest of this code checks the emptyAdjacentRoomLocations List to see if the current empty room location
                        //is already in the array. If it is, it is removed in the for loop and added again after the loop.
                        //If it isn't, then the for loop does nothing and the room location is added after the loop.
                        Vector2 currentCoord = new Vector2(xLocation, yLocation - 1);
                        foreach (Vector2 current in emptyAdjacentRoomLocations)
                        {
                            if (currentCoord == current)
                            {
                                emptyAdjacentRoomLocations.Remove(current);
                                break;
                            }
                        }
                        emptyAdjacentRoomLocations.Add(new Vector2(xLocation, yLocation - 1));
                    }
                }

                if (RoomController.allRoomControllers[xLocation, (yLocation + 1)] == null)
                {
                    if (GameObject.Find("Room " + xLocation + "," + (yLocation + 1)) != null)
                    {
                        Vector2 currentCoord = new Vector2(xLocation, yLocation + 1);
                        foreach (Vector2 current in emptyAdjacentRoomLocations)
                        {
                            if (currentCoord == current)
                            {
                                emptyAdjacentRoomLocations.Remove(current);
                                break;
                            }
                        }
                        emptyAdjacentRoomLocations.Add(new Vector2(xLocation, yLocation + 1));
                    }
                }

                if (RoomController.allRoomControllers[(xLocation - 1), yLocation] == null)
                {
                    if (GameObject.Find("Room " + (xLocation - 1) + "," + yLocation) != null)
                    {
                        Vector2 currentCoord = new Vector2(xLocation - 1, yLocation);
                        foreach (Vector2 current in emptyAdjacentRoomLocations)
                        {
                            if (currentCoord == current)
                            {
                                emptyAdjacentRoomLocations.Remove(current);
                                break;
                            }
                        }
                        emptyAdjacentRoomLocations.Add(new Vector2(xLocation - 1, yLocation));
                    }
                }

                if (RoomController.allRoomControllers[(xLocation + 1), yLocation] == null)
                {
                    if (GameObject.Find("Room " + (xLocation + 1) + "," + yLocation) != null)
                    {
                        Vector2 currentCoord = new Vector2(xLocation + 1, yLocation);
                        foreach (Vector2 current in emptyAdjacentRoomLocations)
                        {
                            if (currentCoord == current)
                            {
                                emptyAdjacentRoomLocations.Remove(current);
                                break;
                            }
                        }
                        emptyAdjacentRoomLocations.Add(new Vector2(xLocation + 1, yLocation));
                    }
                }
            }

            //Iterating the counter variables
            yLocation++;
            if (yLocation > 14)
            {
                yLocation = 0;
                xLocation++;
            }
        }

        //If the whole station has not been filled with rooms, spawn rats
        if (emptyAdjacentRoomLocations.Count > 0)
        {
            //The rest of this code randomly chooses a location of an empty room, finds the room game object in the scene, and assigns that object
            //to the ratRoom variable.The rest of the variables that are altered in the room controller set up the room like a business was placed,
            //except in this case a rat room spawned without being placed by the player.
            Vector2 finalLocation = emptyAdjacentRoomLocations[UnityEngine.Random.Range(0, emptyAdjacentRoomLocations.Count - 1)];
            GameObject ratRoom = GameObject.Find("Room " + finalLocation.x + "," + finalLocation.y);
            ratRoom.GetComponent<RoomController>().roomBusinessTemplate = ratTemplate;
            ratRoom.GetComponent<RoomController>().businessInstance = Instantiate(ratTemplate.roomArt1, new Vector3(ratRoom.transform.position.x + 5, ratRoom.transform.position.y + 0.5f, ratRoom.transform.position.z - 1.15f), Quaternion.identity);
            ratRoom.GetComponent<RoomController>().hasBeenPlaced = true;
            ratRoom.GetComponent<RoomController>().outputModifier = 0;
            ratRoom.GetComponent<RatController>().ratsStarted = true;
            RoomController.allRoomControllers[(int)finalLocation.x, (int)finalLocation.y] = ratRoom.GetComponent<RoomController>();
        }
    }*/
}
