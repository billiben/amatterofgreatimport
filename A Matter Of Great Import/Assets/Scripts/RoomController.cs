﻿using System.Collections.Generic;
using System.Collections;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


public class RoomController : MonoBehaviour
{
    public RoomCreation roomBusinessTemplate;//Business/organization scriptable object located in this room
    public static RoomController[,] allRoomControllers = new RoomController[15, 15];

    public int roomX;
    public int roomY;//coords to set room in 2d array

    public int level;

    public float calculatedProfit;
    public float calculatedPlacementCost;
    public float baseProfit;

    public UIManager uiManager;

    public GameObject analyticUICanvas;
    public GameObject profitStatusSprite;
    public GameObject profitStatusSpriteBackground;
    public Sprite profitStatusUp;
    public Sprite profitStatusDown;
    public Sprite profitStatusNeutral;
    public GameObject attributeAnalyticUI;
    public GameObject attributeSprite;
    public GameObject attributeText;

    public Sprite alienHappySprite;
    public Sprite alienNeutralSprite;
    public Sprite alienMadSprite;

    public AudioClip[] money;
    public AudioClip[] construction;
    private AudioSource audioSource;

    public float likedAttributeStat;
    public float dislikedAttributeStat;
    public float attributeModifier;
    public float factionRelationshipChange;
    public float condition;
    public float conditionModifier;

    public string output;
    public int outputModifier;

    //new storage values for condition calculations
    public bool isAmplificationRoomAdjacent;
    public bool isAmplificationRoomAdjacentBru;
    public float nearLaw;
    public float nearCrime;

    //new float for ratings
    public float rating;

    public Dictionary<string, int> attributeStats = new Dictionary<string, int>()
    {
        ["Foot Traffic"] = 0,
        ["Noise"] = 0,
        ["Security"] = 0,
        ["Professional"] = 0,
        ["Criminal"] = 0,
        ["Food"] = 0
    };

    public GameObject businessInstance;

    private Color32 originalColor;
    public static bool placedRoom;
    public bool hasBeenPlaced;

    /*public int roomPlacementsTillOpen;
    public bool checkedIfOpen;*/

    public RatController ratControl;
    public FloorMesh floorMesh;

    public void Awake()//This function is used for any preplaced room the player starts with
    {
        audioSource = GetComponent<AudioSource>();

        if (roomBusinessTemplate != null)
        {
            /*if(roomBusinessTemplate.name == "Pirat Petting Pen")
            {
                uiManager.numOfPiratPettingPens++;
            }*/

            AddRoomBusiness(false);
        }

        originalColor = gameObject.GetComponent<MeshRenderer>().material.color;

        level = 1;
        outputModifier = 1;
    }

    public void Update()
    {
        /*//Demolition Room Selection code
        //If a room has been placed anywhere in the scene and the countdown till it opens hasn't been checked, check if it is open
        if(placedRoom && !checkedIfOpen)
        {
            //If the room still has more than 0 turns until it opens, subtract one from the turns unti it opens
            if (roomPlacementsTillOpen > 0)
            {
                --roomPlacementsTillOpen;
            }            

            //Make sure this block of code doesn't run a million times in a couple seconds
            checkedIfOpen = true;
        }

        //If the variable for checking if a room has been placed recently is assigned false, make the boolean that checks if this room is open false so that it
        //can check if this room is open the next time a room is placed
        if (!placedRoom)
        {
            checkedIfOpen = false;
        }*/
    }

    //This function is used when getting a room's values before it has been placed and
    //getting a room's values when the player is checking a room. It shows the player what
    //good and bad attributes the room will receive, what attributes the room outputs,
    //and the profit the room will make
    public void GetInitialRoomValues()
    {
        CollectNearbyAttributes();
        /*DetermineLikedAttributes();
        DetermineDislikedAttributes();      
        DetermineAttributeModifier();
        UpdateProfit();*/
    }

    public void UpdateRoomValues()//timer to calculate this rooms profit on the time of the float provided when called
    {
        CollectNearbyAttributes();
        /*DetermineLikedAttributes();
        DetermineDislikedAttributes();
        DetermineAttributeModifier();
        updateFactionRelationship();

        UpdateProfit();

        if(roomBusinessTemplate.faction == "BRU")
        {
            uiManager.currentBRURoomProfits += calculatedProfit;
        }
        else if (roomBusinessTemplate.faction == "Law")
        {
            uiManager.currentLawRoomProfits += calculatedProfit;
        }
        else if (roomBusinessTemplate.faction == "Crime")
        {
            uiManager.currentCrimeRoomProfits += calculatedProfit;
        }*/
    }

    //This function checks the attributes being put out by other rooms within 3 rooms of the room with this script. Based on their output modifiers, it also determines
    //how much of each attribute this room receives
    public void CollectNearbyAttributes()
    {
        nearLaw = 0;
        nearCrime = 0;

        if (roomBusinessTemplate.faction == "BRU")
        {
            isAmplificationRoomAdjacentBru = DetermineIfAmplificationRoomIsAdjacent();
        }

        //collecting how many law and crime rooms border this room
        if (allRoomControllers[roomX, (roomY - 1)] != null)
        {
            if (allRoomControllers[roomX, (roomY - 1)].roomBusinessTemplate.name == "Amplification Array")
            {
                isAmplificationRoomAdjacent = true;
            }
            else if (allRoomControllers[roomX, (roomY - 1)].isAmplificationRoomAdjacent || isAmplificationRoomAdjacentBru)
            {
                if (allRoomControllers[roomX, (roomY - 1)].roomBusinessTemplate.faction == "Law")
                {
                    nearLaw += 2;
                }
                else if (allRoomControllers[roomX, (roomY - 1)].roomBusinessTemplate.faction == "Crime")
                {
                    nearCrime += 2;
                }
            }
            else
            {
                if (allRoomControllers[roomX, (roomY - 1)].roomBusinessTemplate.faction == "Law")
                {
                    nearLaw += 1;
                }
                else if (allRoomControllers[roomX, (roomY - 1)].roomBusinessTemplate.faction == "Crime")
                {
                    nearCrime += 1;
                }
            }   
        }

        if (allRoomControllers[roomX, (roomY + 1)] != null)
        {
            if (allRoomControllers[roomX, (roomY + 1)].roomBusinessTemplate.name == "Amplification Array")
            {
                isAmplificationRoomAdjacent = true;
            }
            else if (allRoomControllers[roomX, (roomY + 1)].isAmplificationRoomAdjacent || isAmplificationRoomAdjacentBru)
            {
                if (allRoomControllers[roomX, (roomY + 1)].roomBusinessTemplate.faction == "Law")
                {
                    nearLaw += 2;
                }
                else if (allRoomControllers[roomX, (roomY + 1)].roomBusinessTemplate.faction == "Crime")
                {
                    nearCrime += 2;
                }
            }
            else
            {
                if (allRoomControllers[roomX, (roomY + 1)].roomBusinessTemplate.faction == "Law")
                {
                    nearLaw += 1;
                }
                else if (allRoomControllers[roomX, (roomY + 1)].roomBusinessTemplate.faction == "Crime")
                {
                    nearCrime += 1;
                }
            }
        }

        if (allRoomControllers[(roomX - 1), roomY] != null)
        {
            if (allRoomControllers[roomX - 1, roomY].roomBusinessTemplate.name == "Amplification Array")
            {
                isAmplificationRoomAdjacent = true;
            }
            else if (allRoomControllers[roomX - 1, roomY].isAmplificationRoomAdjacent || isAmplificationRoomAdjacentBru)
            {
                if (allRoomControllers[roomX - 1, roomY].roomBusinessTemplate.faction == "Law")
                {
                    nearLaw += 2;
                }
                else if (allRoomControllers[roomX - 1, roomY].roomBusinessTemplate.faction == "Crime")
                {
                    nearCrime += 2;
                }
            }
            else
            {
                if (allRoomControllers[roomX - 1, roomY].roomBusinessTemplate.faction == "Law")
                {
                    nearLaw += 1;
                }
                else if (allRoomControllers[roomX - 1, roomY].roomBusinessTemplate.faction == "Crime")
                {
                    nearCrime += 1;
                }
            }
        }

        if (allRoomControllers[(roomX + 1), roomY] != null)
        {
            if (allRoomControllers[roomX + 1, roomY].roomBusinessTemplate.name == "Amplification Array")
            {
                isAmplificationRoomAdjacent = true;
            }
            else if (allRoomControllers[roomX + 1, roomY].isAmplificationRoomAdjacent || isAmplificationRoomAdjacentBru)
            {
                if (allRoomControllers[roomX + 1, roomY].roomBusinessTemplate.faction == "Law")
                {
                    nearLaw += 2;
                }
                else if (allRoomControllers[roomX + 1, roomY].roomBusinessTemplate.faction == "Crime")
                {
                    nearCrime += 2;
                }
            }
            else
            {
                if (allRoomControllers[roomX + 1, roomY].roomBusinessTemplate.faction == "Law")
                {
                    nearLaw += 1;
                }
                else if (allRoomControllers[roomX + 1, roomY].roomBusinessTemplate.faction == "Crime")
                {
                    nearCrime += 1;
                }
            }
        }

        UpdateCondition();

        /*attributeStats["Foot Traffic"] = 0;
        attributeStats["Noise"] = 0;
        attributeStats["Security"] = 0;
        attributeStats["Professional"] = 0;
        attributeStats["Criminal"] = 0;
        attributeStats["Food"] = 0;

        foreach(RoomController room in allRoomControllers)
        {
            if(room != null)
            {
                //If a room that is 3 rooms away has an output modifier of 3, then add 1 to the attribute this room receives
                if ((Mathf.Abs(roomX - room.roomX) + Mathf.Abs(roomY - room.roomY)) == 3)
                {
                    if(room.outputModifier == 3)
                    {
                        attributeStats[room.output] += 1;
                    }
                }

                //If a room that is 2 rooms away has an output modifier of 2, then add 1 to the attribute this room receives. If it has 3, then add 2.
                if ((Mathf.Abs(roomX - room.roomX) + Mathf.Abs(roomY - room.roomY)) == 2)
                {
                    if (room.outputModifier == 3)
                    {
                        attributeStats[room.output] += 2;
                    }
                    else if (room.outputModifier == 2)
                    {
                        attributeStats[room.output] += 1;
                    }
                }

                //If a room that is 1 room away has an output modifier of 1, then add 1 to the attribute this room receives. If it has 2, then add 2. If it has 3, add 3.
                if ((Mathf.Abs(roomX - room.roomX) + Mathf.Abs(roomY - room.roomY)) == 1)
                {
                    if (room.outputModifier == 3)
                    {
                        attributeStats[room.output] += 3;
                    }
                    else if (room.outputModifier == 2)
                    {
                        attributeStats[room.output] += 2;
                    }
                    else if (room.outputModifier == 1)
                    {
                        attributeStats[room.output] += 1;
                    }
                }
            }
        }*/
    }

    public bool DetermineIfAmplificationRoomIsAdjacent()
    {
        if (allRoomControllers[roomX, (roomY - 1)] != null)
        {
            if (allRoomControllers[roomX, (roomY - 1)].roomBusinessTemplate.name == "Amplification Array")
            {
                return true;
            }
        }
        if (allRoomControllers[roomX, (roomY + 1)] != null)
        {
            if (allRoomControllers[roomX, (roomY + 1)].roomBusinessTemplate.name == "Amplification Array")
            {
                return true;
            }
        }
        if (allRoomControllers[(roomX - 1), roomY] != null)
        {
            if (allRoomControllers[roomX - 1, roomY].roomBusinessTemplate.name == "Amplification Array")
            {
                return true;
            }
        }
        if (allRoomControllers[(roomX + 1), roomY] != null)
        {
            if (allRoomControllers[roomX + 1, roomY].roomBusinessTemplate.name == "Amplification Array")
            {
                return true;
            }
        }

        return false;
    }

    public void DetermineLikedAttributes()//function to determine all likes from nearby rooms
    {
        /*likedAttributeStat = 0;

        foreach (string liked in roomBusinessTemplate.likedAttributes)//goes through all the elements of the liked attributes array
        {
            likedAttributeStat += attributeStats[liked];
        }

        if (likedAttributeStat > 10)
        {
            likedAttributeStat = 10;
        }*/
    }

    public void DetermineDislikedAttributes()//function to determine all dislikes from nearby rooms
    {
        /*dislikedAttributeStat = 0;

        foreach (string disliked in roomBusinessTemplate.dislikedAttributes)//goes through all the elements of the disliked attributes array
        {
            dislikedAttributeStat -= attributeStats[disliked];
        }

        if (dislikedAttributeStat < -10)
        {
            dislikedAttributeStat = -10;
        }*/
    }

    public void DetermineAttributeModifier()//function that calls to determine Liked+disliked attribute stats factors to spit out the AM
    {
        //attributeModifier = likedAttributeStat + dislikedAttributeStat;//returns the total after doing math on likes/dislkes
    }

    private void UpdateProfit()
    {
        /*calculatedProfit = baseProfit * ((100 + condition) * 0.01f);
        calculatedProfit = Mathf.Floor(100 * calculatedProfit);
        calculatedProfit /= 100;

        if (ratControl.ratsStealingProfit)
        {
            calculatedProfit = Mathf.Ceil(calculatedProfit * ratControl.percentProfitLeftFromRats);
        }
        else if (ratControl.piratPenReducingProfit)
        {
            calculatedProfit = Mathf.Ceil(calculatedProfit * .75f);
        }
        else
        {
            calculatedProfit = Mathf.Ceil(calculatedProfit);
        }*/  
    }

    private void updateFactionRelationship()
    {
        /*factionRelationshipChange = attributeModifier;        

        if (roomBusinessTemplate.faction == "Law")
        {
            uiManager.lawRelationship += factionRelationshipChange;
            if (uiManager.lawRelationship > 50)
            {
                uiManager.lawRelationship = 50;
            }           
            else if(uiManager.lawRelationship < -50)
            {
                uiManager.lawRelationship = -50;
            }
        }
        if (roomBusinessTemplate.faction == "Crime")
        {
            uiManager.crimeRelationship += factionRelationshipChange;
            if (uiManager.crimeRelationship > 50)
            {
                uiManager.crimeRelationship = 50;
            }
            else if (uiManager.crimeRelationship < -50)
            {
                uiManager.crimeRelationship = -50;
            }
        }
        if (roomBusinessTemplate.faction == "BRU")
        {
            uiManager.bruRelationship += factionRelationshipChange;
            if (uiManager.bruRelationship > 50)
            {
                uiManager.bruRelationship = 50;
            }
            else if (uiManager.bruRelationship < -50)
            {
                uiManager.bruRelationship = -50;
            }
        }

        uiManager.updateFacRelationships();*/
    }

    public void UpdateCondition()
    {
        //calculate new condition
        condition = (nearLaw - nearCrime);
        //set rating
        if (condition > 0)
        {
            rating = 5.0f;
        }
        else if (condition == 0)
        {
            rating = 3.0f;
        }
        else
        {
            rating = 1.0f;
        }

        /*condition += attributeModifier;

        if (condition >= 50)
        {
            condition = 50;
        }
        else if (condition <= -50)
        {
            RemoveRoom();
        }*/
    }

    private void OnMouseOver()
    {
        if (!PauseUI.isPaused) //&& (GameController.rerollChosen || GameController.demolitionChosen))
        {
            uiManager.roomZoomScript.currentHoveredOverFoundation = this;
            bool hasAdjacentRoom = findNearbyRooms();

            if (businessInstance == null && hasAdjacentRoom)
            {
                if (ItemDragHandler.isDragging)
                {
                    gameObject.GetComponent<MeshRenderer>().material.color = new Color32(205, 205, 205, 255);
                }

                //Brings up room placement ui
                if (ItemDragHandler.mouseReleasedRoomController)
                {
                    gameObject.GetComponent<MeshRenderer>().material.color = originalColor;

                    roomBusinessTemplate = Tray.currentBusinessSelected;

                    /*if (GameController.rerollChosen)
                    {
                        roomBusinessTemplate = RerollTrayController.currentBusinessSelected;
                    }
                    else
                    {
                        roomBusinessTemplate = DemolitionTrayController.currentBusinessSelected;
                    }*/

                    if (roomBusinessTemplate.faction == "BRU")
                    {
                        //This line of code scales the placement cost based on the relationship the player has with this room's faction
                        calculatedPlacementCost = Mathf.Floor(roomBusinessTemplate.placementCost * (100 + (uiManager.bruRelationship * -1)) * 0.01f);
                    }                   
                    else if (roomBusinessTemplate.faction == "Law")
                    {
                        calculatedPlacementCost = Mathf.Floor(roomBusinessTemplate.placementCost * (100 + (uiManager.lawRelationship * -1)) * 0.01f);
                    }
                    else
                    {
                        calculatedPlacementCost = Mathf.Floor(roomBusinessTemplate.placementCost * (100 + (uiManager.crimeRelationship * -1)) * 0.01f);
                    }
                    baseProfit = roomBusinessTemplate.profit;

                    uiManager.roomZoomScript.activePlaceRoom = this;
                    GetInitialRoomValues();
                    uiManager.roomZoomScript.ShowRoomZoom();

                    ItemDragHandler.mouseReleasedRoomController = false;
                }
            }
            //This is called if the player is trying to place a room somewhere that isn't adjacent to another room
            else if (businessInstance == null && !hasAdjacentRoom && ItemDragHandler.mouseReleasedRoomController)
            {               
                uiManager.CancelAction("Room wasn't placed next to another room!");
                ItemDragHandler.mouseReleasedRoomController = false;                             
            }
            else
            {
                //This code allows the player to click on rooms and check their current status
                if (Input.GetMouseButtonDown(0) && !RoomZoom.currentlyPlacingRoom) //!uiManager.profitAnalyticUIVisible &&
                {
                    if (roomBusinessTemplate != null)
                    {
                        /*//Makes rat cleanup ui show if a rat room isn't being cleaned, otherwise show room zoom for room
                        if (roomBusinessTemplate.roomName == "Rats" && (!ratControl.cleanupStarted && !ratControl.exterminationStarted))
                        { 
                            uiManager.currentRatRoom = gameObject;
                            uiManager.ShowRatUI();
                        }

                        else if (roomBusinessTemplate.roomName != "Rats")
                        {
                            uiManager.roomZoomScript.activePlaceRoom = GetComponent<RoomController>();
                            GetInitialRoomValues();
                            uiManager.roomZoomScript.ShowRoomZoom();
                        }*/
                    }
                }
                //This if statement shows all the selected room's incoming attributes, including more than just the ones coming from adjacent rooms.
                //This is a part of the analytic UI.
                /*else if (Input.GetMouseButtonDown(0) && uiManager.profitAnalyticUIVisible && !uiManager.roomAttributeAnalyticUIVisible)
                {
                    uiManager.roomZoomScript.activePlaceRoom = GetComponent<RoomController>();
                    uiManager.ShowIncomingAttributesAnalyticUI();
                }
                //This if statement makes the attribute part of the analytic ui made active in the previous if statement inactive.
                //It also shows the profit part of the analytic ui again.
                else if (Input.GetMouseButtonDown(0) && uiManager.roomAttributeAnalyticUIVisible)
                {
                    uiManager.HideIncomingAttributeAnalyticUI();
                }*/
            }
        }
    }

    private void OnMouseExit()
    {
        gameObject.GetComponent<MeshRenderer>().material.color = originalColor;

        uiManager.roomZoomScript.currentHoveredOverFoundation = null;
    }

    public void AddRoomBusiness(bool isBruLawOrCrimeFaction)
    {
        /*if(roomBusinessTemplate.roomName == "Pirat Petting Pen")
        {
            businessInstance = Instantiate(roomBusinessTemplate.roomArt1, 
                new Vector3(gameObject.transform.position.x + 4.75f, gameObject.transform.position.y + 0.5f, gameObject.transform.position.z - 0.88f), Quaternion.identity);//spawns room OBJ at parent's location
        }*/
        if (roomBusinessTemplate.roomName == "Teleportation Hub")
        {
            businessInstance = Instantiate(roomBusinessTemplate.roomArt3,
                new Vector3(gameObject.transform.position.x + 5f, gameObject.transform.position.y + 0.36f, gameObject.transform.position.z - 0.88f), Quaternion.identity);//spawns room OBJ at parent's location
        }
        else if (roomBusinessTemplate.roomName == "Amplification Array")
        {
            businessInstance = Instantiate(roomBusinessTemplate.roomArt3,
                new Vector3(gameObject.transform.position.x + 5f, gameObject.transform.position.y + 0.36f, gameObject.transform.position.z - 0.88f), Quaternion.identity);//spawns room OBJ at parent's location
        }
        else
        {
            businessInstance = Instantiate(roomBusinessTemplate.roomArt3,
        new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + 2.6f, gameObject.transform.position.z), Quaternion.identity);//spawns room OBJ at parent's location
        }
             
        hasBeenPlaced = true;
        output = roomBusinessTemplate.outputs;        
        allRoomControllers[roomX, roomY] = GetComponent<RoomController>();
        
        if (isBruLawOrCrimeFaction)
        {
            /*int randInt = Random.Range(0, 3);
            AudioClip tempMoney = money[randInt];
            audioSource.PlayOneShot(tempMoney);*/

            int randInt = Random.Range(0, 3);
            AudioClip tempConstruction = construction[randInt];
            audioSource.PlayOneShot(tempConstruction);

            analyticUICanvas.SetActive(true);
            StartCoroutine(UpdateAlienSprite());

            floorMesh.customerDestinationList.Add(gameObject);

            if (roomBusinessTemplate.faction == "BRU")
            {
                uiManager.numOfBRURooms++;
                uiManager.bru.value = 45;
            }
            else if (roomBusinessTemplate.faction == "Law")
            {
                uiManager.numOfLawRooms++;
                uiManager.law.value = 45;
            }
            else if (roomBusinessTemplate.faction == "Crime")
            {
                uiManager.numOfCrimeRooms++;
                uiManager.crime.value = 45;
            }
        }
        //print(uiManager.numOfBRURooms + uiManager.numOfLawRooms + uiManager.numOfCrimeRooms);

        if (roomBusinessTemplate.name.Equals("Teleportation Hub"))
        {
            StartCoroutine(StartTeleporting());
        }

        uiManager.calculateRatings();
    }

    public void TeleportRoomBusiness()
    {
        businessInstance = Instantiate(roomBusinessTemplate.roomArt3,
        new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + 2.6f, gameObject.transform.position.z), Quaternion.identity);//spawns room OBJ at parent's location

        hasBeenPlaced = true;
        output = roomBusinessTemplate.outputs;
        allRoomControllers[roomX, roomY] = GetComponent<RoomController>();

        analyticUICanvas.SetActive(true);
        StartCoroutine(UpdateAlienSprite());

        floorMesh.customerDestinationList.Add(gameObject);

        uiManager.calculateRatings();
    }

    IEnumerator StartTeleporting()
    {
        yield return new WaitForSeconds(45);

        List<RoomController> rooms = new List<RoomController>();

        if (allRoomControllers[roomX, roomY - 1] != null)
        {
            rooms.Add(allRoomControllers[roomX, roomY - 1]);
        }
        if (allRoomControllers[roomX, roomY + 1] != null)
        {
            rooms.Add(allRoomControllers[roomX, roomY + 1]);
        }
        if (allRoomControllers[roomX - 1, roomY] != null)
        {
            rooms.Add(allRoomControllers[roomX - 1, roomY]);
        }
        if (allRoomControllers[roomX + 1, roomY] != null)
        {
            rooms.Add(allRoomControllers[roomX + 1, roomY]);
        }

        int randInt = 0;
        RoomCreation tempRoom;
        for(int i = 0; i < rooms.Count; i++)
        {
            tempRoom = rooms[i].roomBusinessTemplate;
            randInt = Random.Range(0, rooms.Count);

            //print("Try #" + i + " :" + randInt);

            if (tempRoom != rooms[randInt].roomBusinessTemplate)
            {
                rooms[i].RemoveRoom();
                rooms[i].roomBusinessTemplate = rooms[randInt].roomBusinessTemplate;
                rooms[i].TeleportRoomBusiness();

                rooms[randInt].RemoveRoom();
                rooms[randInt].roomBusinessTemplate = tempRoom;
                rooms[randInt].TeleportRoomBusiness();
            }       
        }

        StartCoroutine(StartTeleporting());
    }

    public bool findNearbyRooms()
    {
        if (allRoomControllers[roomX, (roomY - 1)] != null && !(allRoomControllers[roomX, (roomY - 1)].roomBusinessTemplate.roomName == "Rats") &&
            !(allRoomControllers[roomX, (roomY - 1)].roomBusinessTemplate.roomName == "Pirat Petting Pen"))
        {          
            return true;          
        }

        if (allRoomControllers[roomX, (roomY + 1)] != null && !(allRoomControllers[roomX, (roomY + 1)].roomBusinessTemplate.roomName == "Rats") && 
            !(allRoomControllers[roomX, (roomY + 1)].roomBusinessTemplate.roomName == "Pirat Petting Pen"))
        {
            return true;
        }

        if (allRoomControllers[(roomX - 1), roomY] != null && !(allRoomControllers[(roomX - 1), roomY].roomBusinessTemplate.roomName == "Rats") &&
            !(allRoomControllers[(roomX - 1), roomY].roomBusinessTemplate.roomName == "Pirat Petting Pen"))
        {
            return true;
        }

        if (allRoomControllers[(roomX + 1), roomY] != null && !(allRoomControllers[(roomX + 1), roomY].roomBusinessTemplate.roomName == "Rats") &&
            !(allRoomControllers[(roomX + 1), roomY].roomBusinessTemplate.roomName == "Pirat Petting Pen"))
        {
            return true;
        }

        return false;
    }

    //This function is called by the Room Zoom upgrade button and actually updates this room's 3D model as well as increases its profit and outputmodifier.
    public void UpgradeRoom()
    {
        /*level++;
        outputModifier++;

        if (level == 2)
        {
            Destroy(businessInstance);
            businessInstance = Instantiate(roomBusinessTemplate.roomArt2, 
                new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + 2.6f, gameObject.transform.position.z), 
                Quaternion.identity);//spawns room OBJ at parent's location
            baseProfit = roomBusinessTemplate.profit * 1.25f;

            int randInt = Random.Range(0, 3);
            AudioClip tempMoney = money[randInt];
            audioSource.PlayOneShot(tempMoney);

            randInt = Random.Range(0, 3);
            AudioClip tempConstruction = construction[randInt];
            audioSource.PlayOneShot(tempConstruction);
        }
        else if (level == 3)
        {
            Destroy(businessInstance);
            businessInstance = Instantiate(roomBusinessTemplate.roomArt3, 
                new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + 2.6f, gameObject.transform.position.z), 
                Quaternion.identity);//spawns room OBJ at parent's location
            baseProfit = roomBusinessTemplate.profit * 1.5f;

            int randInt = Random.Range(0, 3);
            AudioClip tempMoney = money[randInt];
            audioSource.PlayOneShot(tempMoney);

            randInt = Random.Range(0, 3);
            AudioClip tempConstruction = construction[randInt];
            audioSource.PlayOneShot(tempConstruction);
        }

        //Next three lines update the room zoom ui 
        UpdateProfit();
        uiManager.roomZoomScript.profitText.GetComponent<TextMeshProUGUI>().text = calculatedProfit.ToString();
        uiManager.roomZoomScript.outputText.GetComponent<TextMeshProUGUI>().text = "Output:\n" + roomBusinessTemplate.outputs + " " + outputModifier;*/
        
    }

    //This function removes a room and everything associated with it, essentially wiping this script clean for the next room and also removing the 3D gameobject in the scene.
    //It is called when the player removes the room by paying 500 ziggies or when the room leaves due to a -50 condition.
    public void RemoveRoom()
    {
        Destroy(businessInstance);

        /*if (uiManager.roomZoomScript.roomZoomVisible)
        {
            uiManager.roomZoomScript.ExitRoomZoom();
        }

        if (roomBusinessTemplate.faction == "BRU")
        {
            uiManager.numOfBRURooms--;
        }
        else if (roomBusinessTemplate.faction == "Law")
        {
            uiManager.numOfLawRooms--;
        }
        else if (roomBusinessTemplate.faction == "Crime")
        {
            uiManager.numOfCrimeRooms--;
        }
        else if(roomBusinessTemplate.name == "Pirat Petting Pen")
        {
            uiManager.numOfPiratPettingPens--;
        }*/

        analyticUICanvas.SetActive(false);
        StopCoroutine(UpdateAlienSprite());

        roomBusinessTemplate = null;
        businessInstance = null;
        hasBeenPlaced = false;
        outputModifier = 1;
        condition = 0;
        level = 1;
        output = null;
        allRoomControllers[roomX, roomY] = null;
        floorMesh.customerDestinationList.Remove(gameObject);
        CancelInvoke("UpdateRoomValues");
    }

    IEnumerator UpdateAlienSprite()
    {
        if (rating == 5)
        {
            profitStatusSprite.GetComponent<Image>().sprite = alienHappySprite;
        }
        else if (rating == 3)
        {
            profitStatusSprite.GetComponent<Image>().sprite = alienNeutralSprite;
        }
        else
        {
            profitStatusSprite.GetComponent<Image>().sprite = alienMadSprite;
        }

        yield return new WaitForSeconds(1f);

        StartCoroutine(UpdateAlienSprite());
    }

}