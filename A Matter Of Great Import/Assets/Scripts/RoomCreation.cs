using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Room", menuName = "Room")]
public class RoomCreation : ScriptableObject
{
    public string roomName;
    public string faction;
    public string outputs;

    public string[] dislikedAttributes;
    public string[] likedAttributes;

    public int profit;
    public int placementCost;

    public Vector2 location;

    public GameObject roomArt1;
    public GameObject roomArt2;
    public GameObject roomArt3;
    public Sprite roomJPG;

    //public string type;

    //public string[] attributesSent;
    //public string[] attributesReceived;

    //public int foottraffic;
    //public int[] outputValues;

    public override string ToString()
    {
        return "Name: " + roomName + ", X and Y Coordinates: (" + location.x + "," + location.y + ")";
    }
}
