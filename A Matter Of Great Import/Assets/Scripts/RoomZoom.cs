﻿using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;

public class RoomZoom : MonoBehaviour
{
    public UIManager uiManager;
    public PauseUI pauseUI;

    public RoomController currentHoveredOverFoundation;
    public RoomController activePlaceRoom;
    public RoomController originalSelectedRoom;
    public RoomController previousSelectedRoom;
    public GameObject mainCamera;
    public Vector3 originalPosition;

    public GameObject roomZoom;
    public bool roomZoomVisible;

    public GameObject roomNameText;
    public GameObject roomLevelText;
    public GameObject profitText;
    public GameObject conditionProfitNumText;
    public GameObject conditionText;
    public GameObject conditionExplanationNumText;
    public GameObject factionText;
    public GameObject factionRelationshipStatusText;
    public GameObject factionSprite;
    public Sprite[] allFactionSprites;
    public GameObject positiveAttributesText;
    public GameObject negativeAttributesText;
    public GameObject outputText;

    public GameObject topRoomArrowUp;
    public GameObject topRoomArrowDown;
    public GameObject bottomRoomArrowUp;
    public GameObject bottomRoomArrowDown;
    public GameObject leftRoomArrowLeft;
    public GameObject leftRoomArrowRight;
    public GameObject rightRoomArrowLeft;
    public GameObject rightRoomArrowRight;

    public GameObject placementCostText;
    public GameObject placementCostBackground;
    public GameObject placeRoomButton;
    public static bool currentlyPlacingRoom;

    public GameObject upgradeButton;
    public GameObject upgradeButtonText;
    private float upgradeCost;
    
    public GameObject removeButton;
    public GameObject removeUI;
    public GameObject removeButtonText;
    public GameObject removeRoomYesButton;
    public GameObject removeRoomNoButton;
    private float removeRoomCost;

    //Room Demolition Variables
    public GameObject upgradeButtonDemolition;
    public GameObject upgradeButtonDemolitionText;
    public GameObject removeButtonDemolition;
    public GameObject removeButtonDemolitionText;
    public GameObject changeRoomButton;
    public GameObject changeRoomUI;
    public GameObject changeUIRoomNameText;
    public GameObject changeUIRoomImage;
    public GameObject changeUILeftArrow;
    public GameObject changeUIRightArrow;
    public GameObject confirmChangeButton;
    public GameObject cancelChangeButton;
    public int currentChangeRoom;


    // Start is called before the first frame update
    void Start()
    {
        placeRoomButton.GetComponent<Button>().onClick.AddListener(PlaceRoom);

        upgradeButton.GetComponent<Button>().interactable = false;
        removeButton.GetComponent<Button>().interactable = false;
        removeButtonDemolition.GetComponent<Button>().interactable = false;
        changeRoomButton.GetComponent<Button>().interactable = false;
    }

    private void Update()
    {
        //If the player clicks outside a UI object, room zoom is visible, and they aren't clicking on another placed room, then exit room zoom
        if (Input.GetMouseButtonDown(0) && roomZoomVisible && !EventSystem.current.IsPointerOverGameObject())
        {
            if (currentHoveredOverFoundation == null || currentHoveredOverFoundation.roomBusinessTemplate == null)
            {
                ExitRoomZoom();
            }           
        }
    }

    //This is a very lengthy function that sets up room zoom whenever it is brought up either during room placement or when the player clicks on a room
    public void ShowRoomZoom()
    {
        mainCamera.GetComponent<CameraMovement>().enabled = false;
        
        positiveAttributesText.GetComponent<TextMeshProUGUI>().text = "";
        negativeAttributesText.GetComponent<TextMeshProUGUI>().text = "";     

        topRoomArrowUp.SetActive(false);
        topRoomArrowDown.SetActive(false);
        bottomRoomArrowUp.SetActive(false);
        bottomRoomArrowDown.SetActive(false);
        leftRoomArrowLeft.SetActive(false);
        leftRoomArrowRight.SetActive(false);
        rightRoomArrowLeft.SetActive(false);
        rightRoomArrowRight.SetActive(false);

        mainCamera.GetComponent<Camera>().transform.eulerAngles = new Vector3(90, 0, 0);
        mainCamera.transform.position = new Vector3(activePlaceRoom.transform.position.x, activePlaceRoom.transform.position.y + 30, activePlaceRoom.transform.position.z);

        //Setting room placement UI active or inactive
        if (!activePlaceRoom.hasBeenPlaced)
        {
            currentlyPlacingRoom = true;

            placeRoomButton.SetActive(true);         
        }
        else
        {
            placeRoomButton.SetActive(false);
        }

        roomNameText.GetComponent<TextMeshProUGUI>().text = activePlaceRoom.roomBusinessTemplate.roomName;

        if (activePlaceRoom.condition > 0)
        {
            conditionText.GetComponent<TextMeshProUGUI>().color = Color.green;
        }
        else if (activePlaceRoom.condition < 0)
        {
            conditionText.GetComponent<TextMeshProUGUI>().color = Color.red;
        }
        else
        {
            conditionText.GetComponent<TextMeshProUGUI>().color = Color.white;
        }
        conditionText.GetComponent<TextMeshProUGUI>().text = activePlaceRoom.condition.ToString();

        factionText.GetComponent<TextMeshProUGUI>().text = "Faction: " + activePlaceRoom.roomBusinessTemplate.faction;          

        factionSprite.SetActive(true);
        if (activePlaceRoom.roomBusinessTemplate.faction.Equals("Law"))
        {
            if(uiManager.lawRelationship > 0)
            {
                factionRelationshipStatusText.GetComponent<TextMeshProUGUI>().color = Color.green;
                factionRelationshipStatusText.GetComponent<TextMeshProUGUI>().text = "Good";
            }
            else if (uiManager.lawRelationship < 0)
            {
                factionRelationshipStatusText.GetComponent<TextMeshProUGUI>().color = Color.red;
                factionRelationshipStatusText.GetComponent<TextMeshProUGUI>().text = "Bad";
            }
            else
            {
                factionRelationshipStatusText.GetComponent<TextMeshProUGUI>().color = new Color(0.8f, 0.8f, 0.8f);
                factionRelationshipStatusText.GetComponent<TextMeshProUGUI>().text = "Neutral";
            }
            factionSprite.GetComponent<Image>().sprite = allFactionSprites[0];
        }
        else if (activePlaceRoom.roomBusinessTemplate.faction.Equals("BRU"))
        {
            if (uiManager.bruRelationship > 0)
            {
                factionRelationshipStatusText.GetComponent<TextMeshProUGUI>().color = Color.green;
                factionRelationshipStatusText.GetComponent<TextMeshProUGUI>().text = "Good";
            }
            else if (uiManager.bruRelationship < 0)
            {
                factionRelationshipStatusText.GetComponent<TextMeshProUGUI>().color = Color.red;
                factionRelationshipStatusText.GetComponent<TextMeshProUGUI>().text = "Bad";
            }
            else
            {
                factionRelationshipStatusText.GetComponent<TextMeshProUGUI>().color = new Color(0.8f, 0.8f, 0.8f);
                factionRelationshipStatusText.GetComponent<TextMeshProUGUI>().text = "Neutral";
            }
            factionSprite.GetComponent<Image>().sprite = allFactionSprites[1];
        }
        else if (activePlaceRoom.roomBusinessTemplate.faction.Equals("Crime"))
        {
            if (uiManager.crimeRelationship > 0)
            {
                factionRelationshipStatusText.GetComponent<TextMeshProUGUI>().color = Color.green;
                factionRelationshipStatusText.GetComponent<TextMeshProUGUI>().text = "Good";
            }
            else if (uiManager.crimeRelationship < 0)
            {
                factionRelationshipStatusText.GetComponent<TextMeshProUGUI>().color = Color.red;
                factionRelationshipStatusText.GetComponent<TextMeshProUGUI>().text = "Bad";
            }
            else
            {
                factionRelationshipStatusText.GetComponent<TextMeshProUGUI>().color = new Color(0.8f, 0.8f, 0.8f);
                factionRelationshipStatusText.GetComponent<TextMeshProUGUI>().text = "Neutral";
            }
            factionSprite.GetComponent<Image>().sprite = allFactionSprites[2];
        }
        else
        {
            factionRelationshipStatusText.GetComponent<TextMeshProUGUI>().color = new Color(0.8f, 0.8f, 0.8f);
            factionRelationshipStatusText.GetComponent<TextMeshProUGUI>().text = "Neutral";
            factionSprite.SetActive(false);
        }


        for (int i = 0; i < activePlaceRoom.roomBusinessTemplate.likedAttributes.Length; i++)
        {
            positiveAttributesText.GetComponent<TextMeshProUGUI>().text += activePlaceRoom.roomBusinessTemplate.likedAttributes[i] + " " + activePlaceRoom.attributeStats[activePlaceRoom.roomBusinessTemplate.likedAttributes[i]] + "\n";
        }

        for (int i = 0; i < activePlaceRoom.roomBusinessTemplate.dislikedAttributes.Length; i++)
        {
            negativeAttributesText.GetComponent<TextMeshProUGUI>().text += activePlaceRoom.roomBusinessTemplate.dislikedAttributes[i] + " " + activePlaceRoom.attributeStats[activePlaceRoom.roomBusinessTemplate.dislikedAttributes[i]] + "\n";
        }

        outputText.GetComponent<TextMeshProUGUI>().text = "Output:\n" + activePlaceRoom.roomBusinessTemplate.outputs + " " + activePlaceRoom.outputModifier;


        Color currColor;
        if (RoomController.allRoomControllers[activePlaceRoom.roomX - 1, activePlaceRoom.roomY] != null &&
            RoomController.allRoomControllers[activePlaceRoom.roomX - 1, activePlaceRoom.roomY].roomBusinessTemplate.name != "Rats")
        {
            /*topRoomAttributeSprite.GetComponent<Image>().sprite = DetermineAttributeSprite(RoomController.allRoomControllers[activePlaceRoom.roomX - 1, activePlaceRoom.roomY]);
            topRoomOutput.GetComponent<TextMeshProUGUI>().text = RoomController.allRoomControllers[activePlaceRoom.roomX - 1, activePlaceRoom.roomY].output
            + " " + RoomController.allRoomControllers[activePlaceRoom.roomX - 1, activePlaceRoom.roomY].outputModifier;*/


            //topRoom.SetActive(true);

            //topRoomOutput.GetComponent<TextMeshProUGUI>().color = currColor;

            currColor = DetermineAttributeTextColor(RoomController.allRoomControllers[activePlaceRoom.roomX - 1, activePlaceRoom.roomY]);
            topRoomArrowDown.GetComponent<Image>().color = currColor;

            currColor = DetermineAttributeTextColor(activePlaceRoom);
            topRoomArrowUp.GetComponent<Image>().color = currColor;

            topRoomArrowUp.SetActive(true);
            topRoomArrowDown.SetActive(true);
        }
        if (RoomController.allRoomControllers[activePlaceRoom.roomX + 1, activePlaceRoom.roomY] != null &&
            RoomController.allRoomControllers[activePlaceRoom.roomX + 1, activePlaceRoom.roomY].roomBusinessTemplate.name != "Rats")
        {
            /*bottomRoomAttributeSprite.GetComponent<Image>().sprite = DetermineAttributeSprite(RoomController.allRoomControllers[activePlaceRoom.roomX + 1, activePlaceRoom.roomY]);
            bottomRoomOutput.GetComponent<TextMeshProUGUI>().text = RoomController.allRoomControllers[activePlaceRoom.roomX + 1, activePlaceRoom.roomY].output
            + " " + RoomController.allRoomControllers[activePlaceRoom.roomX + 1, activePlaceRoom.roomY].outputModifier;

            bottomRoom.SetActive(true);

            currColor = DetermineAttributeTextColor(RoomController.allRoomControllers[activePlaceRoom.roomX + 1, activePlaceRoom.roomY].output);
            bottomRoomOutput.GetComponent<TextMeshProUGUI>().color = currColor;
            bottomRoomArrow.GetComponent<Image>().color = currColor;*/
            currColor = DetermineAttributeTextColor(activePlaceRoom);
            bottomRoomArrowDown.GetComponent<Image>().color = currColor;

            currColor = DetermineAttributeTextColor(RoomController.allRoomControllers[activePlaceRoom.roomX + 1, activePlaceRoom.roomY]);
            bottomRoomArrowUp.GetComponent<Image>().color = currColor;

            bottomRoomArrowUp.SetActive(true);
            bottomRoomArrowDown.SetActive(true);
        }
        if (RoomController.allRoomControllers[activePlaceRoom.roomX, activePlaceRoom.roomY - 1] != null &&
            RoomController.allRoomControllers[activePlaceRoom.roomX, activePlaceRoom.roomY - 1].roomBusinessTemplate.name != "Rats")
        {
            /*leftRoomAttributeSprite.GetComponent<Image>().sprite = DetermineAttributeSprite(RoomController.allRoomControllers[activePlaceRoom.roomX, activePlaceRoom.roomY - 1]);
            leftRoomOutput.GetComponent<TextMeshProUGUI>().text = RoomController.allRoomControllers[activePlaceRoom.roomX, activePlaceRoom.roomY - 1].output
            + " " + RoomController.allRoomControllers[activePlaceRoom.roomX, activePlaceRoom.roomY - 1].outputModifier;

            leftRoom.SetActive(true);

            currColor = DetermineAttributeTextColor(RoomController.allRoomControllers[activePlaceRoom.roomX, activePlaceRoom.roomY - 1].output);
            leftRoomOutput.GetComponent<TextMeshProUGUI>().color = currColor;
            leftRoomArrow.GetComponent<Image>().color = currColor;*/
            currColor = DetermineAttributeTextColor(activePlaceRoom);
            leftRoomArrowLeft.GetComponent<Image>().color = currColor;

            currColor = DetermineAttributeTextColor(RoomController.allRoomControllers[activePlaceRoom.roomX, activePlaceRoom.roomY - 1]);          
            leftRoomArrowRight.GetComponent<Image>().color = currColor;

            leftRoomArrowLeft.SetActive(true);
            leftRoomArrowRight.SetActive(true);
        }
        if (RoomController.allRoomControllers[activePlaceRoom.roomX, activePlaceRoom.roomY + 1] != null &&
            RoomController.allRoomControllers[activePlaceRoom.roomX, activePlaceRoom.roomY + 1].roomBusinessTemplate.name != "Rats")
        {
            /*rightRoomAttributeSprite.GetComponent<Image>().sprite = DetermineAttributeSprite(RoomController.allRoomControllers[activePlaceRoom.roomX, activePlaceRoom.roomY + 1]);
            rightRoomOutput.GetComponent<TextMeshProUGUI>().text = RoomController.allRoomControllers[activePlaceRoom.roomX, activePlaceRoom.roomY + 1].output
            + " " + RoomController.allRoomControllers[activePlaceRoom.roomX, activePlaceRoom.roomY + 1].outputModifier;

            rightRoom.SetActive(true);

            currColor = DetermineAttributeTextColor(RoomController.allRoomControllers[activePlaceRoom.roomX, activePlaceRoom.roomY + 1].output);
            rightRoomOutput.GetComponent<TextMeshProUGUI>().color = currColor;
            rightRoomArrow.GetComponent<Image>().color = currColor;*/
            currColor = DetermineAttributeTextColor(RoomController.allRoomControllers[activePlaceRoom.roomX, activePlaceRoom.roomY + 1]);            
            rightRoomArrowLeft.GetComponent<Image>().color = currColor;

            currColor = DetermineAttributeTextColor(activePlaceRoom);
            rightRoomArrowRight.GetComponent<Image>().color = currColor;

            rightRoomArrowLeft.SetActive(true);
            rightRoomArrowRight.SetActive(true);
        }


        roomZoom.SetActive(true);

        //This makes the buttons below room zoom clickable after room zoom has been active for one second
        StartCoroutine(MakeButtonsClickable());
        StartCoroutine(UpdateRoomZoom());
    }

    public Color DetermineAttributeTextColor(RoomController room)
    {
        if (room.roomBusinessTemplate.faction == "Law")
        {
            return Color.green;
        }
        else if (room.roomBusinessTemplate.faction == "Crime")
        {
            return Color.red;
        }
        else
        {
            return Color.white;
        }
    }

    public void PlaceRoom()
    {
        if (activePlaceRoom.roomBusinessTemplate.faction == "BRU" && uiManager.bruRoomChargeCount > 0)//activePlaceRoom.calculatedPlacementCost <= uiManager.money)
        {
            currentlyPlacingRoom = false;
            RoomController.placedRoom = true;
            uiManager.confirmPlace();
            activePlaceRoom.AddRoomBusiness(true);

            placeRoomButton.SetActive(false);

            placementCostText.SetActive(false);
            placementCostBackground.SetActive(false);

            /*//This makes the remove button and change room buttons for the demolition room selection system to appear in the room zoom UI
            if (GameController.demolitionChosen)
            {
                upgradeButtonDemolition.SetActive(true);
                removeButtonDemolition.SetActive(true);
                changeRoomButton.SetActive(true);
            }*/
            
            //This shows the reroll room selection system's remove button            
            upgradeButton.SetActive(true);
            removeButton.SetActive(true);
            ExitRoomZoom();
        }
        else if (activePlaceRoom.roomBusinessTemplate.faction == "Law" && uiManager.lawRoomChargeCount > 0)//activePlaceRoom.calculatedPlacementCost <= uiManager.money)
        {
            currentlyPlacingRoom = false;
            RoomController.placedRoom = true;
            uiManager.confirmPlace();
            activePlaceRoom.AddRoomBusiness(true);

            placeRoomButton.SetActive(false);

            placementCostText.SetActive(false);
            placementCostBackground.SetActive(false);

            /*//This makes the remove button and change room buttons for the demolition room selection system to appear in the room zoom UI
            if (GameController.demolitionChosen)
            {
                upgradeButtonDemolition.SetActive(true);
                removeButtonDemolition.SetActive(true);
                changeRoomButton.SetActive(true);
            }*/

            //This shows the reroll room selection system's remove button            
            upgradeButton.SetActive(true);
            removeButton.SetActive(true);
            ExitRoomZoom();
        }
        else if (activePlaceRoom.roomBusinessTemplate.faction == "Crime" && uiManager.crimeRoomChargeCount > 0)//activePlaceRoom.calculatedPlacementCost <= uiManager.money)
        {
            currentlyPlacingRoom = false;
            RoomController.placedRoom = true;
            uiManager.confirmPlace();
            activePlaceRoom.AddRoomBusiness(true);

            placeRoomButton.SetActive(false);

            placementCostText.SetActive(false);
            placementCostBackground.SetActive(false);

            /*//This makes the remove button and change room buttons for the demolition room selection system to appear in the room zoom UI
            if (GameController.demolitionChosen)
            {
                upgradeButtonDemolition.SetActive(true);
                removeButtonDemolition.SetActive(true);
                changeRoomButton.SetActive(true);
            }*/

            //This shows the reroll room selection system's remove button            
            upgradeButton.SetActive(true);
            removeButton.SetActive(true);
            ExitRoomZoom();
        }
        else
        {
            currentlyPlacingRoom = false;
            activePlaceRoom.roomBusinessTemplate = null;
            uiManager.CancelAction("Not enough charges remaining!");
        }
    }

    public void ExitRoomZoom()
    {
        upgradeButton.GetComponent<Button>().interactable = false;
        upgradeButtonDemolition.GetComponent<Button>().interactable = false;
        removeButton.GetComponent<Button>().interactable = false;
        removeButtonDemolition.GetComponent<Button>().interactable = false;
        changeRoomButton.GetComponent<Button>().interactable = false;

        mainCamera.GetComponent<CameraMovement>().enabled = true;
        //uiManager.analyticsButton.GetComponent<Button>().interactable = true;

        roomZoomVisible = false;
        roomZoom.SetActive(false);

        mainCamera.GetComponent<Camera>().transform.eulerAngles = new Vector3(60, 0, 0);
        mainCamera.transform.position = originalPosition;
        

        if (currentlyPlacingRoom)
        {
            currentlyPlacingRoom = false;
            activePlaceRoom.roomBusinessTemplate = null;
        }
    }

    IEnumerator MakeButtonsClickable()
    {
        yield return new WaitForSeconds(0.1f);

        roomZoomVisible = true;

        if (activePlaceRoom.level < 3)
        {
            upgradeButtonDemolition.GetComponent<Button>().interactable = true;
            upgradeButton.GetComponent<Button>().interactable = true;
        }
        removeButton.GetComponent<Button>().interactable = true;
        removeButtonDemolition.GetComponent<Button>().interactable = true;
        changeRoomButton.GetComponent<Button>().interactable = true;
    }

    IEnumerator UpdateRoomZoom()
    {
        if (!roomZoomVisible)
        {
            StopCoroutine(UpdateRoomZoom());
        }

        yield return new WaitForSeconds(1f);

        if (activePlaceRoom.condition > 0)
        {
            conditionText.GetComponent<TextMeshProUGUI>().color = Color.green;
        }
        else if (activePlaceRoom.condition < 0)
        {
            conditionText.GetComponent<TextMeshProUGUI>().color = Color.red;
        }
        else
        {
            conditionText.GetComponent<TextMeshProUGUI>().color = Color.white;
        }
        conditionText.GetComponent<TextMeshProUGUI>().text = activePlaceRoom.condition.ToString();

        StartCoroutine(UpdateRoomZoom());
    }
}
