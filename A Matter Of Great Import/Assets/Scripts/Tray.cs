﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class Tray : MonoBehaviour
{
    public RoomCreation room1;
    public RoomCreation room2;
    public RoomCreation room3;

    public Button room1image;
    public Button room2image;
    public Button room3image;

    public GameObject room1NameText;
    public GameObject room2NameText;
    public GameObject room3NameText;

    public GameObject room1FactionText;
    public GameObject room2FactionText;
    public GameObject room3FactionText;

    private int randomInt;
    private float tempCost;

    public static RoomCreation currentBusinessSelected;
    public UIManager uiManager;

    public RoomCreation[] possibleBRURooms;
    public RoomCreation[] possibleLawRooms;
    public RoomCreation[] possibleCrimeRooms;

    private void Start()
    {
        ChangeRooms1();
        ChangeRooms2();
        ChangeRooms3();

        currentBusinessSelected = room1;
    }

    private void Update()
    {
        //print("MouseReleased: " + ItemDragHandler.mouseReleased);
        //print("Placed Room: " + RoomController.placedRoom);

        //These if statements are only used to choose a new random room scriptable object for each tray slot when the previous room is placed
        if (ItemDragHandler.roomNum.CompareTo('1') == 0 && ItemDragHandler.mouseReleasedTray && RoomController.placedRoom)
        {
            //print('1');
            RoomController.placedRoom = false;
            ItemDragHandler.mouseReleasedTray = false;
            ChangeRooms1();
        }
        if (ItemDragHandler.roomNum.CompareTo('2') == 0 && ItemDragHandler.mouseReleasedTray && RoomController.placedRoom)
        {
            //print('2');
            RoomController.placedRoom = false;
            ItemDragHandler.mouseReleasedTray = false;
            ChangeRooms2();
        }
        if (ItemDragHandler.roomNum.CompareTo('3') == 0 && ItemDragHandler.mouseReleasedTray && RoomController.placedRoom)
        {
            //print('2');
            RoomController.placedRoom = false;
            ItemDragHandler.mouseReleasedTray = false;
            ChangeRooms3();
        }
    }

    //These functions actually change the rooms available in each tray slot and are either called during reroll or after a room has been placed
    public void ChangeRooms1()
    {
        randomInt = Random.Range(0, possibleBRURooms.Length);
        room1 = possibleBRURooms[randomInt];
        room1image.GetComponent<Image>().sprite = room1.roomJPG;
        room1NameText.GetComponent<TextMeshProUGUI>().text = room1.roomName;
        room1FactionText.GetComponent<TextMeshProUGUI>().text = "Faction: BRU";
    }

    public void ChangeRooms2()
    {
        randomInt = Random.Range(0, possibleLawRooms.Length);
        room2 = possibleLawRooms[randomInt];
        room2image.GetComponent<Image>().sprite = room2.roomJPG;
        room2NameText.GetComponent<TextMeshProUGUI>().text = room2.roomName;
        room2FactionText.GetComponent<TextMeshProUGUI>().text = "Faction: Law";
    }

    public void ChangeRooms3()
    {
        randomInt = Random.Range(0, possibleCrimeRooms.Length);
        room3 = possibleCrimeRooms[randomInt];
        room3image.GetComponent<Image>().sprite = room3.roomJPG;
        room3NameText.GetComponent<TextMeshProUGUI>().text = room3.roomName;
        room3FactionText.GetComponent<TextMeshProUGUI>().text = "Faction: Crime";
    }

    //Cycles through the possibleRooms array to the left, and chooses the current placeable room (using the RoomCreation class)
    /*public void clickedLeftArrow()
    {
        currentRoomNum -= 1;
        if (currentRoomNum < 0)
        {
            currentRoomNum = possibleRooms.Length - 1;
        }

        room1 = possibleRooms[currentRoomNum];
        room1image.GetComponent<Image>().sprite = possibleRooms[currentRoomNum].roomJPG;
        room1text.text = possibleRooms[currentRoomNum].roomName;

        currentBusinessSelected = room1;
    }

    //Cycles through the possibleRooms array to the right, and chooses the current placeable room (using the RoomCreation class)
    public void clickedRightArrow()
    {
        currentRoomNum += 1;
        if (currentRoomNum > possibleRooms.Length - 1)
        {
            currentRoomNum = 0;
        }

        room1 = possibleRooms[currentRoomNum];
        room1image.GetComponent<Image>().sprite = possibleRooms[currentRoomNum].roomJPG;
        room1text.text = possibleRooms[currentRoomNum].roomName;

        currentBusinessSelected = room1;
    }*/

    /*public void clickedRoom1()
    {
        
        currentBusinessSelected = room1;
        
    }*/
}
