﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public GameObject mainCamera;
    public GameObject roomContainer;
    public List<GameObject> currentRooms;

    public static UIManager uiManager;
    public PauseUI pauseUI;
    public RoomZoom roomZoomScript;
    public GameObject tray;

    public Slider bru;
    public Slider crime;
    public Slider law;
    public GameObject bruSliderFill;
    public GameObject crimeSliderFill;
    public GameObject lawSliderFill;

    public float lawRelationship;
    public float bruRelationship;
    public float crimeRelationship;

    public GameObject bruPlacementTimeText;
    public GameObject lawPlacementTimeText;
    public GameObject crimePlacementTimeText;
    public int bruPlacementTimer;
    private int lawPlacementTimer;
    private int crimePlacementTimer;

    public GameObject bruRoomChargeCountText;
    public GameObject lawRoomChargeCountText;
    public GameObject crimeRoomChargeCountText;
    public int bruRoomChargeCount;
    public int lawRoomChargeCount;
    public int crimeRoomChargeCount;
    private int bruRoomChargeTimer;
    private int lawRoomChargeTimer;
    private int crimeRoomChargeTimer;

    public int numTotalRooms;
    public int numOfBRURooms;
    public int numOfLawRooms;
    public int numOfCrimeRooms;
    public int numOfPiratPettingPens;

    public GameObject actionFailureNotifier;
    public GameObject actionFailureNotifierText;

    public GameObject objectiveButton;
    public GameObject objectiveList;
    public GameObject objectiveText;

    //Ratings stuff
    public TextMeshProUGUI ratingsText;
    public float currentRatings;
    public float minRating;

    //Station timer
    public TextMeshProUGUI stationTimerText;
    public float stationTimer;

    public void Awake()
    {
        foreach (Transform foundation in roomContainer.transform)
        {
            currentRooms.Add(foundation.gameObject);
        }
    }

    public void Start()
    {        
        roomZoomScript.originalPosition = mainCamera.transform.position;

        bruPlacementTimer = 45;
        lawPlacementTimer = 45;
        crimePlacementTimer = 45;

        bruRoomChargeTimer = 0;
        lawRoomChargeTimer = 0;
        crimeRoomChargeTimer = 0;

        bruRoomChargeCount = 4;
        lawRoomChargeCount = 4;
        crimeRoomChargeCount = 4;

        InvokeRepeating("BRUPlacementTimer", 1, 1);
        InvokeRepeating("LawPlacementTimer", 1, 1);
        InvokeRepeating("CrimePlacementTimer", 1, 1);

        //setting slider values to timer initially
        bru.value = bruPlacementTimer;
        law.value = lawPlacementTimer;
        crime.value = crimePlacementTimer;

        objectiveButton.GetComponent<Button>().onClick.AddListener(objectivesList);
        objectiveText.GetComponent<TextMeshProUGUI>().text = "Have a rating of at least " + minRating + " stars before you fill the station or time runs out!";

        //yesButton.GetComponent<Button>().onClick.AddListener(getRidOfRats);
        //noButton.GetComponent<Button>().onClick.AddListener(leaveRatsAlone);

        //analyticsButton.GetComponent<Button>().onClick.AddListener(ShowAnalyticsUI);

        //This invokes function that shows player their overall profit over last 6 seconds and will need to be disabled
        //InvokeRepeating("OverallProfitOverSixSeconds", 6, 6);
    }

    public void Update()
    {     
        if (stationTimer >= 0)
        {
            stationTimer -= Time.deltaTime;
            stationTimerText.text = "Time Left: " + Mathf.FloorToInt(stationTimer / 60) + " minutes " + Mathf.FloorToInt(stationTimer % 60) + " seconds";
        }
        else
        {
            CheckWin();
        }

        if (numTotalRooms <= (numOfBRURooms + numOfCrimeRooms + numOfLawRooms))
        {
            CheckWin();
        }

        //The following if statements are used for developers to skip to whatever station they want to when playing in the build
        if (Input.GetKey(KeyCode.C) && Input.GetKey(KeyCode.Alpha1))
        {
            SceneManager.LoadScene(2);
        }
        if (Input.GetKey(KeyCode.C) && Input.GetKey(KeyCode.Alpha2))
        {
            SceneManager.LoadScene(6);
        }
        if (Input.GetKey(KeyCode.C) && Input.GetKey(KeyCode.Alpha3))
        {
            SceneManager.LoadScene(9);
        }
        if (Input.GetKey(KeyCode.C) && Input.GetKey(KeyCode.Alpha4))
        {
            SceneManager.LoadScene(12);
        }
        if (Input.GetKey(KeyCode.C) && Input.GetKey(KeyCode.Alpha5))
        {
            SceneManager.LoadScene(16);
        }
    }

    private void CheckWin()
    {
        if (currentRatings > minRating)
        {
            GameController.currentLevelNum++;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
        else
        {
            pauseUI.GameOver();
        }
    }

    //Calculate average ratings
    public void calculateRatings()
    {
        currentRatings = 0;
        int numRooms = 0;

        foreach (GameObject room in currentRooms)
        {
            if (room.GetComponent<RoomController>().roomBusinessTemplate != null && room.GetComponent<RoomController>().roomBusinessTemplate.faction != "none")
            {
                room.GetComponent<RoomController>().CollectNearbyAttributes();
                currentRatings += room.GetComponent<RoomController>().rating;
                numRooms += 1;
            }          
        }

        currentRatings = currentRatings / numRooms;

        if (float.IsNaN(currentRatings))
        {
            currentRatings = 0;
        }

        ratingsText.text = "Ratings: " + (currentRatings.ToString("F2")) + "/5 Stars";
    }

    public void objectivesList()
    {
        if (objectiveList.activeSelf == false)
        {
            objectiveList.SetActive(true);
        }
        else if (objectiveList.activeSelf == true)
        {
            objectiveList.SetActive(false);
        }
    }

    //Checks room charges left for current faction being placed and either allows placement or doesn't
    public void confirmPlace()
    {
        //resets timer on room placed
        if (roomZoomScript.activePlaceRoom.roomBusinessTemplate.faction == "BRU")
        {
            bruPlacementTimer = 45;
            bruRoomChargeCount--;
            bruRoomChargeCountText.GetComponent<TextMeshProUGUI>().text = bruRoomChargeCount + "/4";
            if(bruRoomChargeCount == 3)
            {
                StartCoroutine(CountdownTillBruChargeReplenished());
            }
        }
        else if (roomZoomScript.activePlaceRoom.roomBusinessTemplate.faction == "Law")
        {
            lawPlacementTimer = 45;
            lawRoomChargeCount--;
            lawRoomChargeCountText.GetComponent<TextMeshProUGUI>().text = lawRoomChargeCount + "/4";
            if (lawRoomChargeCount == 3)
            {
                StartCoroutine(CountdownTillLawChargeReplenished());
            }
        }
        else if (roomZoomScript.activePlaceRoom.roomBusinessTemplate.faction == "Crime")
        {
            crimePlacementTimer = 45;
            crimeRoomChargeCount--;
            crimeRoomChargeCountText.GetComponent<TextMeshProUGUI>().text = crimeRoomChargeCount + "/4";
            if (crimeRoomChargeCount == 3)
            {
                StartCoroutine(CountdownTillCrimeChargeReplenished());
            }
        }

        //calculateRatings();
    }

    IEnumerator CountdownTillBruChargeReplenished()
    {
        yield return new WaitForSeconds(1f);
        bruRoomChargeTimer++;

        if(bruRoomChargeTimer == 25)
        {
            bruRoomChargeTimer = 0;
            bruRoomChargeCount++;
            bruRoomChargeCountText.GetComponent<TextMeshProUGUI>().text = bruRoomChargeCount + "/4";

            if(bruRoomChargeCount != 4)
            {
                StartCoroutine(CountdownTillBruChargeReplenished());
            }
        }
        else
        {
            StartCoroutine(CountdownTillBruChargeReplenished());
        }
    }

    IEnumerator CountdownTillLawChargeReplenished()
    {
        yield return new WaitForSeconds(1f);
        lawRoomChargeTimer++;

        if (lawRoomChargeTimer == 25)
        {
            lawRoomChargeTimer = 0;
            lawRoomChargeCount++;
            lawRoomChargeCountText.GetComponent<TextMeshProUGUI>().text = lawRoomChargeCount + "/4";

            if (lawRoomChargeCount != 4)
            {
                StartCoroutine(CountdownTillLawChargeReplenished());
            }
        }
        else
        {
            StartCoroutine(CountdownTillLawChargeReplenished());
        }
    }

    IEnumerator CountdownTillCrimeChargeReplenished()
    {
        yield return new WaitForSeconds(1f);
        crimeRoomChargeTimer++;

        if (crimeRoomChargeTimer == 25)
        {
            crimeRoomChargeTimer = 0;
            crimeRoomChargeCount++;
            crimeRoomChargeCountText.GetComponent<TextMeshProUGUI>().text = crimeRoomChargeCount + "/4";

            if (crimeRoomChargeCount != 4)
            {
                StartCoroutine(CountdownTillCrimeChargeReplenished());
            }
        }
        else
        {
            StartCoroutine(CountdownTillCrimeChargeReplenished());
        }
    }

    //This updates the UI text that shows the player the current timer till a bru room must be placed 
    private void BRUPlacementTimer()
    {
        bruPlacementTimer -= 1;
        bru.value = bruPlacementTimer;
        bruPlacementTimeText.GetComponent<TextMeshProUGUI>().text = bruPlacementTimer + " seconds";

        if (bruPlacementTimer == 10)
        {
            bruPlacementTimeText.GetComponent<TextMeshProUGUI>().color = Color.red;
        }
        else if (bruPlacementTimer == 44)
        {
            bruPlacementTimeText.GetComponent<TextMeshProUGUI>().color = Color.white;
        }

        if (bruPlacementTimer == 0)
        {
            pauseUI.GameOver();
        }
    }

    private void LawPlacementTimer()
    {
        lawPlacementTimer -= 1;
        law.value = lawPlacementTimer;
        lawPlacementTimeText.GetComponent<TextMeshProUGUI>().text = lawPlacementTimer + " seconds";

        if (lawPlacementTimer == 10)
        {
            lawPlacementTimeText.GetComponent<TextMeshProUGUI>().color = Color.red;
        }
        else if (lawPlacementTimer == 44)
        {
            lawPlacementTimeText.GetComponent<TextMeshProUGUI>().color = Color.white;
        }

        if (lawPlacementTimer == 0)
        {
            pauseUI.GameOver();                   
        }
    }

    private void CrimePlacementTimer()
    {
        crimePlacementTimer -= 1;
        crime.value = crimePlacementTimer;
        crimePlacementTimeText.GetComponent<TextMeshProUGUI>().text = crimePlacementTimer + " seconds";

        if (crimePlacementTimer == 10)
        {
            crimePlacementTimeText.GetComponent<TextMeshProUGUI>().color = Color.red;
        }
        else if (crimePlacementTimer == 44)
        {
            crimePlacementTimeText.GetComponent<TextMeshProUGUI>().color = Color.white;
        }

        if (crimePlacementTimer == 0)
        {
            pauseUI.GameOver();
        }
    }

    //Cancels any action the player makes that is not allowed, such as not having enough money to pay for something or not doing adjacent room placement
    public void CancelAction(string explanationString)
    {
        actionFailureNotifierText.GetComponent<TextMeshProUGUI>().text = explanationString;
        actionFailureNotifier.SetActive(true);

        if (roomZoomScript.roomZoom.activeSelf)
        {
            roomZoomScript.ExitRoomZoom();
        }

        StartCoroutine(CancelActionEnumerator());
    }

    //Makes cancel action UI inactive after 4 seconds
    public IEnumerator CancelActionEnumerator()
    {
        yield return new WaitForSeconds(4f);

        actionFailureNotifier.SetActive(false);
    }


}
